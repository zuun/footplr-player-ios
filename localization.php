<?php

   function process_string($str) {
	  $str = str_replace('%s', '%@', $str);
	  return $str;
   }

   $file = fopen('https://docs.google.com/a/footplr.com/spreadsheets/d/1aeBJLrmeDYMfN8DhsA85l9EsoHQ0MTaqYrqwKSPUnSk/export?gid=0&format=tsv', "r");
   $f_ko = fopen("FootplrPlayer/ko.lproj/Localizable.strings", "w");
   $f_en = fopen("FootplrPlayer/en.lproj/Localizable.strings", "w");
   $f_jp = fopen("FootplrPlayer/ja.lproj/Localizable.strings", "w");
   $line_num = 0;
   while(($line = fgets($file)) !== false) {
	  $line_num++;
	  // description
	  if($line_num == 1) continue;

	  $contents = explode("\t", $line);


	  $key = $contents[2];
	  $ko = process_string($contents[3]);

	  // empty line
	  if(empty($ko) || $ko == "") continue;
	  $en = process_string($contents[4]);
	  $jp = process_string($contents[5]);

	  fwrite($f_ko, "\"$key\" = \"".$ko."\";\n");
	  fwrite($f_en, "\"$key\" = \"".$en."\";\n");
	  fwrite($f_jp, "\"$key\" = \"".$jp."\";\n");
   }
   fclose($file);
   fclose($f_ko);
   fclose($f_en);
   fclose($f_jp);
