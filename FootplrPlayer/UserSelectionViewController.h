//
//  UserSelectionViewController.h
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 5. 4..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"

@interface UserSelectionViewController : FootplrViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    NSArray *players;
}
@property int teamId;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end
