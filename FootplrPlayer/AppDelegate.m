//
//  AppDelegate.m
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 2. 23..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "EntryViewController.h"
#import "SplashViewController.h"
#import "PositionHelper.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //note: iOS only allows one crash reporting tool per app; if using another, set to: NO
    [Flurry setCrashReportingEnabled:YES];
    
    // Replace YOUR_API_KEY with the api key in the downloaded package
    [Flurry startSession:@"JX5C3YX6W3RYKQCCTMFT"];
    
    // status bar style
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    // check user data
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    NSDictionary *userInfo = [userData objectForKey:USER_INFO];
    NSLog(@"userInfo : %@", userInfo);
    
    UIViewController *rootVC;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if(userInfo) { // user info exists
        SplashViewController *svc = [storyboard instantiateViewControllerWithIdentifier:@"SplashStoryboard"];
        rootVC = svc;
        [[SessionManager sharedSessionManager] sessionLogin];
        [[SessionManager sharedSessionManager] setDelegate:svc];
    } else {
        EntryViewController *entryVC = [storyboard instantiateViewControllerWithIdentifier:@"EntryStoryboard"];
        rootVC = entryVC;
    }
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootVC];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
    
    [PositionHelper sharedPositionHelper];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

@end
