//
//  JoinViewController.h
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 5. 4..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"

@interface JoinViewController : FootplrViewController <SessionDelegate>

@property NSString *accessToken;
@property (strong, nonatomic) IBOutlet UITextField *idTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *pwTextField;
@property (strong, nonatomic) IBOutlet UITextField *pwCheckTextField;

@end
