//
//  FootplrHelper.m
//  Footplr
//
//  Created by EclipseKim on 2014. 2. 9..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "FootplrHelper.h"

@implementation FootplrHelper

static FootplrHelper *sharedFootplrHelper;
+ (FootplrHelper*)sharedFootplrHelper {
    if (sharedFootplrHelper == nil) {
        sharedFootplrHelper = [[FootplrHelper alloc] init];
    }
    return sharedFootplrHelper;
}

+(id)alloc
{
    @synchronized([FootplrHelper class])
    {
        NSAssert(sharedFootplrHelper == nil, @"Attempted to allocate a second instance of a singleton.");
        sharedFootplrHelper = [super alloc];
        
        return sharedFootplrHelper;
    }
    
    return nil;
}

-(id)init
{
    self = [super init];

    return self;
}


-(UIView*) getFirstResponderInView:(UIView*) v
{
    for (UIView *view in [v subviews]) {
        if ([view isFirstResponder]) {
            return view;
        } else {
            if([self getFirstResponderInView:view] == nil) continue;
            else return [self getFirstResponderInView:view];
        }
    }
    return nil;
}

-(void) logFrame:(CGRect) frame
{
    NSLog(@"%f %f %f %f", frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
}

-(void) showFootplrAlertView:(id)delegate withMessage:(NSString*)message
{
    if(!message) message = NSLocalizedString(@"s27", nil);
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Footplr" message:message delegate:delegate cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
    [alert show];
}

-(void) showError
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Footplr" message:NSLocalizedString(@"s27", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
    [alert show];
}

-(NSString *) getAlertViewTitle
{
    return @"Footplr Player";
}

-(NSString *) getDeviceLocale
{
    NSArray *locales = [NSLocale preferredLanguages];
    for(NSString *locale in locales) {
        NSString *_locale = locale;
        if([_locale length] > 2) {
            _locale = [_locale substringToIndex:2];
        }
        if([_locale isEqualToString:@"ko"]) return @"KO";
        else if([_locale isEqualToString:@"ja"]) return @"JA";
        else if([_locale isEqualToString:@"en"]) return @"EN";
    }
    return @"EN";
}

-(NSString *) getDeviceTypeForLog
{
    return @"iOS";
}
@end
