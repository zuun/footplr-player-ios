//
//  UserSelectionViewController.m
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 5. 4..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "UserSelectionViewController.h"
#import "PlayerCell.h"
#import "RateViewController.h"

@interface UserSelectionViewController ()

@end

@implementation UserSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _init];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) _init
{
    [self setNavigationBackButton];
    [self setNavigationTitle:@""];
    
    
    CGFloat width = self.view.frame.size.width;
    UIView *tableHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 116)];
    [tableHeader setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, width, 30)];
    titleLabel.text = NSLocalizedString(@"s51", nil);
    titleLabel.font = [UIFont systemFontOfSize:24.0f];
    titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [tableHeader addSubview:titleLabel];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 63, 280, 40)];
    subTitleLabel.text = NSLocalizedString(@"s52", nil);
    subTitleLabel.font = [UIFont systemFontOfSize:14.0f];
    subTitleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WC];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    subTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    subTitleLabel.numberOfLines = 0;
    [subTitleLabel sizeToFit];
    [subTitleLabel setFrame:CGRectMake(20, 63, 280, 40)];
    [tableHeader addSubview:subTitleLabel];
    
    _tableView.tableHeaderView = tableHeader;
    [_tableView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];

    players = [NSArray new];
    [self getPlayers];
}

-(void) getPlayers
{
    NSString *localeCode = [[FootplrHelper sharedFootplrHelper] getDeviceLocale];
    [[ApiHelper sharedApiHelper]
        getRequestWithUrl:MANAGER_API_URL
        withPath:[NSString stringWithFormat:@"/api/member/all/%d/%@", _teamId, localeCode]
        withParameters:nil
        withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
//                    NSLog(@"players : %@", responseObject);
            if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS){ // 성공
                players = [responseObject objectForKey:@"result"];
                [_tableView reloadData];
            }
    }];
}

-(void) selectUser
{
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [players count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlayerCell *cell = [[PlayerCell alloc] init];
    NSDictionary *playerInfo = [players objectAtIndex:indexPath.row];
    
    cell.backNumberLabel.text = [playerInfo objectForKey:@"back_no"];
    cell.playerNameLabel.text = [playerInfo objectForKey:@"member_nm"];
    cell.positionLabel.text = [playerInfo objectForKey:@"position_nm"];
    
    int userId = [[playerInfo objectForKey:@"user_id"] intValue];
    if(userId == 0) { // choosable
        UIButton *chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [chooseButton setFrame:CGRectMake(0, 0, 60, 30)];
        [chooseButton setBackgroundImage:[[UIImage imageNamed:@"btn_red_n"] stretchableImageWithLeftCapWidth:4 topCapHeight:4] forState:UIControlStateNormal];
        [chooseButton setTitle:NSLocalizedString(@"s53", nil) forState:UIControlStateNormal];
        [chooseButton setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_WA] forState:UIControlStateNormal];
        chooseButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        chooseButton.tag = indexPath.row;
        [chooseButton addTarget:self action:@selector(chooseUser:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryView = chooseButton;
        
        CGRect frame = cell.playerNameLabel.frame;
        frame.size.width = self.view.frame.size.width - CGRectGetMinX(frame) - chooseButton.frame.size.width - 20;
        cell.playerNameLabel.frame = frame;
    } else { // already chose
        CGRect frame = cell.playerNameLabel.frame;
        frame.size.width = self.view.frame.size.width - CGRectGetMinX(frame) - 10;
        cell.playerNameLabel.frame = frame;
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

#pragma mark - Actions

-(void) chooseUser:(UIButton *) button
{
    int row = (int) button.tag;
    NSDictionary *player = [players objectAtIndex:row];
    NSString *title = [NSString stringWithFormat:@"%@. %@", [player objectForKey:@"back_no"], [player objectForKey:@"member_nm"]];
    NSString *msg = NSLocalizedString(@"s54", nil);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"s55", nil) otherButtonTitles:NSLocalizedString(@"s56", nil), nil];
    alert.tag = [[player objectForKey:@"member_id"] intValue];
    [alert show];
}

#pragma mark - UIAlertViewDelegate

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    int memberId = (int)alertView.tag;
//    memberId = 52;
    
    if(buttonIndex == 1){
        [[ApiHelper sharedApiHelper]
         getRequestWithUrl:API_URL
         withPath:[NSString stringWithFormat:@"/api/team/ask_join/%d/%d", _teamId, memberId]
         withParameters:nil
         withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"%@", responseObject);
             if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
                 RateViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"RateStoryboard"];
                 rvc.teamId = _teamId;
                 [self.navigationController pushViewController:rvc animated:YES];
             }
         }];
    }
}

-(void) test:(int) memberId
{
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:[NSString stringWithFormat:@"/api/team/ask_join/%d/%d", _teamId, memberId] withParameters:nil withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"bb : %@", responseObject);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
