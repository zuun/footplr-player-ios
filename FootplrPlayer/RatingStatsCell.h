//
//  RatingStatsCell.h
//  footplr
//
//  Created by EclipseKim on 2015. 5. 31..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingStatsCell : UITableViewCell

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *scheduleTitleLabel;
@property (strong, nonatomic) UILabel *scheduleDescriptionLabel;
@property (strong, nonatomic) UILabel *ratingLabel;
@property (strong, nonatomic) UIView *ratingView;
@property (strong, nonatomic) UIView *starFillView;

@end
