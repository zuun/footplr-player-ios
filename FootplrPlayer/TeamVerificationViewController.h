//
//  TeamVerificationViewController.h
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 5. 4..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrKeyboardViewController.h"

@interface TeamVerificationViewController : FootplrKeyboardViewController

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *teamCodeTextField;
@property (strong, nonatomic) IBOutlet UILabel *moreLabel;
@end
