//
//  AttendedPlayerCell.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 11..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "AttendedPlayerCell.h"

@implementation AttendedPlayerCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        
        _backNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 25, 40)];
        _backNumberLabel.textAlignment = NSTextAlignmentCenter;
        _backNumberLabel.font = [UIFont systemFontOfSize:14.0f];
        _backNumberLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        [self.contentView addSubview:_backNumberLabel];
        
        _playerNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 2, 175, 40)];
        _playerNameLabel.font = [UIFont systemFontOfSize:14.0f];
        _playerNameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        [self.contentView addSubview:_playerNameLabel];
        
        _ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, 2, 60, 40)];
        _ratingLabel.textAlignment = NSTextAlignmentCenter;
        _ratingLabel.font = [UIFont systemFontOfSize:14.0f];
        _ratingLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        [self.contentView addSubview:_ratingLabel];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
