//
//  SettingsViewController.m
//  footplr
//
//  Created by EclipseKim on 2015. 5. 28..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "SettingsViewController.h"
#import "ChangePasswordViewController.h"
#import "WebViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self _init];
}

-(void) viewWillAppear:(BOOL)animated
{
    [[SessionManager sharedSessionManager] setDelegate:self];
}

-(void) _init
{
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"s83", nil)];


    UIBarButtonItem* doneBtn = [UIBarButtonItem barItemWithImageName:@"nav_confirm_n" target:self action:@selector(modifyUserInfo)];
    [self setRightBarButtons:@[doneBtn]];
    
    // tableView
    [_tableView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tap2.delegate = self;
    [_tableView addGestureRecognizer:tap2];
    _tableView.userInteractionEnabled = YES;
    
    [self setInputWrapperScrollView:_tableView];
    
    if(!_hideUserInfo) { // user info exist
        [self getUserDetails];
        // header
        imageHelper = [ImageHelper new];
        imageHelper.isSquare = YES;
        imageHelper.viewController = self;
        imageHelper.delegate = self;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
        [_headerView setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:26 withGreen:29 withBlue:31]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
        tap.delegate = self;
        [_headerView addGestureRecognizer:tap];
        _headerView.userInteractionEnabled = YES;
        
        
        // profile image wrapper
        // why ? camera icon also clips when [profileImageView addSubview:cameraIconImageView]
        UIView *avatarWrapper = [[UIView alloc] initWithFrame:CGRectMake(120, 24, 80, 80)];
        avatarWrapper.userInteractionEnabled = YES;
        UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeAvatar)];
        [avatarWrapper addGestureRecognizer:avatarTap];
        [_headerView addSubview:avatarWrapper];
        
        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        profileImageView.layer.cornerRadius = 40;
        profileImageView.layer.masksToBounds = YES;
        [profileImageView sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"default_profile_photo"]];
        [avatarWrapper addSubview:profileImageView];
        
        // camera button icon
        UIImageView *cameraIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(51, 51, 29, 29)];
        cameraIconImageView.image = [UIImage imageNamed:@"setting_add_profile_photo_n"];
        [avatarWrapper addSubview:cameraIconImageView];

        // backnumber, name, position
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 115, 300, 26)];
        nameLabel.font = [UIFont boldSystemFontOfSize:17];
        nameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [_headerView addSubview:nameLabel];
        
        positionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 145, 300, 14)];
        positionLabel.font = [UIFont systemFontOfSize:12];
        positionLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WC];
        positionLabel.textAlignment = NSTextAlignmentCenter;
        [_headerView addSubview:positionLabel];
        _tableView.tableHeaderView = _headerView;
        

        picker = [UIPickerView new];
        picker.delegate = self;
        picker.dataSource = self;
        picker.showsSelectionIndicator = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getUserDetails
{
    NSString *locale = [[FootplrHelper sharedFootplrHelper] getDeviceLocale];
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    int memberId = [[[userData objectForKey:USER_INFO] objectForKey:@"member_id"] intValue];
    [[ApiHelper sharedApiHelper] getRequestWithUrl:API_URL
        withPath:[NSString stringWithFormat:@"/api/member/detail/%@/%d", locale, memberId]
        withParameters:nil
        withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@", responseObject);
            if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
                userDetailInfo = [[responseObject objectForKey:@"result"] mutableCopy];
                
                NSString *avatarPath = [userDetailInfo objectForKey:@"profile_pic"];
                [profileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://footplr.com%@", avatarPath]] placeholderImage:[UIImage imageNamed:@"setting_profile_photo_default"]];
                
                int backNumber = [[userDetailInfo objectForKey:@"back_no"] intValue];
                NSString *memberName = [userDetailInfo objectForKey:@"member_nm"];
                NSString *positionName = [userDetailInfo objectForKey:@"position_nm"];
                
                positionLabel.text = positionName;
                
                nameLabel.text = [NSString stringWithFormat:@"%d %@", backNumber, memberName];
                [_tableView reloadData];
            }
    }];
}

-(void) modifyUserInfo
{
    if(isLoading) return ;
    isLoading = YES;
    
    [self hideKeyboard];
    
//    member_nm, back_no, login_id, email, height, weight, main_foot=[L,R]
    NSString *loginId = loginIdTextField.text;
    NSString *email = emailTextField.text;
    
    NSString *height = heightTextField.text;
    NSString *weight = weightTextField.text;
    NSString *birthday = birthdayTextField.text;
    NSString *mainFoot = [self getFootCodeByFootString:mainFootTextField.text];
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    if(loginId) params[@"login_id"] = loginId;
    if(email) params[@"email"] = email;
    if(height) params[@"height"] = height;
    if(weight) params[@"weight"] = weight;
    if(birthday) params[@"birth_dt"] = birthday;
    if(mainFoot) params[@"main_foot"] = mainFoot;
    
    NSLog(@"%@", params);

    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/user/modify" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        isLoading = NO;
    } withErrorCallback:^(AFHTTPRequestOperation *operation, NSError *error) {
        isLoading = NO;
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) return 4;
    return 3;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_hideUserInfo) {
        if(indexPath.section == 1 || indexPath.section == 2) return 48;
        else return 0;
    } else {
        return 48;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    // separator line
    if(indexPath.section == 0 || indexPath.section == 1) {
        UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 47, 320, 1)];
        [separator setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
        [cell.contentView addSubview:separator];
    }
    if(indexPath.section == 0) {
        if(_hideUserInfo) {
            return cell;
        }
        if(indexPath.row == 0) {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_height"];
            
            // textfield
            if(!heightTextField) {
                heightTextField = [[UITextField alloc] initWithFrame:CGRectMake(65, 0, 220, 48)];
                heightTextField.placeholder = NSLocalizedString(@"s86", nil);
                heightTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
                heightTextField.font = [UIFont systemFontOfSize:14.0f];
                heightTextField.keyboardType = UIKeyboardTypeNumberPad;
                heightTextField.delegate = self;
            }
        
            int height = [[userDetailInfo objectForKey:@"height"] intValue];
            if(height != 0)
                heightTextField.text = [NSString stringWithFormat:@"%d", height];
            [cell.contentView addSubview:heightTextField];
            
            UILabel *cmLabel = [[UILabel alloc] initWithFrame:CGRectMake(290, 0, 20, 48)];
            cmLabel.font = [UIFont systemFontOfSize:14.0f];
            cmLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BC];
            cmLabel.text = @"cm";
            [cell.contentView addSubview:cmLabel];
        } else if(indexPath.row == 1) {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_weight"];
            
            // textfield
            if(!weightTextField) {
                weightTextField = [[UITextField alloc] initWithFrame:CGRectMake(65, 0, 220, 48)];
                weightTextField.placeholder = NSLocalizedString(@"s87", nil);
                weightTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
                weightTextField.font = [UIFont systemFontOfSize:14.0f];
                weightTextField.keyboardType = UIKeyboardTypeNumberPad;
                weightTextField.delegate = self;
            }
            
            int weight = [[userDetailInfo objectForKey:@"weight"] intValue];
            if(weight != 0)
                weightTextField.text = [NSString stringWithFormat:@"%d", weight];
            [cell.contentView addSubview:weightTextField];
            
            UILabel *kgLabel = [[UILabel alloc] initWithFrame:CGRectMake(290, 0, 20, 48)];
            kgLabel.font = [UIFont systemFontOfSize:14.0f];
            kgLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BC];
            kgLabel.text = @"kg";
            [cell.contentView addSubview:kgLabel];
        } else if(indexPath.row == 2) {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_birth"];
            if(!birthdayTextField) {
                birthdayTextField = [[UITextField alloc] initWithFrame:CGRectMake(65, 0, 245, 48)];
                birthdayTextField.placeholder = NSLocalizedString(@"s87_1", nil);
                birthdayTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
                birthdayTextField.font = [UIFont systemFontOfSize:14.0f];
                
                // date picker
                datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 216, 320, 216)];
                datePicker.datePickerMode = UIDatePickerModeDate;
                [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
                birthdayTextField.inputView = datePicker;
            }
            NSString *birthday = [userDetailInfo objectForKey:@"birth_dt"];
            if(birthday && ![birthday isEqualToString:@""] && ![birthday isEqualToString:@"0000-00-00"])
                birthdayTextField.text = birthday;
            [cell.contentView addSubview:birthdayTextField];
        } else {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_foot"];
            
            // textfield
            if(!mainFootTextField) {
                mainFootTextField = [[UITextField alloc] initWithFrame:CGRectMake(65, 0, 245, 48)];
                mainFootTextField.placeholder = NSLocalizedString(@"s88", nil);
                mainFootTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
                mainFootTextField.font = [UIFont systemFontOfSize:14.0f];
                
                picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 216, 320, 216)];
                picker.delegate = self;
                picker.dataSource = self;
                picker.showsSelectionIndicator = YES;
                mainFootTextField.inputView = picker;
                mainFootTextField.delegate = self;
            }
            NSString *foot = [userDetailInfo objectForKey:@"main_foot"];
            if(foot) {
                foot = [self getFootStringByFootCode:foot];
                mainFootTextField.text = foot;
            }
            [cell.contentView addSubview:mainFootTextField];
        }
    } else if(indexPath.section == 1) {
        cell.textLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];

        if(indexPath.row == 0) {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_id"];
            
            // textfield
            if(!loginIdTextField) {
                loginIdTextField = [[UITextField alloc] initWithFrame:CGRectMake(65, 0, 245, 48)];
                loginIdTextField.placeholder = NSLocalizedString(@"s15", nil);
                loginIdTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
                loginIdTextField.font = [UIFont systemFontOfSize:14.0f];
                loginIdTextField.delegate = self;
            }
            
            NSString *userId = [[[NSUserDefaults standardUserDefaults] objectForKey:USER_INFO] objectForKey:@"login_id"];
            if(userId)
                loginIdTextField.text = userId;
            [cell.contentView addSubview:loginIdTextField];
            
        } else if(indexPath.row == 1) {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_mail"];
            
            // textfield
            if(!emailTextField) {
                emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(65, 0, 245, 48)];
                emailTextField.placeholder = NSLocalizedString(@"s16", nil);
                emailTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
                emailTextField.font = [UIFont systemFontOfSize:14.0f];
                emailTextField.delegate = self;
            }
            
            NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:USER_INFO] objectForKey:@"email"];
            if(email)
                emailTextField.text = email;
            [cell.contentView addSubview:emailTextField];
        } else {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_password"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = NSLocalizedString(@"s93", nil);
        }
    } else {
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.textLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
        if(indexPath.row == 0) {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_about"];
            cell.textLabel.text = NSLocalizedString(@"s99", nil);
        } else if(indexPath.row == 1) {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_mail"];
            cell.textLabel.text = NSLocalizedString(@"s100", nil);
        } else {
            cell.imageView.image = [UIImage imageNamed:@"textfield_ic_logout"];
            cell.textLabel.text = NSLocalizedString(@"s97", nil);
        }
    }
    return cell;
}


-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(!_hideUserInfo) {
        if(section == 0 || section == 1) return 50;
    } else {
        if(section == 1) return 50;
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = [self tableView:_tableView heightForHeaderInSection:section];
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, headerHeight)];
    [header setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    header.userInteractionEnabled = YES;
    [header addGestureRecognizer:tap];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, 300, 15)];
    titleLabel.font = [UIFont systemFontOfSize:14.0f];
    titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WC];
    [header addSubview:titleLabel];
    if(section == 0) {
        titleLabel.text = NSLocalizedString(@"s84", nil);
    } else if(section == 1) {
        titleLabel.text = NSLocalizedString(@"s92", nil);
    }
    return header;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideKeyboard];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if( indexPath.section == 1 ) {
        if(indexPath.row == 2) {
            ChangePasswordViewController *cpvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordStoryboard"];
            [self.navigationController pushViewController:cpvc animated:YES];
        }
    } else if(indexPath.section == 2) {
        if(indexPath.row == 0) {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://footplr.com"]];
            WebViewController *wvc = [[WebViewController alloc] init];
            wvc.urlString = @"http://footplr.com";
            [self.navigationController pushViewController:wvc animated:YES];
        } else if(indexPath.row == 1) {
            [self sendEmail];
        } else {
            [[SessionManager sharedSessionManager] logout];
        }
    }
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 각 세션의 헤더가 스크롤시 고정되있는 현상을 수정하기 위해 위치를 재조정하는 코드 추가
//    CGFloat sectionHeaderHeight = self.tableView.sectionHeaderHeight;
    CGFloat sectionHeaderHeight = 50;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self showKeyboard];
}


#pragma mark - sendEmail

-(void) sendEmail
{
    MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
    [comp setMailComposeDelegate:self];
    if([MFMailComposeViewController canSendMail])
    {
        [comp setToRecipients:[NSArray arrayWithObjects:@"contact@footplr.com", nil]];
        
        // content
        NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString *appInfo = [NSString stringWithFormat:@"App Info : Footplr %@",appVersion];
        NSString *deviceInfo = [NSString stringWithFormat:@"Device Info : %@", [self deviceName]];
        NSString *osVersion = [NSString stringWithFormat:@"iOS Version : %@", [[UIDevice currentDevice] systemVersion]];
        NSArray *contents = @[appInfo, deviceInfo, osVersion];
        
        [comp setMessageBody:[contents componentsJoinedByString:@"\n"] isHTML:NO];
        
        [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:comp animated:YES completion:nil];
    } else {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Footplr" message:@"This device doesn't support sending email." delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil];
        [alert show];
    }
}


- (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      :@"Simulator",
                              @"iPod1,1"   :@"iPod Touch Original",
                              @"iPod2,1"   :@"iPod Touch Second Generation",
                              @"iPod3,1"   :@"iPod Touch Third Generation",
                              @"iPod4,1"   :@"iPod Touch Fourth Generation",
                              @"iPhone1,1" :@"iPhone Original",
                              @"iPhone1,2" :@"iPhone 3G",
                              @"iPhone2,1" :@"iPhone 3GS",
                              @"iPad1,1"   :@"iPad Original",
                              @"iPad2,1"   :@"iPad 2",
                              @"iPad3,1"   :@"iPad Third Generation",
                              @"iPhone3,1" :@"iPhone 4 GSM",
                              @"iPhone3,3" :@"iPhone 4 CDMA/Verizon/Sprint",
                              @"iPhone4,1" :@"iPhone 4S",
                              @"iPhone5,1" :@"iPhone 5 A1428 AT&T/Canada",
                              @"iPhone5,2" :@"iPhone 5 A1429",  // (everything else)
                              @"iPad3,4"   :@"iPad Fourth Generation",
                              @"iPad2,5"   :@"iPad Mini Original",
                              @"iPhone5,3" :@"iPhone 5c A1456, A1532 GSM",
                              @"iPhone5,4" :@"iPhone 5c A1507 A1516 A1526(China) A1529 Global",
                              @"iPhone6,1" :@"iPhone 5s A1433 A1533 GSM",
                              @"iPhone6,2" :@"iPhone 5s A1457 A1518 A1528(China) A1530 Global",
                              @"iPhone7,1" :@"iPhone 6 Plus",
                              @"iPhone7,2" :@"iPhone 6",
                              @"iPad4,1"   :@"iPad Air Fifth Generation - Wifi",
                              @"iPad4,2"   :@"iPad Air Fifth Generation - Cellular",
                              @"iPad4,4"   :@"iPad Mini Second Generation - Wifi",
                              @"iPad4,5"   :@"iPad Mini Second Generation - Cellular"
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
    }
    
    return deviceName;
}


-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if(error)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Footplr" message:@"Error occured while sending email. Please try again." delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil];
        [alert show];
        
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
    else{
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
    
}

-(NSString *) getFootStringByFootCode:(NSString *) code
{
    if([code isEqualToString:@"R"]) {
        return NSLocalizedString(@"s90", nil);
    } else if([code isEqualToString:@"L"]) {
        return NSLocalizedString(@"s89", nil);
    } else {
        return NSLocalizedString(@"s_34", nil);
    }
}

-(NSString *) getFootCodeByFootString:(NSString *) s
{
    if([s isEqualToString:NSLocalizedString(@"s90", nil)]) {
        return @"R";
    } else if([s isEqualToString:NSLocalizedString(@"s89", nil)]) {
        return @"L";
    } else {
        return @"B";
    }
}

#pragma mark - UIPickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSArray *items = @[NSLocalizedString(@"s90", nil), NSLocalizedString(@"s89", nil), NSLocalizedString(@"s_34", nil)];
    return [items objectAtIndex:row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *text = [self pickerView:pickerView titleForRow:row forComponent:component];
    [mainFootTextField setText:text];
}

#pragma mark - UIGestureRecognizerDelegate
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([[gestureRecognizer view] isKindOfClass:[UITableView class]])
        return NO;
    return YES;
}

#pragma mark - Avatar
-(void) changeAvatar
{
    [imageHelper showImageSelection];
}

-(void) updateAvatar:(NSString *)imageName
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"profile_pic"] = imageName;
    
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/user/modify" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        isLoading = NO;
    } withErrorCallback:^(AFHTTPRequestOperation *operation, NSError *error) {
        isLoading = NO;
    }];
}

#pragma mark - ImageHelperDelegate
-(void) squareImageUploadCompleted:(UIImage *)image withUploadedInfo:(NSDictionary *)uploadedInfo
{
    profileImageView.image = image;
    [self updateAvatar:[uploadedInfo objectForKey:@"file_name"]];
}

#pragma mark - UIDatePicker
-(void) dateChanged:(UIDatePicker *)picker
{
    NSDate *date = [picker date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    birthdayTextField.text = [df stringFromDate:date];
}
@end
