//
//  EntryViewController.h
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 2. 23..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"

@interface EntryViewController : FootplrViewController
{
    UIImageView *imageView;
    NSArray *images;
    int imageIndex;
}
@end

