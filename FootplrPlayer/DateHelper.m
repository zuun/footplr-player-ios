//
//  DateHelper.m
//  Footplr
//
//  Created by EclipseKim on 2014. 8. 13..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "DateHelper.h"

@implementation DateHelper
static DateHelper *sharedDateHelper;
+ (DateHelper*)sharedDateHelper {
    if (sharedDateHelper == nil) {
        sharedDateHelper = [[DateHelper alloc] init];
    }
    return sharedDateHelper;
}

+(id)alloc
{
    @synchronized([DateHelper class])
    {
        NSAssert(sharedDateHelper == nil, @"Attempted to allocate a second instance of a singleton.");
        sharedDateHelper = [super alloc];
        
        return sharedDateHelper;
    }
    
    return nil;
}

-(id)init
{
    self = [super init];
    return self;
}

-(NSInteger) getWeekDay:(NSDate*) date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSWeekdayCalendarUnit | NSSecondCalendarUnit | NSMinuteCalendarUnit | NSHourCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    return [components weekday];
}

- (NSString *) getWeekDay3CodeString:(NSDate*) date
{
    NSInteger weekday = [self getWeekDay:date];
    NSArray* weekdayArray = [[NSArray alloc] initWithObjects:@"SUN", @"MON", @"TUE", @"WED", @"THU", @"FRI", @"SAT", nil];
    return  [weekdayArray objectAtIndex:weekday - 1];
}

- (NSString *) getHumanizedDateStringForDate:(NSDate*)date withDay:(BOOL) withDay
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSWeekdayCalendarUnit | NSSecondCalendarUnit | NSMinuteCalendarUnit | NSHourCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    //    int year = [components year];
    int month = (int)[components month];
    int day = (int)[components day];
    int hour = (int)[components hour];
    int minute = (int)[components minute];
    int weekday = (int)[components weekday];
    NSString* hourString;
    if(hour >= 12) {
        if(hour == 12) hour = 12;
        else hour -= 12;
        hourString = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"s_39_2", nil), hour ];
    } else {
        if(hour == 0) hour = 12;
        hourString = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"s_39_1", nil), hour ];
    }
    
    NSString* minuteString = [NSString stringWithFormat:@"%02d", minute];
    NSString *lang = [[FootplrHelper sharedFootplrHelper] getDeviceLocale];
    if(withDay) { // 요일 포함

        if([lang isEqualToString:@"KO"] || [lang isEqualToString:@"JA"]) { // 한국어, 일본어
            return [NSString stringWithFormat:@"%@ %@ %@:%@", [self getLocalizedDateStringForMonth:month day:day], [self getWeekDayName:weekday] ,hourString, minuteString];
        } else { // 영어
            
            NSString *monthString = [self getMonthName:month];
            monthString = [monthString substringWithRange:NSMakeRange(0, 3)];
            
            //ex) Sat 10 Jul 14:30
            return [NSString stringWithFormat:@"%@ %d %@ %@:%@", [self getWeekDayName:weekday], day, monthString, hourString, minuteString];
        }
    } else { // 요일 없이
        if([lang isEqualToString:@"KO"] || [lang isEqualToString:@"JA"]) { // 한국어, 일본어
            return [NSString stringWithFormat:@"%@ %@:%@", [self getLocalizedDateStringForMonth:month day:day], hourString, minuteString];
        } else { // 영어
            NSString *monthString = [self getMonthName:month];
            monthString = [monthString substringWithRange:NSMakeRange(0, 3)];
            
            //ex) Sat 10 Jul 14:30
            return [NSString stringWithFormat:@"%d %@ %@:%@", day, monthString, hourString, minuteString];
        }
    }
}

-(NSString *) getLocalizedDateStringForMonth:(int) month day:(int) day
{
    NSString *lang = [[FootplrHelper sharedFootplrHelper] getDeviceLocale];
    NSString *dateString = @"";
    if([lang isEqualToString:@"KO"]) {
        dateString = [NSString stringWithFormat:@"%d월 %d일", month, day];
    } else if([lang isEqualToString:@"JA"]) {
        dateString = [NSString stringWithFormat:@"%d月 %d日", month, day];
    } else {
        
    }
    return dateString;
}

- (NSString*) getWeekDayName:(NSInteger)weekday
{
    NSArray* weekdayArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"s_46", nil), NSLocalizedString(@"s_40", nil), NSLocalizedString(@"s_41", nil), NSLocalizedString(@"s_42", nil), NSLocalizedString(@"s_43", nil), NSLocalizedString(@"s_44", nil), NSLocalizedString(@"s_45", nil), nil];
    return  [weekdayArray objectAtIndex:weekday - 1];
}

- (NSString*) getMonthName:(int)month
{
//    NSArray* monthArray = [[NSArray alloc] initWithObjects:@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December", nil];
    NSArray* monthArray = @[
                            NSLocalizedString(@"s_49", nil),
                            NSLocalizedString(@"s_50", nil),
                            NSLocalizedString(@"s_51", nil),
                            NSLocalizedString(@"s_52", nil),
                            NSLocalizedString(@"s_53", nil),
                            NSLocalizedString(@"s_54", nil),
                            NSLocalizedString(@"s_55", nil),
                            NSLocalizedString(@"s_56", nil),
                            NSLocalizedString(@"s_57", nil),
                            NSLocalizedString(@"s_58", nil),
                            NSLocalizedString(@"s_59", nil),
                            NSLocalizedString(@"s_60", nil)
                            ];
    return  [monthArray objectAtIndex:month - 1];
}

- (NSUInteger) getMonth:(NSDate*) date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSWeekdayCalendarUnit | NSSecondCalendarUnit | NSMinuteCalendarUnit | NSHourCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    return [components month];
}

- (NSUInteger) getDay:(NSDate *) date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSWeekdayCalendarUnit | NSSecondCalendarUnit | NSMinuteCalendarUnit | NSHourCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    return [components day];
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}
@end
