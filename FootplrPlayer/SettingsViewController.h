//
//  SettingsViewController.h
//  footplr
//
//  Created by EclipseKim on 2015. 5. 28..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrKeyboardViewController.h"
#import <MessageUI/MessageUI.h>
#import <sys/utsname.h>
#import "ImageHelper.h"

@interface SettingsViewController : FootplrKeyboardViewController <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, ImageHelperDelegate>
{
    ImageHelper *imageHelper;
    UIImageView *profileImageView;

    UILabel *nameLabel;
    UILabel *positionLabel;
    
    NSMutableDictionary *userDetailInfo;
    
    BOOL isLoading;
    
    UIDatePicker *datePicker;
    UIPickerView *picker;
    
    UITextField *heightTextField;
    UITextField *weightTextField;
    UITextField *birthdayTextField;
    UITextField *mainFootTextField;
    
    UITextField *loginIdTextField;
    UITextField *emailTextField;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property BOOL hideUserInfo;

@end
