//
//  RatingStatsCell.m
//  footplr
//
//  Created by EclipseKim on 2015. 5. 31..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "RatingStatsCell.h"

@implementation RatingStatsCell
@synthesize titleLabel, scheduleDescriptionLabel, scheduleTitleLabel, ratingLabel, starFillView;


-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        [self setBackgroundColor:[UIColor clearColor]];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        
        //titleLabel
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300,15)];
        titleLabel.font = [UIFont systemFontOfSize:11.0f];
        titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [self.contentView addSubview:titleLabel];
        
        // scheduleTitleLabel
        scheduleTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 35, 220, 20)];
        scheduleTitleLabel.font = [UIFont systemFontOfSize:15.0f];
        scheduleTitleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [self.contentView addSubview:scheduleTitleLabel];
        
        // scheduleDescriptionLabel
        scheduleDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 220, 30)];
        scheduleDescriptionLabel.font = [UIFont systemFontOfSize:11.0f];
        scheduleDescriptionLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WC];
        scheduleDescriptionLabel.numberOfLines = 2;
        scheduleDescriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:scheduleDescriptionLabel];
        
        // ratingLabel
        ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, 50, 60, 18)];
        ratingLabel.font = [UIFont systemFontOfSize:18.0f];
        ratingLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        ratingLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:ratingLabel];
        
        // ratingView
        
        _ratingView = [[UIView alloc] initWithFrame:CGRectMake(258, 75, 44, 8)];
        [_ratingView setBackgroundColor:[UIColor whiteColor]];
        
        starFillView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 18, 8)];
        [starFillView setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:255 withGreen:198 withBlue:0]];
        [_ratingView addSubview: starFillView];
        
        UIImageView *starImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 44, 8)];
        starImageView.image = [UIImage imageNamed:@"schedule_star"];
        starImageView.contentMode = UIViewContentModeScaleAspectFit;
        [_ratingView addSubview:starImageView];
        [self.contentView addSubview:_ratingView];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
