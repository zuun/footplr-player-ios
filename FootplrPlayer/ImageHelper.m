//
//  ImageHelper.m
//  Footplr
//
//  Created by JunhyuckKim on 2014. 10. 8..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "ImageHelper.h"

@implementation ImageHelper


//#pragma mark - UIAlertViewDelegate
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if(alertView.tag == IMAGE_SELECTION_ALERTVIEW_TAG) {
//        if(buttonIndex == 1) { // 카메라
//            [self getImageFromCamera];
//        } else if(buttonIndex == 2) { // 사진 앨범
//            [self getImageFromPhotoLibrary];
//        }
//    }
//}


- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

#pragma mark - UIActionSheetDelegate

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0) {
        [self getImageFromCamera];
    } else if(buttonIndex == 1) {
        [self getImageFromPhotoLibrary];
    }
}

#pragma mark - Image

-(void) showImageSelection
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"s_245", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"s_244", nil), NSLocalizedString(@"s_243", nil), nil];
    [sheet showInView:_viewController.view];
    
}

-(void) getImageFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [picker.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        [picker.navigationBar setBarTintColor:nil];
        
        [_viewController presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[FootplrHelper sharedFootplrHelper] getAlertViewTitle] 
                                                        message:@"Error to access Camera"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}

-(void) getImageFromPhotoLibrary
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [picker.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        [picker.navigationBar setBarTintColor:nil];
        
        [_viewController presentViewController:picker animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[FootplrHelper sharedFootplrHelper] getAlertViewTitle]
                                                        message:@"Error to access PhotoLibrary"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - ImagePickerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo NS_DEPRECATED_IOS(2_0, 3_0)
{
    [self uploadImage:image];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

}


#pragma mark - imageUpload API
-(void) uploadImage:(UIImage *) image
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    int isSquare = _isSquare ? 1 : 0;
    NSDictionary *parameters = @{
                                 @"width": [NSNumber numberWithFloat:image.size.width],
                                 @"height" : [NSNumber numberWithFloat:image.size.height],
                                 @"square" : [NSNumber numberWithInt:isSquare]
                                 };
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    [manager POST:[NSString stringWithFormat:@"%@%@", MANAGER_API_URL, [NSString stringWithFormat:@"/api/common/file_upload/%d", isSquare]] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"userfile" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
            NSDictionary* uploadedInfo = [[responseObject objectForKey:@"result"] objectForKey:@"upload_data"];
            
            if(isSquare == 0) {
                if([_delegate respondsToSelector:@selector(rectImageUploadCompleted:withUploadedInfo:)]) {
                    [_delegate rectImageUploadCompleted:image withUploadedInfo:uploadedInfo];
                }
            } else {
                if([_delegate respondsToSelector:@selector(squareImageUploadCompleted:withUploadedInfo:)]) {
                    [_delegate squareImageUploadCompleted:image withUploadedInfo:uploadedInfo];
                }
            }
//            [self modifyTeamInfo:@{@"profile_pic":imageUrl}];
        } else {
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

#pragma mark - resize
-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, YES, [UIScreen mainScreen].scale);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage*) resizeImage:(UIImage*) image withMaxWidth:(CGFloat) MAX_WIDTH withMaxHeight:(CGFloat) MAX_HEIGHT
{
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    //    NSLog(@"%f %f", image.size.width, image.size.height);
    
    // crop image
    if(width > MAX_WIDTH) {
        height = height * MAX_WIDTH / width;
        width = MAX_WIDTH;
    }
    if( height > MAX_HEIGHT) {
        width = width * MAX_HEIGHT / height;
        height = MAX_HEIGHT;
    }
    
    image = [self imageWithImage:image scaledToSize:CGSizeMake(width, height)];
    //    NSLog(@"resized %f %f", image.size.width, image.size.height);
    
    return image;
}

-(UIImage*) resizeImage:(UIImage*) image withMinWidth:(CGFloat) MIN_WIDTH withMinHeight:(CGFloat) MIN_HEIGHT
{
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    
    CGFloat xPortion = MIN_WIDTH / width;
    CGFloat yPortion = MIN_HEIGHT / height;
    if(xPortion > yPortion) {
        image = [self imageWithImage:image scaledToSize:CGSizeMake(width * xPortion, height * xPortion)];
    } else {
        image = [self imageWithImage:image scaledToSize:CGSizeMake(width * yPortion, height * yPortion)];
    }
    return image;
}


-(UIImage*) resizeImage:(UIImage*) image withWidth:(CGFloat) width withHeight:(CGFloat) height
{
    image = [self imageWithImage:image scaledToSize:CGSizeMake(width, height)];
    return image;
}
@end
