//
//  MatchDetailViewController.h
//  FootplrPlayer
//
//  Created by EclipseKim on 2015. 5. 5..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"
#import "RateViewController.h"

@interface MatchDetailViewController : FootplrViewController <UITableViewDataSource, UITableViewDelegate, RateViewControllerDelegate, UITextFieldDelegate>
{
    UIView *customNavBar;
    
    BOOL showGoals;

    NSDictionary *momInfo;
    NSMutableArray *myRatings;
    
    BOOL isLoading;
    BOOL isDone;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) Schedule *schedule;
@end
