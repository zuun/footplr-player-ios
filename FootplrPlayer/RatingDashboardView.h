//
//  RatingDashboardView.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 26..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMSimpleLineGraphView.h"

@interface RatingDashboardView : UIView <BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate>
{
    UILabel *nameLabel;
    UILabel *ratingLabel;
    UIView *ratingStarBgFill;
    
    CGFloat rating;
    NSArray *recentRatings;
    NSString *noDataLabelText;
    BEMSimpleLineGraphView *ratingChart ;
}
-(void) setRating:(CGFloat) rating withRecentRatings:(NSArray *) ratings;

@end
