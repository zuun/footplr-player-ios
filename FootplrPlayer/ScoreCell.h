//
//  ScoreCell.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreCell : UITableViewCell

@property (strong, nonatomic) UILabel *homeScoreLabel;
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *awayScoreLabel;
@end
