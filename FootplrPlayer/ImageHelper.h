//
//  ImageHelper.h
//  Footplr
//
//  Created by JunhyuckKim on 2014. 10. 8..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ImageHelperDelegate <NSObject>

-(void) squareImageUploadCompleted:(UIImage *)image withUploadedInfo:(NSDictionary *)uploadedInfo;
-(void) rectImageUploadCompleted:(UIImage *)image withUploadedInfo:(NSDictionary *)uploadedInfo;
@end

static const NSInteger IMAGE_SELECTION_ALERTVIEW_TAG = 120;


@interface ImageHelper : NSObject <UIAlertViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>


-(void) showImageSelection;
@property BOOL isSquare;
@property (nonatomic, assign) id<ImageHelperDelegate> delegate;
@property (strong, nonatomic) UIViewController *viewController;
-(UIImage*) resizeImage:(UIImage*) image withMaxWidth:(CGFloat) MAX_WIDTH withMaxHeight:(CGFloat) MAX_HEIGHT;
-(UIImage*) resizeImage:(UIImage*) image withMinWidth:(CGFloat) MIN_WIDTH withMinHeight:(CGFloat) MIN_HEIGHT;
-(UIImage*) resizeImage:(UIImage*) image withWidth:(CGFloat) width withHeight:(CGFloat) height;
@end
