//
//  ScheduleCell.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 20..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingResultView.h"

@interface ScheduleCell : UITableViewCell

@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *dayLabel;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *descriptionLabel;        
@property (strong, nonatomic) UIImageView *divisionLineImageView;
@property (strong, nonatomic) UILabel *scoreResultLabel;
@property (strong, nonatomic) UIButton *rateButton;
@property (strong, nonatomic) UIImageView *scheduleTypeImageView;
@property (strong, nonatomic) RatingResultView *ratingView;
@end
