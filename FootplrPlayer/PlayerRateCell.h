//
//  PlayerRateCell.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 15..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"
@interface PlayerRateCell : UITableViewCell

@property (nonatomic, retain) UIImageView *avatarImageView;
@property (nonatomic, retain) UILabel *playerNameLabel;
@property (nonatomic, retain) UILabel *positionLabel;
@property (nonatomic, retain) RatingView *ratingView;
@end
