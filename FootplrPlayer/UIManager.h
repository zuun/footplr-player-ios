//
//  UIManager.h
//  footplr
//
//  Created by EclipseKim on 2015. 5. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>

enum colorTypes {
    COLOR_BA = 1,
    COLOR_BB,
    COLOR_BC,
    COLOR_BD,
    COLOR_WA,
    COLOR_WB,
    COLOR_WC,
    COLOR_WD,
    COLOR_RED_,
    COLOR_YELLOW_,
    COLOR_LIST_BG,
    COLOR_LIST_SEPERATOR,
    COLOR_BG,
    COLOR_BLUE_,
    COLOR_GREEN_
};

enum fontWeights {
    FONT_LIGHT,
    FONT_MEDIUM,
    FONT_BOLD,
    FONT_SYSTEM
};

@interface UIManager : NSObject

+ (UIManager *)sharedUIManager;
- (UIView *)addTopBorderToView:(UIView *)v withBorder:(CGFloat)borderWidth withColor:(UIColor *)color;
- (UIView *)addTopBorderToView:(UIView *)v withBorder:(CGFloat)borderWidth withColor:(UIColor *)color withLength:(CGFloat) length;
- (UIView *)addBottomBorderToView:(UIView *)v withBorder:(CGFloat)borderWidth withColor:(UIColor *)color;
- (void) setLeftViewToTextField:(UITextField *)v withImageName:(NSString *)imageName;
- (void) setRightViewToTextField:(UITextField *)v withImageName:(NSString *)imageName;
- (void) setDeleteButtonRightViewToTextField:(UITextField *)v;

// COLOR
-(UIColor*) appColor:(int)color;
-(UIColor*) colorWithHexCode:(int)rgbValue;
-(UIColor *) colorWithRed:(int)red withGreen:(int)green withBlue:(int)blue;

// FONT
-(UIFont *) fontWithWeight:(int)weight withSize:(CGFloat)size;
@end
