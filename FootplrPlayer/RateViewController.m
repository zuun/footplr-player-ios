//
//  RateViewController.m
//  FootplrPlayer
//
//  Created by EclipseKim on 2015. 5. 5..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "RateViewController.h"
#import "MainViewController.h"
#import "PositionHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RateViewController ()

@end

@implementation RateViewController
@synthesize players;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    positionHelper = [PositionHelper sharedPositionHelper];
    [self _init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) _init
{
    if(_schedule) {
        if(_schedule.attendedPlayers) {
            players = [[NSMutableArray alloc] initWithArray:_schedule.attendedPlayers];
//            [players removeObjectAtIndex:0]; // remove me
            [_tableView reloadData];
            [self initRatings];
        } else {
            [self loadScheduleDetail];
        }
    } else if(!_schedule && !players) {
        [self getPlayers];
    }
    
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"s64", nil)];

    [self.view setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BA]];
    [self.tableView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BA]];
    
    UIBarButtonItem* doneBtn = [UIBarButtonItem barItemWithImageName:@"nav_confirm_n" target:self action:@selector(rate)];
    [self setRightBarButtons:@[doneBtn]];

    
    // UITableView Header
    CGFloat width = self.view.frame.size.width;
    UIView *tableHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 116)];
    [tableHeader setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, width, 60)];
    titleLabel.text = NSLocalizedString(@"s58", nil);
    titleLabel.font = [UIFont systemFontOfSize:24.0f];
    titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [titleLabel sizeToFit];
    [titleLabel setFrame:CGRectMake(20, 30, 280, 60)];
    [tableHeader addSubview:titleLabel];
    
    _tableView.tableHeaderView = tableHeader;

}

#pragma mark - API

-(void) loadScheduleDetail
{
    NSString *localeCode = [[FootplrHelper sharedFootplrHelper] getDeviceLocale];
    [[ApiHelper sharedApiHelper] getRequestWithUrl:MANAGER_API_URL withPath:[NSString stringWithFormat:@"/api/match/detail/%d/%@", _schedule.matchId, localeCode] withParameters:nil withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"sdsd : %@", responseObject);
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
            NSDictionary *result = [responseObject objectForKey:@"result"];
            
            NSArray *goals = [result objectForKey:@"goal"];
            _schedule.scores = [goals mutableCopy];
            
            [_schedule setAttendedPlayers:[result objectForKey:@"attend"]];
            players = [[NSMutableArray alloc] initWithArray:_schedule.attendedPlayers];
            [self initRatings];
//            [players removeObjectAtIndex:0]; // remove me
            
            NSDictionary *matchInfo = [result objectForKey:@"match"];
            _schedule.matchInfo = matchInfo;
            _schedule.memo = [matchInfo objectForKey:@"memo"];
            
            [_tableView reloadData];
        }
    }];
}

-(void) getPlayers
{
//    NSLog(@"%@", [NSString stringWithFormat:@"/api/member/all/%d/%@", _teamId, @"ko"]);
    NSString *localeCode = [[FootplrHelper sharedFootplrHelper] getDeviceLocale];
    [[ApiHelper sharedApiHelper]
     getRequestWithUrl:MANAGER_API_URL
     withPath:[NSString stringWithFormat:@"/api/member/all/%d/%@", _teamId, localeCode]
     withParameters:nil
     withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"players : %@", responseObject);
         if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS){ // 성공
             NSArray *_players = [[responseObject objectForKey:@"result"] mutableCopy];
             players = [NSMutableArray new];
             
             int userId = [[[[NSUserDefaults standardUserDefaults] objectForKey:USER_INFO] objectForKey:@"user_id"] intValue];
             for(NSDictionary *playerInfo in _players) {
                 Player *p = [[Player alloc] initWithPlayerInfo:playerInfo];
//                 if(p.userId == userId) continue; // skip me
                 [players addObject:p];
             }
             [self initRatings];
             [_tableView reloadData];
         }
     }];
}

-(void) initRatings
{
    ratings = [NSMutableArray new];
    for(int i = 0 ; i < [players count] ; i++) {
        [ratings addObject:[NSNumber numberWithFloat:INITIAL_RATING]];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [players count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *playerRateCellReuseId = @"PlayerRateCell";
    PlayerRateCell *cell = [tableView dequeueReusableCellWithIdentifier:playerRateCellReuseId];
    if(!cell) {
        cell = [[PlayerRateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:playerRateCellReuseId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    Player *p = [players objectAtIndex:indexPath.row];
    cell.playerNameLabel.text = [NSString stringWithFormat:@"%d %@", p.backNumber, p.name];
    cell.positionLabel.text = p.position;
    cell.positionLabel.text = [positionHelper getPositionNameByCode:p.positionCode];
    
    [cell.avatarImageView sd_setImageWithURL:p.avatarUrl placeholderImage:[UIImage imageNamed:@"default_profile_photo"]];
   
    CGPoint rateViewCenter = cell.ratingView.center;
    rateViewCenter.x = self.view.frame.size.width / 2;
    cell.ratingView.center = rateViewCenter;
    cell.ratingView.tag = indexPath.row;
    cell.ratingView.delegate = self;

    CGFloat rating = [[ratings objectAtIndex:indexPath.row] floatValue];
    [cell.ratingView setRating:rating];
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 124.0f;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

#pragma mark - Actions

-(void) rate
{
    NSMutableArray *members = [NSMutableArray new];
    
    for(Player *player in players) {
        [members addObject:[NSNumber numberWithInt:player.memberId]];
    }
    NSDictionary *params = @{
                             @"member_id" : members,
                             @"rate" : ratings
                             };
    NSString *path = [NSString stringWithFormat:@"/api/team/member_rate/%d/2", _teamId];
    if(_schedule) {
        path = [NSString stringWithFormat:@"/api/match/rate/%d/2", _schedule.matchId];
    }
    [[ApiHelper sharedApiHelper]
     postRequestWithUrl:API_URL
     withPath:path
     withParameters:params
     withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"%@", responseObject);

         if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue]  == API_SUCCESS) {
             if(_schedule) {
                 _schedule.rated = YES;
                 
                 CGFloat selfRating = 0;
                 for(int i = 0 ; i < [players count] ; i++) {
                     Player *p = [players objectAtIndex:i];
                     if(p.userId == [[[[NSUserDefaults standardUserDefaults] objectForKey:USER_INFO] objectForKey:@"user_id"] intValue]) {
                         selfRating = [[ratings objectAtIndex:i] floatValue];
                         break;
                     }
                 }
                 [_schedule addRating:selfRating];
                 
                 [self.navigationController popViewControllerAnimated:YES];
                 [_delegate rated];
             } else { // base rating
                 MainViewController *mvc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainStoryboard"];
                 [self.navigationController pushViewController:mvc animated:YES];
             }
         } else {
             [[FootplrHelper sharedFootplrHelper] showError];
         }
    }];
}

#pragma mark - RatingViewDelegate

-(void) ratingChanged:(CGFloat)rating forTag:(NSInteger) tag
{
    [ratings replaceObjectAtIndex:tag withObject:[NSNumber numberWithFloat:rating]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
