//
//  Player.m
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 4. 21..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "Player.h"

@implementation Player

/*
 * Initializers
 */


-(id) initWithPlayerInfo:(NSDictionary *) _playerInfo
{
    if(self = [super init]) {
        _backNumber = [[_playerInfo objectForKey:@"back_no"] intValue];
        _memberId = [[_playerInfo objectForKey:@"member_id"] intValue];
        _userId = [[_playerInfo objectForKey:@"user_id"] intValue];
        
        _positionCode = [_playerInfo objectForKey:@"position"];
        _name = [_playerInfo objectForKey:@"member_nm"];
        
        // ratings
        ratingCounts = [[_playerInfo objectForKey:@"rate_cnt"] intValue];
        ratingSum = [[_playerInfo objectForKey:@"rate_sum"] floatValue];
        _ratings = ratingCounts == 0 ? 0 : ratingSum / ratingCounts;
        
        NSString *avatarPath = [_playerInfo objectForKey:@"profile_pic"];
        _avatarUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://footplr.com%@", avatarPath]];
        
    }
    return self;
}
@end