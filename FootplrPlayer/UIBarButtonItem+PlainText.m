//
//  UIBarButtonItem+PlainText.m
//  Footplr
//
//  Created by EclipseKim on 2014. 7. 13..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "UIBarButtonItem+PlainText.h"

@implementation UIBarButtonItem (PlainText)

+ (UIBarButtonItem*)barItemWithFrame:(CGRect)frame withTitle:(NSString*)title target:(id)target action:(SEL)action{
    UIButton* customBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBarButton setFrame:frame];
    [customBarButton setBackgroundColor:[UIColor clearColor]];
    [customBarButton.titleLabel setTextAlignment:NSTextAlignmentRight];
    [customBarButton.titleLabel setFont:[UIFont systemFontOfSize:17.0f]];
    [customBarButton.titleLabel sizeToFit];
    [customBarButton setTitle:title forState:UIControlStateNormal];
    [customBarButton setFrame:CGRectMake(320 - customBarButton.titleLabel.frame.size.width, 0, customBarButton.titleLabel.frame.size.width, 44)];
    [customBarButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
//    customBarButton.titleLabel.textColor = [UIColor colorWithRed:56/255.0f green:59/255.0f blue:67/255.0f alpha:1.0];
    customBarButton.titleLabel.textColor = [UIColor whiteColor];
    UIBarButtonItem* barButton = [[UIBarButtonItem alloc] initWithCustomView:customBarButton];
    barButton.tag = BAR_BUTTON_PLAIN_TEXT_TAG;
    return barButton;
}

+ (UIBarButtonItem*)barItemWithTitle:(NSString*)title target:(id)target action:(SEL)action{
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
//    barButton.tintColor = [[FootplrHelper sharedFootplrHelper] colorWithRed:56 withGreen:59 withBlue:67];
    barButton.tintColor = [UIColor whiteColor];
    barButton.tag = BAR_BUTTON_PLAIN_TEXT_TAG;
    return barButton;
}
@end
