//
//  SessionManager.h
//  Footplr
//
//  Created by EclipseKim on 2014. 8. 10..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeychainItemWrapper.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@protocol SessionDelegate <NSObject>

-(void) showLoginViewController;
-(void) loginComplete:(NSDictionary*) userInfo;
-(void) needJoin:(NSString *) accessToken;
-(void) passwordChanged:(int) status;
@end

static NSString *USER_INFO = @"user_info";
static NSString *TEAM_INFO = @"team_info";

@interface SessionManager : NSObject
{
    NSUserDefaults* userData;
    KeychainItemWrapper *keychainItem;
}

@property (nonatomic, assign) id<SessionDelegate> delegate;
+(SessionManager*)sharedSessionManager;


-(void) joinWithId:(NSString *)userId withEmail:(NSString*)email withPassword:(NSString*)password withAccessToken:(NSString *) accessToken;
-(void) loginWithFb:(NSString *) accessToken;
-(void) loginWithId:(NSString*)userId withPassword:(NSString*) password;
-(void) sessionLogin;
-(void) logout;
-(void) changePassword:(NSString *)currentPw withNewPassword:(NSString *)newPw;

-(int) getCurrentTeamId;

-(BOOL) isValidId:(NSString *)userId;
-(BOOL) isValidEmail:(NSString *)email;
-(BOOL) isValidPassword:(NSString *)password;
-(NSString*) shaEncode:(NSString*)input;
@end
