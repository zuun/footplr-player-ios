//
//  ForgotPasswordViewController.h
//  footplr
//
//  Created by EclipseKim on 2015. 5. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"


@interface ForgotPasswordViewController : FootplrViewController < UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UILabel *guideLabel;
@end
