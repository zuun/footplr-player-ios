//
//  PositionHelper.h
//  Footplr
//
//  Created by JunhyuckKim on 14. 8. 18..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef __PositionHelper__
#define __PositionHelper__

#define POSITION_GK @"GK"
#define POSITION_DF @"DF"
#define POSITION_MF @"MF"
#define POSITION_FW @"FW"

#endif

static NSArray *positionCodes;
@interface PositionHelper : NSObject

+ (PositionHelper*)sharedPositionHelper;
+(void) loadPositions;
-(UIColor*) getPositionColorByPositionType:(NSString*) positionType;
-(NSString*) getPositionTypeByCode:(NSString*) positionCode;
-(NSArray*) getPositionCodes;
-(NSString *)getPositionNameByCode:(NSString *)positionCode;
@end
