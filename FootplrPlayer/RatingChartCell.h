//
//  RatingChartCell.h
//  footplr
//
//  Created by EclipseKim on 2015. 6. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingChartBar.h"

@interface RatingChartCell : UITableViewCell

@property NSMutableArray *ratingBars;
@end
