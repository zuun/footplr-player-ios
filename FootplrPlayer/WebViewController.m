//
//  WebViewController.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 16..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    url = [NSURL URLWithString:_urlString];
    [self _init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) _init
{
    [self setNavigationBackButton];
    
    
    [[FootplrHelper sharedFootplrHelper] logFrame:self.view.frame];
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, CGRectGetHeight(self.view.frame) - 44)];
    [webView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    [self.view addSubview:webView];
    
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
