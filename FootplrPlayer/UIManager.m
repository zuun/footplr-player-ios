//
//  UIManager.m
//  footplr
//
//  Created by EclipseKim on 2015. 5. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "UIManager.h"


@implementation UIManager

static UIManager *sharedUIManager;
+ (UIManager *)sharedUIManager {
    if (sharedUIManager == nil) {
        sharedUIManager = [[UIManager alloc] init];
    }
    return sharedUIManager;
}

+(id)alloc
{
    @synchronized([UIManager class])
    {
        NSAssert(sharedUIManager == nil, @"Attempted to allocate a second instance of a singleton.");
        sharedUIManager = [super alloc];
        
        return sharedUIManager;
    }
    
    return nil;
}


- (UIView *)addTopBorderToView:(UIView *)v withBorder:(CGFloat)borderWidth withColor:(UIColor *)color
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    
    border.frame = CGRectMake(0, 0, v.frame.size.width, borderWidth);
    [v.layer addSublayer:border];
    return v;
}

- (UIView *)addTopBorderToView:(UIView *)v withBorder:(CGFloat)borderWidth withColor:(UIColor *)color withLength:(CGFloat)length
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    
    border.frame = CGRectMake((v.frame.size.width - length) / 2, 0,  length, borderWidth);
    [v.layer addSublayer:border];
    return v;
}

- (UIView *)addBottomBorderToView:(UIView *)v withBorder:(CGFloat)borderWidth withColor:(UIColor *)color
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;

    border.frame = CGRectMake(0, v.frame.size.height - borderWidth, v.frame.size.width, borderWidth);
    [v.layer addSublayer:border];
    return v;
}

- (void) setLeftViewToTextField:(UITextField *)v withImageName:(NSString *)imageName
{
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(1, 1, 46, 46)];
    UIImageView *iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    iconImageView.contentMode = UIViewContentModeCenter;
    [leftView addSubview:iconImageView];
    iconImageView.center = leftView.center;
    v.leftView = leftView;
    v.leftViewMode = UITextFieldViewModeAlways;
}

- (void) setRightViewToTextField:(UITextField *)v withImageName:(NSString *)imageName
{
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(1, 1, 46, 46)];
    UIImageView *iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [leftView addSubview:iconImageView];
    iconImageView.center = leftView.center;
    v.rightView = leftView;
    v.rightViewMode = UITextFieldViewModeWhileEditing;
}

- (void) setDeleteButtonRightViewToTextField:(UITextField *)v
{
    UIButton *deleteAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteAllButton setBackgroundImage:[UIImage imageNamed:@"textfield_delete_p"] forState:UIControlStateNormal];
    [deleteAllButton setFrame:CGRectMake(12, 12, 24, 24)];
    [deleteAllButton addTarget:self action:@selector(deleteText:) forControlEvents:UIControlEventTouchUpInside];
    v.rightView = deleteAllButton;
    v.rightViewMode = UITextFieldViewModeWhileEditing;
}

-(void) deleteText:(UIButton *)button
{
    UITextField *v = (UITextField *)button.superview;
    v.text = @"";
}

-(UIColor*) appColor:(int)_color
{
    UIColor *color;
    switch (_color) {
        case COLOR_BA:
            color = [self colorWithHexCode:0x25282A];
            break;
        case COLOR_BB:
            color = [[self colorWithHexCode:0x25282A] colorWithAlphaComponent:0.7f];
            break;
        case COLOR_BC:
            color = [[self colorWithHexCode:0x25282A] colorWithAlphaComponent:0.55f];
            break;
        case COLOR_BD:
            color = [[self colorWithHexCode:0x25282A] colorWithAlphaComponent:0.2f];
            break;
        case COLOR_WA:
            color = [UIColor whiteColor];
            break;
        case COLOR_WB:
            color = [[UIColor whiteColor] colorWithAlphaComponent:0.7f];
            break;
        case COLOR_WC:
            color = [[UIColor whiteColor] colorWithAlphaComponent:0.55f];
            break;
        case COLOR_WD:
            color = [[UIColor whiteColor] colorWithAlphaComponent:0.2f];
            break;
        case COLOR_RED_:
            color = [self colorWithHexCode:0xf04951];
            break;
        case COLOR_YELLOW_:
            color = [self colorWithHexCode:0xffc600];
            break;
        case COLOR_LIST_BG :
            color = [UIColor whiteColor];
            break;
        case COLOR_LIST_SEPERATOR:
            color = [self colorWithHexCode:0xdddddd];
            break;
        case COLOR_BG:
            color = [self colorWithHexCode:0x25282a];
            break;
        case COLOR_BLUE_ :
            color = [self colorWithHexCode:0x32abea];
            break;
        case COLOR_GREEN_ :
            color = [self colorWithHexCode:0x18c37c];
            break;
        default:
            color = nil;
            break;
    }
    return color;
}

-(UIColor*) colorWithHexCode:(int)rgbValue
{
    return [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0];
}

-(UIColor *) colorWithRed:(int)red withGreen:(int)green withBlue:(int)blue
{
    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1.0f];
}

#pragma mark - FONT

-(UIFont *) fontWithWeight:(int)weight withSize:(CGFloat)size
{
    UIFont *font = [UIFont systemFontOfSize:size];
    switch(weight) {
        case FONT_LIGHT:
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:size];
            break;
        case FONT_MEDIUM:
            font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
            break;
        case FONT_BOLD:
            font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
            break;
        case FONT_SYSTEM:
        default:
            font = [UIFont systemFontOfSize:size];
            
    }
    return font;
}

@end
