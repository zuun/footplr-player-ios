//
//  FootplrKeyboardViewController.h
//  Footplr
//
//  Created by JunhyuckKim on 14. 7. 16..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "FootplrViewController.h"

#import "UIView+FormScroll.h"

@interface FootplrKeyboardViewController : FootplrViewController <UITextFieldDelegate>
{
    UIScrollView *inputWrapperScrollView;
    CGRect inputWrapperScrollViewFrame;
}
-(void) setInputWrapperScrollView:(UIScrollView *) scrollView;
-(void) showLoginViewController;
-(void) showKeyboard;
-(void) hideKeyboard;
@end
