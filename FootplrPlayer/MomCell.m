//
//  MomCell.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 11..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "MomCell.h"

@implementation MomCell
@synthesize momLabel;


-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        [self setBackgroundColor:[UIColor clearColor]];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView setFrame:CGRectMake(0, 0, 320, 200)];
        
        UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"match_mom_back"]];
        [bgImageView setFrame:self.contentView.frame];
        [self.contentView addSubview:bgImageView];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"match_mom"]];
        [imageView setFrame:CGRectMake(106, 66, 107, 24)];
        [self.contentView addSubview:imageView];
        
        momLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 116, 300, 20)];
        momLabel.textAlignment = NSTextAlignmentCenter;
        momLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        momLabel.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:momLabel];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
