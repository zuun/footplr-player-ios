//
//  ForgotPasswordViewController.m
//  footplr
//
//  Created by EclipseKim on 2015. 5. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self _init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) _init
{
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"s39", nil)];
    
    UIBarButtonItem* doneBtn = [UIBarButtonItem barItemWithImageName:@"nav_confirm_n" target:self action:@selector(sendTempPw)];
    [self setRightBarButtons:@[doneBtn]];
    
    // emailTextfield
    _emailTextField.placeholder = NSLocalizedString(@"s40", nil);
    _emailTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _emailTextField.font = [UIFont systemFontOfSize:14.0f];
    [_emailTextField setBackgroundColor:[UIColor whiteColor]];
    [[UIManager sharedUIManager] setLeftViewToTextField:_emailTextField withImageName:@"textfield_ic_password"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_emailTextField];
    
    // guideLabel
    _guideLabel.text = NSLocalizedString(@"s41", nil);
    CGRect guideLabelFrame = _guideLabel.frame;
    guideLabelFrame.origin.x = 20;
    guideLabelFrame.size.width = 280;
    _guideLabel.frame = guideLabelFrame;
    
    _guideLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WC];
    _guideLabel.font = [UIFont systemFontOfSize:14.0f];
    _guideLabel.numberOfLines = 0;
    _guideLabel.textAlignment = NSTextAlignmentCenter;
    [_guideLabel sizeToFit];
}

#pragma mark - Actions

-(void) sendTempPw
{
    NSString *text = _emailTextField.text;
    NSString *key = [text containsString:@"@"] ? @"email" : @"id";
    
    NSDictionary *params = @{
                             key : text
                             };
    
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/auth/send_passwd" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[FootplrHelper sharedFootplrHelper] getAlertViewTitle] message:NSLocalizedString(@"s43", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
            alert.tag = ALERT_NAV_POP_ON_CONFIRM;
            [alert show];
        } else if([[responseObject objectForKey:@"message"] isEqualToString:@"NO SUCH EMAIL"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[FootplrHelper sharedFootplrHelper] getAlertViewTitle] message:NSLocalizedString(@"s42", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

#pragma mark - UIAlertViewDelegate
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == ALERT_NAV_POP_ON_CONFIRM) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
