//
//  MatchHeaderCell.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 11..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "MatchHeaderCell.h"

@implementation MatchHeaderCell
@synthesize goalToggleButton;


-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        [self setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:163 withGreen:190 withBlue:199]];
        
        [self setFrame:CGRectMake(0, 0, 320, 320)];
        
        // cover image
        _coverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
        [self.contentView addSubview:_coverImageView];

        // cover shadow
        _coverShadowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
        [_coverShadowImageView setImage:[UIImage imageNamed:@"match_cover_shadow"]];
        [self.contentView addSubview:_coverShadowImageView];
        
        // titleLabel
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, 300, 15)];
        _titleLabel.font = [UIFont systemFontOfSize:11.0f];
        _titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_titleLabel];
        
        // subTitleLabel
        _subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 98, 300, 24)];
        _subTitleLabel.font = [UIFont systemFontOfSize:20.0f];
        _subTitleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        _subTitleLabel.numberOfLines = 0;
        _subTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_subTitleLabel];
        
        // homeScoreLabel
        _homeScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 115, 125, 60)];
        _homeScoreLabel.font = [UIFont systemFontOfSize:75.0f];
        _homeScoreLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        _homeScoreLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_homeScoreLabel];
        
        // awayScoreLabel
        _awayScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(190, 115, 125, 60)];
        _awayScoreLabel.font = [UIFont systemFontOfSize:75.0f];
        _awayScoreLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        _awayScoreLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_awayScoreLabel];
        
        // matchStatusLabel
        _matchStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(140, 130, 40, 40)];
        _matchStatusLabel.font = [UIFont boldSystemFontOfSize:15.0f];
        _matchStatusLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        _matchStatusLabel.textAlignment = NSTextAlignmentCenter;
        
        _matchStatusLabel.layer.cornerRadius = 20;
        _matchStatusLabel.layer.masksToBounds = YES;
        _matchStatusLabel.layer.borderWidth = 1.0f;
        _matchStatusLabel.layer.borderColor = [[[UIManager sharedUIManager] appColor:COLOR_WA] CGColor];
        [self.contentView addSubview:_matchStatusLabel];
        
        // homeTeamNameLabel
        _homeTeamNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 180, 125, 31)];
        _homeTeamNameLabel.font = [UIFont boldSystemFontOfSize:15.0f];
        _homeTeamNameLabel.textAlignment = NSTextAlignmentCenter;
        _homeTeamNameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [self.contentView addSubview:_homeTeamNameLabel];
        
        // awayTeamNameLabel
        _awayTeamNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(190, 180, 125, 31)];
        _awayTeamNameLabel.font = [UIFont boldSystemFontOfSize:15.0f];
        _awayTeamNameLabel.textAlignment = NSTextAlignmentCenter;
        _awayTeamNameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [self.contentView addSubview:_awayTeamNameLabel];
        
        
        // goalToggleButton
        goalToggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [goalToggleButton setBackgroundColor:[UIColor clearColor]];
        goalToggleButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        goalToggleButton.titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [goalToggleButton setFrame:CGRectMake(0, 232, 320, 88)];
        [self.contentView addSubview:goalToggleButton];
    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
