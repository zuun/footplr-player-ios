//
//  JoinViewController.m
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 5. 4..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "JoinViewController.h"
#import "TeamVerificationViewController.h"
#import "UserSelectionViewController.h"
#import "MainViewController.h"

@interface JoinViewController ()

@end

@implementation JoinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self _init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) _init
{
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"s14", nil)];
    
    UIBarButtonItem* doneBtn = [UIBarButtonItem barItemWithImageName:@"nav_confirm_n" target:self action:@selector(join)];
    [self setRightBarButtons:@[doneBtn]];
    
    // idTextField
    _idTextField.placeholder = NSLocalizedString(@"s15", nil);
    _idTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _idTextField.font = [UIFont systemFontOfSize:14.0f];
    [_idTextField setBackgroundColor:[UIColor whiteColor]];
    
    [[UIManager sharedUIManager] setLeftViewToTextField:_idTextField withImageName:@"textfield_ic_id"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_idTextField];
    [[UIManager sharedUIManager] addBottomBorderToView:_idTextField withBorder:0.8f withColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
    
    // emailTextField
    _emailTextField.placeholder = NSLocalizedString(@"s16", nil);
    _emailTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _emailTextField.font = [UIFont systemFontOfSize:14.0f];
    [_emailTextField setBackgroundColor:[UIColor whiteColor]];
    
    [[UIManager sharedUIManager] setLeftViewToTextField:_emailTextField withImageName:@"textfield_ic_mail"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_emailTextField];
    [[UIManager sharedUIManager] addBottomBorderToView:_emailTextField withBorder:0.4f withColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
    
    // pwTextField
    _pwTextField.placeholder = NSLocalizedString(@"s17", nil);
    _pwTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _pwTextField.font = [UIFont systemFontOfSize:14.0f];
    [_pwTextField setBackgroundColor:[UIColor whiteColor]];
    
    
    [[UIManager sharedUIManager] addBottomBorderToView:_pwTextField withBorder:0.4f withColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
    _pwTextField.secureTextEntry = YES;
    [[UIManager sharedUIManager] setLeftViewToTextField:_pwTextField withImageName:@"textfield_ic_password"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_pwTextField];
    
    // pwCheckTextField
    _pwCheckTextField.placeholder = NSLocalizedString(@"s18", nil);
    _pwCheckTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _pwCheckTextField.font = [UIFont systemFontOfSize:14.0f];
    [_pwCheckTextField setBackgroundColor:[UIColor whiteColor]];
    _pwCheckTextField.secureTextEntry = YES;
    
    [[UIManager sharedUIManager] setLeftViewToTextField:_pwCheckTextField withImageName:@"textfield_ic_password"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_pwCheckTextField];
}



-(void) join
{
    @try {
        [self checkInputs];
        [self checkPassword];
        
        NSString* userId = _idTextField.text;
        NSString* email = _emailTextField.text;
        NSString* password = _pwTextField.text;
        
        [[SessionManager sharedSessionManager] joinWithId:userId withEmail:email withPassword:password withAccessToken:_accessToken];
        [[SessionManager sharedSessionManager] setDelegate:self];
        
    } @catch (NSException* e){
        [[FootplrHelper sharedFootplrHelper] showFootplrAlertView:nil withMessage:[e reason]];
    } @finally {
        NSLog(@"finally");
    }
}

-(void) checkInputs
{
    NSString* userId = _idTextField.text;
    NSString* email = _emailTextField.text;
    NSString* password = _pwTextField.text;
    
    
    NSString* exceptionReason;
    if([StringHelper isEmptyString:userId]) exceptionReason = NSLocalizedString(@"s19", nil);
    else if([StringHelper isEmptyString:email]) exceptionReason = NSLocalizedString(@"s20", nil);
    else if([StringHelper isEmptyString:password]) exceptionReason = NSLocalizedString(@"s21", nil);
    
    if(exceptionReason) {
        NSException* e = [NSException
                          exceptionWithName:@"EmptyInputException"
                          reason:exceptionReason
                          userInfo:nil];
        @throw e;
    }
    
    //validation for userId
    if(![[SessionManager sharedSessionManager] isValidId:userId]) {
        NSLog(@"invalid ID");
        NSException* e = [NSException
                          exceptionWithName:@"EmptyInputException"
                          reason:NSLocalizedString(@"s22", nil)
                          userInfo:nil];
        @throw e;
    }
    
    //validation for email
    if(![[SessionManager sharedSessionManager] isValidEmail:email]) {
        NSException* e = [NSException
                          exceptionWithName:@"EmptyInputException"
                          reason:NSLocalizedString(@"s24", nil)
                          userInfo:nil];
        @throw e;
    }
    
    //validation for password
    if(![[SessionManager sharedSessionManager] isValidPassword:password]) {
        NSException* e = [NSException
                          exceptionWithName:@"EmptyInputException"
                          reason:NSLocalizedString(@"s25", nil)
                          userInfo:nil];
        @throw e;
    }
}

-(void) checkPassword
{
    NSString *pw = _pwTextField.text;
    NSString *pw2 = _pwCheckTextField.text;
    if(![pw isEqualToString:pw2]){
        NSException *e = [NSException
                          exceptionWithName:@"PasswordMismatchException"
                          reason:NSLocalizedString(@"s26", nil)
                          userInfo:nil];
        @throw e;
    }
}


#pragma mark - SessionDelegate
-(void)loginComplete:(NSDictionary *)userInfo
{
    NSString *teamId = [userInfo objectForKey:@"team_id"];
    NSString *userId = [userInfo objectForKey:@"user_id"];
    UIViewController *rootVC;
    if(teamId && [teamId isEqualToString:@""]) { // team not verified
        TeamVerificationViewController *tvvc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamVerificationStoryboard"];
        rootVC = tvvc;
    } else if(userId && [userId isEqualToString:@""]) { // member not selected
        UserSelectionViewController *usvc =  [self.storyboard instantiateViewControllerWithIdentifier:@"UserSelectionStoryboard"];
        usvc.teamId = [teamId intValue];
        rootVC = usvc;
    } else {
        MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainStoryboard"];
        mainVC.teamId = [teamId intValue];
        rootVC = mainVC;
    }
    
    [self.navigationController pushViewController:rootVC animated:YES];
}

@end
