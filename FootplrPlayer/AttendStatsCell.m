//
//  AttendStatsCell.m
//  footplr
//
//  Created by EclipseKim on 2015. 5. 31..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "AttendStatsCell.h"

@implementation AttendStatsCell
@synthesize titleLabel, attendedValueLabel, notAttendedValueLabel;

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        [self setBackgroundColor:[UIColor clearColor]];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        
        // attendedValueLabel
        attendedValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 14, 100, 22)];
        attendedValueLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_LIGHT withSize:22.0f];
        attendedValueLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        attendedValueLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:attendedValueLabel];
        
        // notAttendedValueLabel
        notAttendedValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(190, 14, 100, 22)];
        notAttendedValueLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_LIGHT withSize:22.0f];
        notAttendedValueLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        notAttendedValueLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:notAttendedValueLabel];
        
        //titleLabel
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(135, 17, 50, 16)];
        titleLabel.font = [UIFont systemFontOfSize:12.0f];
        titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:titleLabel];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
