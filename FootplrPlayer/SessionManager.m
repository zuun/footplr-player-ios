//
//  SessionManager.m
//  Footplr
//
//  Created by EclipseKim on 2014. 8. 10..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "SessionManager.h"
#import <CommonCrypto/CommonDigest.h>
#import "EntryViewController.h"

@implementation SessionManager
@synthesize delegate;
static SessionManager* sharedSessionManager = nil;

+(SessionManager*)sharedSessionManager
{
    @synchronized([SessionManager class])
    {
        if (!sharedSessionManager)
            sharedSessionManager= [[self alloc] init];
        
        return sharedSessionManager;
    }
    
    return nil;
}

+(id)alloc
{
    @synchronized([SessionManager class])
    {
        NSAssert(sharedSessionManager == nil, @"Attempted to allocate a second instance of a singleton.");
        sharedSessionManager = [super alloc];
        
        return sharedSessionManager;
    }
    
    return nil;
}

-(id)init
{
    self = [super init];
    userData = [NSUserDefaults standardUserDefaults];
//    keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:USER_INFO accessGroup:nil];
    return self;
}

-(void) sessionLogin
{
    NSString *sessionKey = [[userData objectForKey:USER_INFO] objectForKey:@"session_key"];
    // deviceInfo
    NSString *idFV = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *deviceType = [[FootplrHelper sharedFootplrHelper] getDeviceTypeForLog];
    if(!sessionKey) sessionKey = @"";
    NSDictionary *params = @{
                             @"session_key" : sessionKey,
                             @"uuid" : idFV,
                             @"device_type" : deviceType
                             };
    NSLog(@"ssparam: %@", params);
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/auth/login_sess" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"sessionkey login : %@", responseObject);
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) { // 성공
            [Flurry logEvent:@"SESSION LOGIN FAIL"];
            [self loginComplete:[responseObject objectForKey:@"user_info"]];
        } else { // 실패
            [userData removeObjectForKey:USER_INFO];
            [userData synchronize];
            
            if([delegate respondsToSelector:@selector(showLoginViewController)]) {
                [delegate showLoginViewController];
            }
            [Flurry logEvent:@"SESSION LOGIN FAIL"];
        }
    }];
}


-(void) loginWithId:(NSString*)userId withPassword:(NSString*) password
{
    password = [self shaEncode:password];
    
    // deviceInfo
    NSString *idFV = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *deviceType = [[FootplrHelper sharedFootplrHelper] getDeviceTypeForLog];
    NSDictionary* params = @{
                             @"id" : userId,
                             @"pw" : password,
                             @"uuid" : idFV,
                             @"device_type" : deviceType
                             };
    
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/auth/login" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) { // 성공
            [self loginComplete:[responseObject objectForKey:@"user_info"]];

            [Flurry logEvent:@"LOGIN SUCCESS"];
        } else { // 실패
            [Flurry logEvent:@"LOGIN FAIL"];

            //자동 로그인 아닐경우에만 alert
//            [[FootplrHelper sharedFootplrHelper] showFootplrAlertView:nil withMessage:errorMessage];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Footplr" message:NSLocalizedString(@"s38", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    
}

-(void) loginWithFb:(NSString *) accessToken
{
    // deviceInfo
    NSString *idFV = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *deviceType = [[FootplrHelper sharedFootplrHelper] getDeviceTypeForLog];
    
    NSDictionary *params = @{
                             @"access_token" : accessToken,
                             @"uuid" : idFV,
                             @"device_type" : deviceType
                             };
    [[ApiHelper sharedApiHelper]
        postRequestWithUrl:API_URL
        withPath:@"/api/auth/login_fb"
        withParameters:params
        withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
                    NSLog(@"%@", responseObject);
            if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) { // 성공
                [self loginComplete:[responseObject objectForKey:@"user_info"]];
                
                [Flurry logEvent:@"FB LOGIN SUCCESS"];
            } else if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == 400) { // NEED JOIN
                [delegate needJoin:accessToken];
            } else { // 실패
//                NSString* errorMessage = NSLocalizedString(@"s27", nil);
//                [[FootplrHelper sharedFootplrHelper] showFootplrAlertView:nil withMessage:errorMessage];
                [Flurry logEvent:@"FB LOGIN FAIL"];
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Footplr" message:NSLocalizedString(@"s38", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
                [alert show];
//                [[FootplrHelper sharedFootplrHelper] showError];
            }
    }];
}

-(void) joinWithId:(NSString *)userId withEmail:(NSString*)email withPassword:(NSString*)password withAccessToken:(NSString *) accessToken
{
    password = [self shaEncode:password];
    
    // deviceInfo
    NSString *idFV = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *deviceType = [[FootplrHelper sharedFootplrHelper] getDeviceTypeForLog];
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                            userId, @"id",
                            email, @"email",
                            password, @"pw",
                            idFV, @"uuid",
                            deviceType, @"device_type",
                            nil];
    if(accessToken) {
        [params setObject:accessToken forKey:@"access_token"];
    }
    
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/auth/join" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) { // 성공
            [self loginComplete:[responseObject objectForKey:@"user_info"]];
            [Flurry logEvent:@"USER JOINED"];
        } else { // 실패
            
            NSString* errorMessage;
            if([[responseObject objectForKey:@"message"] isEqualToString:@"EMAIL DUPLIACTED"]) {
                errorMessage = NSLocalizedString(@"s28", nil);
            } else if([[responseObject objectForKey:@"message"] isEqualToString:@"ID DUPLIACTED"]) {
                errorMessage = NSLocalizedString(@"s23", nil);
            } else {
                errorMessage = NSLocalizedString(@"s27", nil);
            }
            [[FootplrHelper sharedFootplrHelper] showFootplrAlertView:nil withMessage:errorMessage];
            
            
        }

    }];
}

-(void) checkIdDuplication:(NSString *) userId
{
    NSDictionary* params = @{
                             @"id" : userId
                            };
    
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/auth/dup_id_chk" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) { // 성공
            
        } else { // 실패
            
            
        }
        
    }];
}


-(void) sendPwResetEmail:(NSString *)email forUser:(NSString *)userId
{
    NSDictionary* params = @{
                             @"email" : email,
                             @"id" : userId
                             };
    
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/auth/send_passwd" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) { // 성공
            
        } else { // 실패
            
            
        }
        
    }];
}


-(void) loginComplete:(NSDictionary*) userInfo
{
    NSLog(@"lc : %@", userInfo);
    
    // SAVE USERINFO TO NSUSERDEFAULTS
    [userData setObject:userInfo forKey:USER_INFO];
    [userData synchronize];
    
    if([delegate respondsToSelector:@selector(loginComplete:)]) {
        [delegate loginComplete:userInfo];
    }
}

-(int) getCurrentTeamId
{
    NSDictionary* userInfo = [userData objectForKey:USER_INFO];
    if(userInfo) {
        if([userInfo objectForKey:@"teamId"]) {
            return [[userInfo objectForKey:@"teamId"] intValue];
        } else {
            return false;
        }
    }
    return false;
}

-(void) logout
{
    if ([FBSDKAccessToken currentAccessToken]) { // logout FB
        FBSDKLoginManager *fbSession = [[FBSDKLoginManager alloc] init];
        [fbSession logOut];
    }
    
    [[ApiHelper sharedApiHelper] getRequestWithUrl:API_URL withPath:@"/api/auth/logout" withParameters:nil withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
    }];
    [userData removeObjectForKey:USER_INFO];
    [userData synchronize];
    
    [delegate showLoginViewController];
}

-(void) changePassword:(NSString *)currentPw withNewPassword:(NSString *)newPw
{
    NSDictionary *params = @{
                             @"org_pw" : [self shaEncode:currentPw],
                             @"new_pw" : [self shaEncode:newPw]
                             };
    [[ApiHelper sharedApiHelper] postRequestWithUrl:API_URL withPath:@"/api/user/modify_pw" withParameters:params withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
            [delegate passwordChanged:API_SUCCESS];
        } else {
            int code = [[responseObject objectForKey:@"code"] intValue];
            
            // key 'code' not exist, then code = 0;
            if(code == 0) code = -1;
            [delegate passwordChanged:code];
        }
    }];
}

-(NSString*) shaEncode:(NSString*)input
{
    
    NSData *data = [input dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(data.bytes, data.length, digest);
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}


#pragma mark - Validation
-(BOOL) isValidId:(NSString *)userId
{
    NSString *idRegex = @"[a-zA-Z0-9-_]{4,12}";
    NSPredicate *idTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", idRegex];
    return [idTest evaluateWithObject:userId];
}

-(BOOL) isValidEmail:(NSString *)email
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(BOOL) isValidPassword:(NSString *)password
{
    NSString *passwordRegex = @"[a-zA-Z0-9!@#$%^&*-_]{4,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordTest evaluateWithObject:password];
}

@end
