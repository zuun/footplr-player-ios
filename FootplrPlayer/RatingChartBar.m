//
//  RatingChartBar.m
//  footplr
//
//  Created by EclipseKim on 2015. 6. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "RatingChartBar.h"

@implementation RatingChartBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _init];
    }
    return self;
}

-(void) _init
{
    rating = 0;
    count = 0;
    
    // ratingLabel
    ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 70, 30, 10)];
    ratingLabel.textAlignment = NSTextAlignmentCenter;
    ratingLabel.font = [UIFont systemFontOfSize:10.0f];
    ratingLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    [self addSubview:ratingLabel];
    
    countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 15)];
    countLabel.textAlignment = NSTextAlignmentCenter;
    countLabel.font = [UIFont systemFontOfSize:10.0f];
    countLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    [self addSubview:countLabel];
    
    barFillView = [[UIView alloc] initWithFrame:CGRectMake(8, 62, 14, 3)];
    [barFillView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WD]];
    [self addSubview:barFillView];
}



-(void) setRatingValue:(CGFloat) _rating
{
    rating = _rating;
    if((int)(_rating*2) % 2 == 0) { // natural
        ratingLabel.text = [NSString stringWithFormat:@"%d", (int)_rating];
    } else { // float
        ratingLabel.text = [NSString stringWithFormat:@"%.1f", _rating];
    }
}

-(void) setCount:(int)_count withMaximumCount:(int)_max
{
    count = _count;
    maxCount = _max;
    [self updateView];
}

-(void) updateView
{
    if(count == 0) {
        [barFillView setFrame:CGRectMake(8, 62, 14, 3)];
        [barFillView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WD]];
    } else {
        CGFloat barHeight = 50 * count / maxCount;
        [barFillView setFrame:CGRectMake(8, 65 - barHeight, 14, barHeight)];
        [barFillView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WA]];
        
        countLabel.text = [NSString stringWithFormat:@"%d", count];
        [countLabel setFrame:CGRectMake(0, CGRectGetMinY(barFillView.frame)-15, 30, 10)];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
