//
//  AttendStatsCell.h
//  footplr
//
//  Created by EclipseKim on 2015. 5. 31..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttendStatsCell : UITableViewCell

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *attendedValueLabel;
@property (strong, nonatomic) UILabel *notAttendedValueLabel;
@end
