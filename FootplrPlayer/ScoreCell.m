//
//  ScoreCell.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "ScoreCell.h"

@implementation ScoreCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        [self setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
        
        _homeScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 120, 40)];
        _homeScoreLabel.textAlignment = NSTextAlignmentCenter;
        _homeScoreLabel.numberOfLines = 2;
        [self.contentView addSubview:_homeScoreLabel];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(135, 2, 50, 40)];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.font = [UIFont systemFontOfSize:14.0f];
        _timeLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [self.contentView addSubview:_timeLabel];
        
        _awayScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(190, 2, 120, 40)];
        _awayScoreLabel.textAlignment = NSTextAlignmentCenter;
        _awayScoreLabel.font = [UIFont systemFontOfSize:14.0f];
        _awayScoreLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [self.contentView addSubview:_awayScoreLabel];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
