//
//  PlayerCell.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 13..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "PlayerCell.h"

@implementation PlayerCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        
        // backNumberLabel
        _backNumberLabel = [UILabel new];
        [_backNumberLabel setFrame:CGRectMake(10, 20, 34, 20)];
        _backNumberLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        _backNumberLabel.font = [UIFont systemFontOfSize:14.0f];
        _backNumberLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_backNumberLabel];
        
        // playerNameLabel
        _playerNameLabel = [UILabel new];
        [_playerNameLabel setFrame:CGRectMake(54, 14, 56, 15)];
        _playerNameLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_MEDIUM withSize:14.0f];
        _playerNameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        [self addSubview:_playerNameLabel];
        
        // positionLabel
        _positionLabel = [UILabel new];
        [_positionLabel setFrame:CGRectMake(54, 32, 256, 14)];
        _positionLabel.font = [UIFont systemFontOfSize:14.0f];
        _positionLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BC];
        [self addSubview:_positionLabel];
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
