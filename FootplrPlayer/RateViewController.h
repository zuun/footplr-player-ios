//
//  RateViewController.h
//  FootplrPlayer
//
//  Created by EclipseKim on 2015. 5. 5..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"
#import "PlayerRateCell.h"
#import "PositionHelper.h"

@protocol RateViewControllerDelegate <NSObject>

-(void) rated;

@end

@interface RateViewController : FootplrViewController <UITableViewDataSource, UITableViewDelegate, RatingViewDelegate>
{
    NSMutableArray *ratings;
    PositionHelper *positionHelper;
}

@property (nonatomic, assign) id<RateViewControllerDelegate> delegate;
@property Schedule *schedule;
@property NSMutableArray *players;
@property int teamId;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
