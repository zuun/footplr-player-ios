//
//  AttendedPlayerCell.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 11..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttendedPlayerCell : UITableViewCell

@property (strong, nonatomic) UILabel *backNumberLabel;
@property (strong, nonatomic) UILabel *playerNameLabel;
@property (strong, nonatomic) UILabel *ratingLabel;
@end
