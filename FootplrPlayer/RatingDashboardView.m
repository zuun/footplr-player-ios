//
//  RatingDashboardView.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 26..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "RatingDashboardView.h"

@implementation RatingDashboardView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _init];
    }
    return self;
}

-(void) _init
{
    rating = 0.0f;
    recentRatings = [NSArray new];

    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    NSDictionary *userInfo = [userData objectForKey:USER_INFO];
    NSString *userName = @"";
    if(userInfo) {
        userName = [userInfo objectForKey:@"member_nm"];
    }
    
    // nameLabel
    nameLabel = [UILabel new];
    [nameLabel setFrame:CGRectMake(10, 16, 300, 22)];
//    [nameLabel setFrame:CGRectMake(10, 0, 300, 44)];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_MEDIUM withSize:18.0f];
    nameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    nameLabel.text = userName;
    [self addSubview:nameLabel];
    
    // rating Label
    ratingLabel = [UILabel new];
    [ratingLabel setFrame:CGRectMake(10, 42, 300, 62)];
    ratingLabel.textAlignment = NSTextAlignmentCenter;
    ratingLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_LIGHT withSize:72.0f];
    ratingLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    ratingLabel.text = [NSString stringWithFormat:@"%.2f", rating];
    [self addSubview:ratingLabel];
    
    // ratingStarView
    UIView *ratingStarView = [UIView new];
    [ratingStarView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WA]];
    [ratingStarView setFrame:CGRectMake(115, 108, 90, 16)];
    
    ratingStarBgFill = [UIView new];
    [ratingStarBgFill setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:255 withGreen:198 withBlue:0]];
    [ratingStarView addSubview:ratingStarBgFill];
    
    UIImageView *starImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 90, 16)];
    [starImageView setImage:[UIImage imageNamed:@"schedule_star"]];
    [ratingStarView addSubview:starImageView];
    [self addSubview:ratingStarView];
    
    // rating chart
    ratingChart = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(0, 138, 320, 138)];
    ratingChart.dataSource = self;
    ratingChart.delegate = self;
    
    // spline
    ratingChart.enableBezierCurve = YES;
    
    //font
    ratingChart.labelFont = [UIFont systemFontOfSize:12.0f];
    
    ratingChart.colorTop = [UIColor clearColor];
    ratingChart.colorBottom = [UIColor clearColor];
    ratingChart.colorLine = [[UIManager sharedUIManager] colorWithRed:255 withGreen:198 withBlue:0];
    ratingChart.colorReferenceLines = [[UIManager sharedUIManager] colorWithRed:20 withGreen:21 withBlue:22];
    ratingChart.widthLine = 3.0f;
    
    ratingChart.animationGraphEntranceTime = .5f;
    ratingChart.animationGraphStyle = BEMLineAnimationFade;
    
    ratingChart.enablePopUpReport = YES;
    ratingChart.enableTouchReport = YES;
    
    ratingChart.autoScaleYAxis = YES;
    
    ratingChart.alwaysDisplayDots = NO;
    ratingChart.alwaysDisplayPopUpLabels = NO;
    
    ratingChart.formatStringForValues = @"%.2f";
    
    
//    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
//    size_t num_locations = 2;
//    CGFloat locations[2] = { 0.0, 1.0 };
//    CGFloat components[8] = {
//        1.0, 1.0, 1.0, 1.0,
//        1.0, 1.0, 1.0, 0.0
//    };
//    ratingChart.gradientBottom = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
    
    
    // average line
//    ratingChart.averageLine.enableAverageLine = YES;
//    ratingChart.averageLine.width = 0.2f;
//    ratingChart.averageLine.color = [[UIManager sharedUIManager] appColor:COLOR_WD];
//    ratingChart.averageLine.dashPattern = @[@(2),@(2)];
    
    ratingChart.colorReferenceLines = [[UIManager sharedUIManager] colorWithHexCode:0x5d616c];
//    ratingChart.colorReferenceLines = [[UIManager sharedUIManager] appColor:COLOR_RED_];
    
    
    // xAxis
    ratingChart.enableXAxisLabel = YES;
    ratingChart.enableReferenceXAxisLines = YES;
    
    ratingChart.colorXaxisLabel = [[UIManager sharedUIManager] appColor:COLOR_WA];
    ratingChart.lineDashPatternForReferenceXAxisLines = @[@(2),@(2)];
    
    // yAxis
//    ratingChart.enableYAxisLabel = YES;
    ratingChart.enableReferenceYAxisLines = YES;
    
    ratingChart.colorYaxisLabel = [[UIManager sharedUIManager] appColor:COLOR_WA];
    ratingChart.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];

    /*
     // Create a gradient to apply to the bottom portion of the graph
     CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
     size_t num_locations = 2;
     CGFloat locations[2] = { 0.0, 1.0 };
     CGFloat components[8] = {
     1.0, 1.0, 1.0, 1.0,
     1.0, 1.0, 1.0, 0.0
     };
     
     // Apply the gradient to the bottom portion of the graph
     self.myGraph.gradientBottom = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
     
     // Enable and disable various graph properties and axis displays
     self.myGraph.enableTouchReport = YES;
     self.myGraph.enablePopUpReport = YES;
     self.myGraph.enableYAxisLabel = YES;
     self.myGraph.autoScaleYAxis = YES;
     self.myGraph.alwaysDisplayDots = NO;
     self.myGraph.enableReferenceXAxisLines = YES;
     self.myGraph.enableReferenceYAxisLines = YES;
     self.myGraph.enableReferenceAxisFrame = YES;
     
     // Draw an average line
     self.myGraph.averageLine.enableAverageLine = YES;
     self.myGraph.averageLine.alpha = 0.6;
     self.myGraph.averageLine.color = [UIColor darkGrayColor];
     self.myGraph.averageLine.width = 2.5;
     self.myGraph.averageLine.dashPattern = @[@(2),@(2)];
     
     // Set the graph's animation style to draw, fade, or none
     self.myGraph.animationGraphStyle = BEMLineAnimationDraw;
     
     // Dash the y reference lines
     self.myGraph.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
     
     // Show the y axis values with this format string
     self.myGraph.formatStringForValues = @"%.1f";
     
     // Setup initial curve selection segment
     self.curveChoice.selectedSegmentIndex = self.myGraph.enableBezierCurve;
     
     // The labels to report the values of the graph when the user touches it
     self.labelValues.text = [NSString stringWithFormat:@"%i", [[self.myGraph calculatePointValueSum] intValue]];
     self.labelDates.text = @"between now and later";
     */
    noDataLabelText = NSLocalizedString(@"s63", nil);
    
    [self addSubview:ratingChart];
    
    [self updateView];
}

-(void) setRating:(CGFloat) _rating withRecentRatings:(NSArray *) ratings;
{
    rating = _rating;
    recentRatings = ratings;
    if([recentRatings count] == 1) {
        recentRatings = [NSArray new];
        noDataLabelText = NSLocalizedString(@"s63_1", nil);
    }
    [self updateView];
}

-(void) updateView
{
    ratingLabel.text = [NSString stringWithFormat:@"%.2f", rating];
    [ratingStarBgFill setFrame:CGRectMake(0, 0, 90 * rating / 10, 16)];
    [ratingChart reloadGraph];
}

#pragma mark - BEMSimpleLineGraphDataSource
- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph
{
//    NSLog(@"numberOfPoints %lu", [recentRatings count]);
    return [recentRatings count];
}


/** The vertical position for a point at the given index. It corresponds to the Y-axis value of the Graph.
 @param graph The graph object requesting the point value.
 @param index The index from left to right of a given point (X-axis). The first value for the index is 0.
 @return The Y-axis value at a given index. */
- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index
{
    NSDictionary *ratingInfo = [recentRatings objectAtIndex:index];
    return [[ratingInfo objectForKey:@"rate"] floatValue];
}
#pragma mark - SimpleLineGraph Delegate
- (NSString *)noDataLabelTextForLineGraph:(BEMSimpleLineGraphView *)graph
{
    return noDataLabelText;
}

- (UIView *)popUpViewForLineGraph:(BEMSimpleLineGraphView *)graph
{
//    graph.
    UILabel *popupLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 90, 40)];
    [popupLabel setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WA]];
    popupLabel.font = [ UIFont systemFontOfSize:12.0f];
    popupLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BG];
    popupLabel.numberOfLines = 0;
    popupLabel.textAlignment = NSTextAlignmentCenter;
    
    popupLabel.layer.cornerRadius = 5;
    popupLabel.layer.masksToBounds = YES;
    return popupLabel;
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph modifyPopupView:(UIView *)popupView forIndex:(NSUInteger)index
{
    UILabel *label = (UILabel *) popupView;
    
    NSDictionary *ratingInfo = [recentRatings objectAtIndex:index];
    NSString *title = [ratingInfo objectForKey:@"title"];
    CGFloat rating = [[ratingInfo objectForKey:@"rate"] floatValue];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *matchTime = [df dateFromString:[ratingInfo objectForKey:@"schedule"]];
    NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
    [df2 setDateFormat:@"yyyy-MM-dd"];
    NSString *matchTimeString = [df2 stringFromDate:matchTime];
    
    NSString *ratingString = [NSString stringWithFormat:@"%.2f", rating];
    
    NSString *text = [NSString stringWithFormat:@"%@\n%@\n%@", matchTimeString, title, ratingString];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:text];
    [attrText addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:11.0f] range:NSMakeRange(0, title.length + matchTimeString.length + 2)];
    [attrText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0f] range:NSMakeRange(title.length + matchTimeString.length + 2, ratingString.length)];
    label.attributedText = attrText;
    [label sizeToFit];
    
    CGRect frame = label.frame;
    frame.size.width += 20;
    frame.size.height += 10;
    label.frame = frame;
    /*
    location = "\Ubcf4\Ub77c\Ub9e4\Uacf5\Uc6d0 \Uc778\Uc870\Uc794\Ub514 \Ucd95\Uad6c\Uc7a5 ";
    rate = "3.12500";
    schedule = "2015-03-28 14:00:00";
    title = "FC \Ud55c\Ubc31";
    */
}

//- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph {
//    return 0;
//}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
