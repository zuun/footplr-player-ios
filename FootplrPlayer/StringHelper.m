//
//  StringHelper.m
//  Footplr
//
//  Created by EclipseKim on 2015. 1. 5..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "StringHelper.h"

@implementation StringHelper

+(CGFloat) heightForLabelWithFont:(UIFont *)font withLabelWidth:(CGFloat)labelWidth withContent:(NSString*) content
{
    CGSize constraint = CGSizeMake(labelWidth, MAXFLOAT);
    
    CGRect rect = [content boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    CGSize size = rect.size;
    
    CGFloat height = MAX(ceilf(size.height), 21.0f);
    return height;
}

+(BOOL) isEmptyString:(NSString *) s
{
    return (!s || [[s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]);
}

@end
