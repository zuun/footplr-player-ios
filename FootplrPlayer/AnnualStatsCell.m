//
//  AnnualStatsCell.m
//  footplr
//
//  Created by EclipseKim on 2015. 5. 28..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "AnnualStatsCell.h"

@implementation AnnualStatsCell
@synthesize yearLabel, attendedLabel, goalLabel, assistLabel, momLabel, ratingsLabel;

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        [self setBackgroundColor:[UIColor clearColor]];
        [self.contentView setBackgroundColor:[UIColor clearColor]];

        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        CGFloat cellWidth = window.frame.size.width;

        // yearLabel
        yearLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 40, 20)];
        yearLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        yearLabel.font = [UIFont systemFontOfSize:12.0f];
        [self addSubview:yearLabel];
        
        // attendedLabel
        attendedLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 5, 40, 20)];
        attendedLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        attendedLabel.font = [UIFont systemFontOfSize:12.0f];
        attendedLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:attendedLabel];
        
        // goalLabel
        goalLabel = [[UILabel alloc] initWithFrame:CGRectMake(135, 5, 40, 20)];
        goalLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        goalLabel.font = [UIFont systemFontOfSize:12.0f];
        goalLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:goalLabel];
        
        // assistLabel
        assistLabel = [[UILabel alloc] initWithFrame:CGRectMake(180, 5, 40, 20)];
        assistLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        assistLabel.font = [UIFont systemFontOfSize:12.0f];
        assistLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:assistLabel];
        
        // momLabel
        momLabel = [[UILabel alloc] initWithFrame:CGRectMake(225, 5, 40, 20)];
        momLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        momLabel.font = [UIFont systemFontOfSize:12.0f];
        momLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:momLabel];
        
        // ratingsLabel
        ratingsLabel = [[UILabel alloc] initWithFrame:CGRectMake(270, 5, 40, 20)];
        ratingsLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        ratingsLabel.font = [UIFont systemFontOfSize:12.0f];
        ratingsLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:ratingsLabel];
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
