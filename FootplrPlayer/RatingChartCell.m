//
//  RatingChartCell.m
//  footplr
//
//  Created by EclipseKim on 2015. 6. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "RatingChartCell.h"

@implementation RatingChartCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        [self setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:163 withGreen:190 withBlue:199]];
        
        _ratingBars = [NSMutableArray new];
        for(int i = 0 ; i < 10 ; i++) {
            RatingChartBar *bar = [[RatingChartBar alloc] initWithFrame:CGRectMake(30*i+10, 11, 30, 80)];
            [bar setRatingValue:(i+1)];
            [self.contentView addSubview:bar];
            [_ratingBars addObject:bar];
        }
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
