//
//  RatingResultView.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 22..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingResultView : UIView
{
    UILabel *ratingLabel;
    UIView *starFillView;
    UIImageView *ratingImageView;
    CGFloat rating;
}
-(void) setRating:(CGFloat) rating;
@end
