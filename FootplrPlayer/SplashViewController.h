//
//  SplashViewController.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 20..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController <SessionDelegate>
{
    UIWindow *window;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end
