//
//  MomCell.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 11..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MomCell : UITableViewCell

@property (nonatomic, strong) UILabel *momLabel;
@end
