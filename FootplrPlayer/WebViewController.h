//
//  WebViewController.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 16..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"

@interface WebViewController : FootplrViewController
{
    NSURL *url;
}

@property (strong, nonatomic) NSString *urlString;

@end
