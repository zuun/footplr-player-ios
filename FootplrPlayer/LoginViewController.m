//
//  LoginViewController.m
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 5. 4..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "LoginViewController.h"
#import "TeamVerificationViewController.h"
#import "UserSelectionViewController.h"
#import "MainViewController.h"
#import "ForgotPasswordViewController.h"
#import "JoinViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) _init
{
    [self setNavigationTitle:NSLocalizedString(@"s29", nil)];
    [self setNavigationBackButton];
    
    UIBarButtonItem* loginBtn = [UIBarButtonItem barItemWithImageName:@"nav_confirm_n" target:self action:@selector(login)];
    [self setRightBarButtons:@[loginBtn]];
    
    
    // idTextField
    _idTextField.placeholder = NSLocalizedString(@"s30", nil);
    _idTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _idTextField.font = [UIFont systemFontOfSize:14.0f];
    [_idTextField setBackgroundColor:[UIColor whiteColor]];
    
    [[UIManager sharedUIManager] setLeftViewToTextField:_idTextField withImageName:@"textfield_ic_id"];
    [[UIManager sharedUIManager] addBottomBorderToView:_idTextField withBorder:1 withColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_idTextField];
    
    // passwordTextField
    _passwordTextField.secureTextEntry = YES;
    _passwordTextField.placeholder = NSLocalizedString(@"s31", nil);
    _passwordTextField.font = [UIFont systemFontOfSize:14.0f];
    [_passwordTextField setBackgroundColor:[UIColor whiteColor]];
    [[UIManager sharedUIManager] setLeftViewToTextField:_passwordTextField withImageName:@"textfield_ic_password"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_passwordTextField];

    
    // forgotPasswordBtn
    [_forgotPasswordBtn setTitle:NSLocalizedString(@"s32", nil) forState:UIControlStateNormal];
    [_forgotPasswordBtn addTarget:self action:@selector(forgotPassword) forControlEvents:UIControlEventTouchUpInside];
    [[_forgotPasswordBtn titleLabel] setFont:[UIFont systemFontOfSize:14.0f]];
    [_forgotPasswordBtn setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_WC] forState:UIControlStateNormal];
    
    // fbLoginBtn
    [_fbLoginBtn setBackgroundImage:[[UIImage imageNamed:@"btn_fb_n"] stretchableImageWithLeftCapWidth:4 topCapHeight:4] forState:UIControlStateNormal];
    [_fbLoginBtn setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_WA] forState:UIControlStateNormal];
    [[_fbLoginBtn titleLabel] setFont:[UIFont systemFontOfSize:14.0f]];
    [_fbLoginBtn setTitle:NSLocalizedString(@"s33", nil) forState:UIControlStateNormal];
    [_fbLoginBtn addTarget:self action:@selector(loginWithFb) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Actions

-(void) login
{
    @try {
        [self checkInputs];
        
        NSString* userId = _idTextField.text;
        NSString* password = _passwordTextField.text;
        
        [[SessionManager sharedSessionManager] loginWithId:userId withPassword:password];
        [[SessionManager sharedSessionManager] setDelegate:self];
        
    } @catch (NSException* e){
        [[FootplrHelper sharedFootplrHelper] showFootplrAlertView:nil withMessage:[e reason]];
    } @finally {
        NSLog(@"finally");
    }
}


-(void) loginWithFb
{
    if ([FBSDKAccessToken currentAccessToken]) {
        NSString *accessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
        NSLog(@"token from session : %@", accessToken);
        [[SessionManager sharedSessionManager] loginWithFb:accessToken];
        [[SessionManager sharedSessionManager] setDelegate:self];
    } else {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                // Process error
            } else if (result.isCancelled) {
                // Handle cancellations
            } else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if ([result.grantedPermissions containsObject:@"email"]) {
                    // Do work
                    NSString *accessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
                    [[SessionManager sharedSessionManager] loginWithFb:accessToken];
                    [[SessionManager sharedSessionManager] setDelegate:self];
                }
            }
        }];
    }
}

-(void) forgotPassword
{
    ForgotPasswordViewController *fpvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordStoryboard"];
    [self.navigationController pushViewController:fpvc animated:YES];
}


-(void) checkInputs
{
    NSString* userId = _idTextField.text;
    NSString* password = _passwordTextField.text;
    
    
    NSString* exceptionReason;
    if([StringHelper isEmptyString:userId]) exceptionReason = NSLocalizedString(@"s34", nil);
    else if([StringHelper isEmptyString:password]) exceptionReason = NSLocalizedString(@"s35", nil);
    
    if(exceptionReason) {
        NSException* e = [NSException
                          exceptionWithName:@"EmptyInputException"
                          reason:exceptionReason
                          userInfo:nil];
        @throw e;
    }
}

#pragma mark -SessionDelegate

-(void)loginComplete:(NSDictionary *)userInfo
{
    NSString *teamId = [userInfo objectForKey:@"team_id"];
    NSString *userId = [userInfo objectForKey:@"user_id"];
    UIViewController *rootVC;
    if(teamId && [teamId isEqualToString:@""]) { // team not verified
        TeamVerificationViewController *tvvc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamVerificationStoryboard"];
        rootVC = tvvc;
    } else if(userId && [userId isEqualToString:@""]) { // member not selected
        UserSelectionViewController *usvc =  [self.storyboard instantiateViewControllerWithIdentifier:@"UserSelectionStoryboard"];
        usvc.teamId = [teamId intValue];
        rootVC = usvc;
    } else {
        MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainStoryboard"];
        mainVC.teamId = [teamId intValue];
        rootVC = mainVC;
    }
    
    [self.navigationController pushViewController:rootVC animated:YES];
}

-(void) needJoin:(NSString *)accessToken
{
    JoinViewController *jvc = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinStoryboard"];
    jvc.accessToken = accessToken;
    [self.navigationController pushViewController:jvc animated:YES];
}
@end
