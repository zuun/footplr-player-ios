//
//  Schedule.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 21..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "Schedule.h"


@implementation Schedule
@synthesize title, type, teamId, stadiumId, matchTime, matchTimeString, matchId, location, ga, gf, coverImageUrl, isMatchDone, rated, attended, rating, memo, attendedPlayers, scores, monthKey, ratingExpired;
-(id) initWithScheduleInfo:(NSDictionary *) _schedule
{
    if(self = [super init]) {
        schedule = _schedule;
        
        title = [schedule objectForKey:@"title"];
        type = [[schedule objectForKey:@"type"] isEqualToString:@"match"] ? MATCH : TEAM_SCHEDULE;
        teamId = [[schedule objectForKey:@"team_id"] intValue];
        stadiumId = [[schedule objectForKey:@"stadium_id"] intValue];

        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        matchTime = [df dateFromString:[schedule objectForKey:@"schedule"]];
        matchTimeString = [[DateHelper sharedDateHelper] getHumanizedDateStringForDate:matchTime withDay:YES];
        
        ratingExpired = [DateHelper daysBetweenDate:matchTime andDate:[NSDate new]] > 7;
        
        NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
        [df2 setDateFormat:@"yyyy-MM"];
        monthKey = [df2 stringFromDate:matchTime];
        
        
        
        matchId = [[schedule objectForKey:@"match_id"] intValue];
        location = [schedule objectForKey:@"location"];
        ga = [[schedule objectForKey:@"ga"] intValue];
        gf = [[schedule objectForKey:@"gf"] intValue];
        coverImageUrl = [schedule objectForKey:@"cover_pic"];
      
        isMatchDone = !([[schedule objectForKey:@"ga"] isEqualToString:@""] || [[schedule objectForKey:@"gf"] isEqualToString:@""]);
        rated = [[schedule objectForKey:@"is_rate"] intValue] == 1;
        attended = [[schedule objectForKey:@"is_attend"] intValue] == 1;
        
        // rating
        rateCount = [[schedule objectForKey:@"rate_cnt"] intValue];
        ratingSum = [[schedule objectForKey:@"rate_sum"] floatValue];
        rating = (rateCount == 0) ? 0.0f : ratingSum / rateCount;
        
        scores = [NSMutableArray new];
    }
    return self;
}

-(void) setAttendedPlayers:(NSMutableArray *)_attendedPlayers
{
    attendedPlayers = [NSMutableArray new];
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    int userId = [[[userData objectForKey:USER_INFO] objectForKey:@"user_id"] intValue];
    for(NSDictionary *playerInfo in _attendedPlayers) {
        Player *p = [[Player alloc] initWithPlayerInfo:playerInfo];
        if(p.userId == userId) {
            [attendedPlayers insertObject:p atIndex:0];
        } else {
            [attendedPlayers addObject:p];
        }
    }
}

-(void) addRating:(CGFloat)__rating
{
    NSLog(@"addRating : %d, %f %f", rateCount, __rating, rating);
    rateCount++;
    ratingSum += __rating;
        NSLog(@"addRating : %d, %f %f", rateCount, __rating, rating);
    rating = ratingSum / rateCount;
}
@end
