//
//  UIBarButtonItem+IconImage.m
//  Footplr
//
//  Created by JunhyuckKim on 14. 8. 6..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "UIBarButtonItem+IconImage.h"

@implementation UIBarButtonItem (IconImage)

+ (UIBarButtonItem*)barItemWithImageName:(NSString*)imageName target:(id)target action:(SEL)action {
    UIImage *iconImage;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) { // 7.0 이상
        iconImage = [[UIImage imageNamed:imageName]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    } else {
        iconImage = [UIImage imageNamed:imageName];
    }
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:iconImage style:UIBarButtonItemStylePlain target:target action:action];
    barButton.tag = BAR_BUTTON_ICON_IMAGE_TAG;
    return barButton;
}

+ (UIBarButtonItem *) barItemWithImageName:(NSString *)imageName withFrame:(CGRect)frame target:(id)target action:(SEL)action
{
    UIImage *iconImage;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) { // 7.0 이상
        iconImage = [[UIImage imageNamed:imageName]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    } else {
        iconImage = [UIImage imageNamed:imageName];
    }
    UIButton* customBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBarButton setImage:iconImage forState:UIControlStateNormal];
    [customBarButton setFrame:frame];
    [customBarButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem* barButton = [[UIBarButtonItem alloc] initWithCustomView:customBarButton];
    barButton.tag = BAR_BUTTON_ICON_IMAGE_TAG;
    return barButton;
}
@end
