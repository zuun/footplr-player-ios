//
//  Player.h
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 4. 21..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject
{
    int ratingCounts;
    CGFloat ratingSum;
}
@property int backNumber;
@property int memberId;
@property int userId;
@property NSString *positionCode;
@property NSString *position;
@property NSString *name;
@property NSURL *avatarUrl;

@property CGFloat ratings;

-(id) initWithPlayerInfo:(NSDictionary *) _playerInfo;
@end
