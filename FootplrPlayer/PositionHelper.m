//
//  PositionHelper.m
//  Footplr
//
//  Created by JunhyuckKim on 14. 8. 18..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "PositionHelper.h"

@implementation PositionHelper
static PositionHelper *sharedPositionHelper;
+ (PositionHelper*)sharedPositionHelper {
    if (sharedPositionHelper == nil) {
        sharedPositionHelper = [[PositionHelper alloc] init];
    }
    [self loadPositions];
    return sharedPositionHelper;
}

+(id)alloc
{
    @synchronized([PositionHelper class])
    {
        NSAssert(sharedPositionHelper == nil, @"Attempted to allocate a second instance of a singleton.");
        sharedPositionHelper = [super alloc];
        
        return sharedPositionHelper;
    }
    
    return nil;
}

-(id)init
{
    self = [super init];
    return self;
}

+(void) loadPositions
{
    if(positionCodes) return ;
    NSString *localeCode = [[FootplrHelper sharedFootplrHelper] getDeviceLocale];
    NSString *apiPath = [NSString stringWithFormat:@"/api/common/position_code/%@",localeCode];
    [[ApiHelper sharedApiHelper] getRequestWithUrl:MANAGER_API_URL withPath:apiPath withParameters:nil withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"pcodes : %@", responseObject);
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == 0) {
            positionCodes = [responseObject objectForKey:@"result"];
        }
    }];
}

-(NSArray*) getPositionCodes
{
    return positionCodes;
}


-(UIColor*) getPositionColorByPositionType:(NSString*) positionType
{
    NSDictionary* positionColor = @{
                                    POSITION_GK :     [[UIManager sharedUIManager] appColor:COLOR_YELLOW_],
                                    POSITION_DF :     [[UIManager sharedUIManager] appColor:COLOR_BLUE_],
                                    POSITION_MF :     [[UIManager sharedUIManager] appColor:COLOR_GREEN_],
                                    POSITION_FW :     [[UIManager sharedUIManager] appColor:COLOR_RED_]
                                    };
    return [positionColor objectForKey:positionType];

}
-(NSString*) getPositionTypeByCode:(NSString*) positionCode
{
    NSDictionary* positionTypeMap = @{
                                      @"GK": POSITION_GK,
                                      @"SW": POSITION_DF,
                                      @"LWB": POSITION_DF,
                                      @"RWB": POSITION_DF,
                                      @"LSB": POSITION_DF,
                                      @"RSB": POSITION_DF,
                                      @"CB": POSITION_DF,
                                      @"DMF": POSITION_MF,
                                      @"LMF": POSITION_MF,
                                      @"RMF": POSITION_MF,
                                      @"CMF": POSITION_MF,
                                      @"AMF": POSITION_MF,
                                      @"LWF": POSITION_FW,
                                      @"RWF": POSITION_FW,
                                      @"SS": POSITION_FW,
                                      @"ST": POSITION_FW
                                      };
    return [positionTypeMap objectForKey:positionCode];
}

-(NSString *)getPositionNameByCode:(NSString *)positionCode
{
    for(NSDictionary *pinfo in positionCodes) {
        if([pinfo[@"position_cd"] isEqualToString:positionCode]) {
            return pinfo[@"position_nm"];
        }
    }
    return nil;
}
@end
