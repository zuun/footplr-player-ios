//
//  UIBarButtonItem+IconImage.h
//  Footplr
//
//  Created by JunhyuckKim on 14. 8. 6..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
const static int BAR_BUTTON_ICON_IMAGE_TAG = 11;
@interface UIBarButtonItem (IconImage)
+ (UIBarButtonItem*)barItemWithImageName:(NSString*)imageName target:(id)target action:(SEL)action;
+ (UIBarButtonItem *) barItemWithImageName:(NSString *)imageName withFrame:(CGRect)frame target:(id)target action:(SEL)action;
@end
