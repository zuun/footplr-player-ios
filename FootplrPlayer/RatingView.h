//
//  RatingView.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 19..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RatingViewDelegate <NSObject>
-(void) ratingChanged:(CGFloat)rating forTag:(NSInteger) tag;


@end

const static int INITIAL_RATING = 5;

@interface RatingView : UIView
{
    NSMutableArray *starViews;
    
    UIImage *highlightedDot;
    UIImage *unhighlightedDot;
    UIImage *currentRatingDot;
    UILabel *currentRatingLabel;
//    UIImage *smallEmptyStarImage;
//    UIImage *bigEmptyStarImage;
//    UIImage *smallHalfStarImage;
//    UIImage *bigHalfStarImage;
//    UIImage *smallFullStarImage;
//    UIImage *bigFullStarImage;
    BOOL isTouching;
}

@property (nonatomic, assign) id<RatingViewDelegate> delegate;
@property (nonatomic) int maxRating;
@property (nonatomic) int rating;
- (void)setRating:(int)rating ;
@end
