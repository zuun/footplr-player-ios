//
//  StartViewController.h
//  footplr
//
//  Created by EclipseKim on 2015. 5. 7..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface StartViewController : FootplrViewController <SessionDelegate>

@property (strong, nonatomic) IBOutlet UIButton *fbJoinBtn;
@property (strong, nonatomic) IBOutlet UIButton *emailJoinBtn;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@end
