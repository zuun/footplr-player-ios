//
//  ChangePasswordViewController.m
//  footplr
//
//  Created by EclipseKim on 2015. 6. 14..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) _init
{
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"s_20", nil)];
    
    UIBarButtonItem* doneBtn = [UIBarButtonItem barItemWithImageName:@"nav_confirm_n" target:self action:@selector(changePassword)];
    [self setRightBarButtons:@[doneBtn]];
    
    // curPasswordTextField
    _curPasswordTextField.placeholder = NSLocalizedString(@"s_21", nil);
    _curPasswordTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _curPasswordTextField.font = [UIFont systemFontOfSize:14.0f];
    [_curPasswordTextField setBackgroundColor:[UIColor whiteColor]];
    _curPasswordTextField.secureTextEntry = YES;
    
    [[UIManager sharedUIManager] setLeftViewToTextField:_curPasswordTextField withImageName:@"textfield_ic_password"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_curPasswordTextField];
    [[UIManager sharedUIManager] addBottomBorderToView:_curPasswordTextField withBorder:0.8f withColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
    
    // pwTextField
    _pwTextField.placeholder = NSLocalizedString(@"s_23", nil);
    _pwTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _pwTextField.font = [UIFont systemFontOfSize:14.0f];
    [_pwTextField setBackgroundColor:[UIColor whiteColor]];
    _pwTextField.secureTextEntry = YES;
    
    
    [[UIManager sharedUIManager] addBottomBorderToView:_pwTextField withBorder:0.4f withColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
    _pwTextField.secureTextEntry = YES;
    [[UIManager sharedUIManager] setLeftViewToTextField:_pwTextField withImageName:@"textfield_ic_password"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_pwTextField];
    
    // pwTextField2
    _pwTextField2.placeholder = NSLocalizedString(@"s_22", nil);
    _pwTextField2.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    _pwTextField2.font = [UIFont systemFontOfSize:14.0f];
    [_pwTextField2 setBackgroundColor:[UIColor whiteColor]];
    _pwTextField2.secureTextEntry = YES;
    
    [[UIManager sharedUIManager] setLeftViewToTextField:_pwTextField2 withImageName:@"textfield_ic_password"];
    [[UIManager sharedUIManager] setDeleteButtonRightViewToTextField:_pwTextField2];
    [[UIManager sharedUIManager] addBottomBorderToView:_pwTextField2 withBorder:0.4f withColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
    
}

-(void) changePassword
{
    @try {
        [self checkInputs];
        
        NSString *curPW = _curPasswordTextField.text;
        NSString *pw = _pwTextField.text;
        
        [[SessionManager sharedSessionManager] changePassword:curPW withNewPassword:pw];

        
    } @catch (NSException* e){
        [[FootplrHelper sharedFootplrHelper] showFootplrAlertView:nil withMessage:[e reason]];
    } @finally {
        NSLog(@"finally");
    }
}

-(void) checkInputs
{
    NSString *curPW = _curPasswordTextField.text;
    NSString *password = _pwTextField.text;
    NSString *password2 = _pwTextField2.text;
    
    NSString* exceptionReason;
    if([StringHelper isEmptyString:curPW] || [StringHelper isEmptyString:password] || [StringHelper isEmptyString:password2]) exceptionReason = NSLocalizedString(@"s35", nil);
    
    if(exceptionReason) {
        NSException* e = [NSException
                          exceptionWithName:@"EmptyInputException"
                          reason:exceptionReason
                          userInfo:nil];
        @throw e;
    }
    
    //validation for password
    if(![[SessionManager sharedSessionManager] isValidPassword:password]) {
        NSException* e = [NSException
                          exceptionWithName:@"EmptyInputException"
                          reason:NSLocalizedString(@"s25", nil)
                          userInfo:nil];
        @throw e;
    }
    
    // password / confirm match
    if(![password isEqualToString:password2]) {
        NSException* e = [NSException
                          exceptionWithName:@"EmptyInputException"
                          reason:NSLocalizedString(@"s_25", nil)
                          userInfo:nil];
        @throw e;
    }
}

#pragma mark - Session Delegate
-(void) passwordChanged:(int)status
{
    if(status == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"s_26", nil) message:@"" delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
        [alert show];
        [self.navigationController popViewControllerAnimated:YES];
    } else if(status == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"s_24", nil) message:@"" delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
        [alert show];
    } else {
        [[FootplrHelper sharedFootplrHelper] showError];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
