//
//  UIBarButtonItem+PlainText.h
//  Footplr
//
//  Created by EclipseKim on 2014. 7. 13..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
const static int BAR_BUTTON_PLAIN_TEXT_TAG = 10;

@interface UIBarButtonItem (PlainText)
+ (UIBarButtonItem*)barItemWithFrame:(CGRect)frame withTitle:(NSString*)title target:(id)target action:(SEL)action;
+ (UIBarButtonItem*)barItemWithTitle:(NSString*)title target:(id)target action:(SEL)action;
@end
