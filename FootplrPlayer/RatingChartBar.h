//
//  RatingChartBar.h
//  footplr
//
//  Created by EclipseKim on 2015. 6. 12..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MAX_BAR_HEIGHT  50

@interface RatingChartBar : UIView
{
    UIView *barFillView;
    UILabel *ratingLabel;
    UILabel *countLabel;
    
    CGFloat rating;
    int count;
    int maxCount;
}

-(void) setRatingValue:(CGFloat) rating;
-(void) setCount:(int)_count withMaximumCount:(int)_max;
@end
