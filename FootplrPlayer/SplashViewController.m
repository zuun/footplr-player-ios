//
//  SplashViewController.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 20..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "SplashViewController.h"
#import "TeamVerificationViewController.h"
#import "UserSelectionViewController.h"
#import "MainViewController.h"
#import "RateViewController.h"
#import "EntryViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash"]];
    imageView.tag = 4141;
    
    window = [[[UIApplication sharedApplication] delegate] window];
    [imageView setFrame:window.frame];
    [window addSubview:imageView];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [[window viewWithTag:4141] removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SessionDelegate

-(void) loginComplete:(NSDictionary *)userInfo
{
    UIViewController *rootVC;
    if(!userInfo) {
        EntryViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"EntryStoryboard"];
        rootVC = evc;
    } else {
        NSString *teamId = [userInfo objectForKey:@"team_id"];
        NSString *userId = [userInfo objectForKey:@"user_id"];
    
        if(teamId && [teamId isEqualToString:@""]) { // team not verified
            TeamVerificationViewController *tvvc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamVerificationStoryboard"];
            rootVC = tvvc;
        } else if(userId && [userId isEqualToString:@""]) { // member not selected
            UserSelectionViewController *usvc =  [self.storyboard instantiateViewControllerWithIdentifier:@"UserSelectionStoryboard"];
            usvc.teamId = [teamId intValue];
            rootVC = usvc;
        } else {
            MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainStoryboard"];
            mainVC.teamId = [teamId intValue];
            rootVC = mainVC;
        }
    }    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rootVC];
    window.rootViewController = navigationController;
}

-(void) showLoginViewController
{
    EntryViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"EntryStoryboard"];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:evc];
    window.rootViewController = navigationController;
}
@end
