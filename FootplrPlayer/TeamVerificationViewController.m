//
//  TeamVerificationViewController.m
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 5. 4..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "TeamVerificationViewController.h"
#import "UserSelectionViewController.h"
#import "SettingsViewController.h"
#import "WebViewController.h"

@interface TeamVerificationViewController ()

@end

@implementation TeamVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.navigationItem setHidesBackButton:YES];
    [self _init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void) _init
{
    [self setNavigationTitle:NSLocalizedString(@"s44", nil)];
    
    UIBarButtonItem* settingsBtn = [UIBarButtonItem barItemWithImageName:@"nav_setting_n" target:self action:@selector(settings)];
    [self setRightBarButtons:@[settingsBtn]];
    
    // titleLabel
    _titleLabel.text = NSLocalizedString(@"s45", nil);
    _titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    _titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.numberOfLines = 0;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    // subTitleLabel
    _subTitleLabel.text = NSLocalizedString(@"s46", nil);
    _subTitleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WC];
    _subTitleLabel.font = [UIFont systemFontOfSize:14.0f];
    _subTitleLabel.textAlignment = NSTextAlignmentCenter;
    _subTitleLabel.numberOfLines = 0;
    
    // teamCodeTextfield
    _teamCodeTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    _teamCodeTextField.font = [UIFont systemFontOfSize:24.0f];
    _teamCodeTextField.textAlignment = NSTextAlignmentCenter;
    _teamCodeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"s50", nil) attributes:@{NSForegroundColorAttributeName: [[UIManager sharedUIManager] appColor:COLOR_BC]}];
//    [[UIManager sharedUIManager] addBottomBorderToView:_teamCodeTextField withBorder:1 withColor:[[UIManager sharedUIManager] appColor:COLOR_RED_]];
    _teamCodeTextField.layer.cornerRadius = 2;
    _teamCodeTextField.layer.masksToBounds = YES;
    _teamCodeTextField.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    [_teamCodeTextField setBackgroundColor:[UIColor whiteColor]];
    
    // moreLabel
    _moreLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTeamCodeGuide)];
    [_moreLabel addGestureRecognizer:tap];
    
    _moreLabel.text = NSLocalizedString(@"s48", nil);
    _moreLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    _moreLabel.font = [UIFont systemFontOfSize:14.0f];
    _moreLabel.textAlignment = NSTextAlignmentCenter;
    
    // done button
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setFrame:CGRectMake(102.5, CGRectGetMaxY(_teamCodeTextField.frame) + 30, 115, 44)];
    [doneButton setBackgroundImage:[UIImage imageNamed:@"btn_codedone_n"] forState:UIControlStateNormal];
    [doneButton setTitle:NSLocalizedString(@"s56", nil) forState:UIControlStateNormal];
    [doneButton setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_WA] forState:UIControlStateNormal];
    doneButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [doneButton addTarget:self action:@selector(verifyTeam) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:doneButton];
}

#pragma mark - Actions

-(void)verifyTeam
{
    NSString *teamCode = _teamCodeTextField.text ;
    if([StringHelper isEmptyString:teamCode]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Footplr" message:NSLocalizedString(@"s45", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
        [alert show];
        return ;
    }
    
    NSDictionary *params = @{
                             @"code" : teamCode
                             };
    [[ApiHelper sharedApiHelper]
        getRequestWithUrl:API_URL
        withPath:[NSString stringWithFormat:@"/api/team/get_team_by_code/%@", teamCode]
        withParameters:params
        withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
            if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
                NSDictionary *teamInfo = [responseObject objectForKey:@"result"];
                NSString *teamId = [teamInfo objectForKey:@"team_id"];
//                NSString *teamName = [result objectForKey:@"team_nm"];
                
                NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
                
                // update team id in userinfo
                NSMutableDictionary *userInfo = [[userData objectForKey:USER_INFO] mutableCopy];
                [userInfo setObject:teamId forKey:@"team_id"];
                [userData setObject:userInfo forKey:USER_INFO];
                
                // set team info
                [userData setObject:teamInfo forKey:TEAM_INFO];
                [userData synchronize];
                
                UserSelectionViewController *usvc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserSelectionStoryboard"];
                usvc.teamId = [teamId intValue];
                [self.navigationController pushViewController:usvc animated:YES];
                
            } else if([[responseObject objectForKey:@"code"] intValue] == 400) { // INVALID CODE
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Footplr" message:NSLocalizedString(@"s49", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"s56", nil) otherButtonTitles:nil, nil];
                [alert show];
            } else { // unknown response
                [[FootplrHelper sharedFootplrHelper] showError];
            }
    }];
}

-(void) showTeamCodeGuide
{
    WebViewController *wvc = [[WebViewController alloc] init];
    wvc.urlString = @"http://player.footplr.com/code";
    [self.navigationController pushViewController:wvc animated:YES];
}

-(void) settings
{
    SettingsViewController *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsStoryboard"];
    svc.hideUserInfo = YES;
    [self.navigationController pushViewController:svc animated:YES];
}
@end
