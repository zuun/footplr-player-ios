//
//  ApiHelper.h
//  Footplr
//
//  Created by EclipseKim on 2014. 8. 17..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

const static NSString *API_URL = @"http://player.footplr.com";
const static NSString *MANAGER_API_URL = @"http://footplr.com";

const static NSUInteger API_LOADING_VIEW_TAG = 2020;

@interface ApiHelper : NSObject < SessionDelegate >
{
    UIView *layer;
    UIActivityIndicatorView *activityIndicator;
    UIWindow *window;
}


@property (nonatomic, assign) id<SessionDelegate> delegate;
+ (ApiHelper*)sharedApiHelper;


-(void) getRequestWithUrl:(const NSString*)url withPath:(NSString*)path withParameters:(NSDictionary*) params withSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback;

-(void) getRequestWithUrl:(const NSString*)url withPath:(NSString*)path withParameters:(NSDictionary*) params withSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback withErrorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback;

-(void) postRequestWithUrl:(const NSString*)url withPath:(NSString*)path withParameters:(NSDictionary*) params withSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback;

-(void) postRequestWithUrl:(const NSString*)url withPath:(NSString*)path withParameters:(NSDictionary*) params withSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback withErrorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback;
@end
