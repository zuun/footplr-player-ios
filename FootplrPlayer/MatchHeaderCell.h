//
//  MatchHeaderCell.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 6. 11..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchHeaderCell : UITableViewCell

@property UIImageView *coverImageView;
@property UIImageView *coverShadowImageView;

@property UILabel *titleLabel;
@property UILabel *subTitleLabel;
@property UILabel *homeScoreLabel;
@property UILabel *matchStatusLabel;
@property UILabel *awayScoreLabel;
@property UILabel *homeTeamNameLabel;
@property UILabel *awayTeamNameLabel;

@property UIButton *goalToggleButton;
@end
