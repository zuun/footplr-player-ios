//
//  AttendWinningRateCell.m
//  footplr
//
//  Created by EclipseKim on 2015. 5. 30..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "AttendWinningRateCell.h"

@implementation AttendWinningRateCell
@synthesize aWinningRateProgressLabel, naWinningRateProgressLabel;

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        [self setBackgroundColor:[UIColor clearColor]];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        
        // attendedTextLabel
        attendedTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 14, 140, 16)];
        attendedTextLabel.font = [UIFont systemFontOfSize:12.0f];
        attendedTextLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        attendedTextLabel.textAlignment = NSTextAlignmentCenter;
        attendedTextLabel.text = NSLocalizedString(@"s74", nil);
        [self.contentView addSubview:attendedTextLabel];
        
        // notAttendedTextLabel
        notAttendedTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(170, 14, 140, 16)];
        notAttendedTextLabel.font = [UIFont systemFontOfSize:12.0f];
        notAttendedTextLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        notAttendedTextLabel.textAlignment = NSTextAlignmentCenter;
        notAttendedTextLabel.text = NSLocalizedString(@"s75", nil);
        [self.contentView addSubview:notAttendedTextLabel];
        
        //ratingTextLabel
        ratingTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(135, 81, 50, 16)];
        ratingTextLabel.font = [UIFont systemFontOfSize:12.0f];
        ratingTextLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        ratingTextLabel.textAlignment = NSTextAlignmentCenter;
        ratingTextLabel.text = NSLocalizedString(@"s76", nil);
        ratingTextLabel.numberOfLines = 0;
        ratingTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [ratingTextLabel sizeToFit];
        
        CGRect ratingTextLabelFrame = ratingTextLabel.frame;
        ratingTextLabelFrame.origin.x = 135;
        ratingTextLabelFrame.size.width = 50;
        ratingTextLabel.frame = ratingTextLabelFrame;
        [self.contentView addSubview:ratingTextLabel];
        
        // aWinningRateProgressLabel
        aWinningRateProgressLabel = [[KAProgressLabel alloc] initWithFrame:CGRectMake(35, 44, 90, 90)];
        [aWinningRateProgressLabel setBackgroundColor:[UIColor clearColor]];
        [aWinningRateProgressLabel setFillColor:[UIColor clearColor]];
        
        [aWinningRateProgressLabel setTrackColor:[[UIManager sharedUIManager] appColor:COLOR_WD]];
        [aWinningRateProgressLabel setProgressColor:[[UIManager sharedUIManager] colorWithRed:255 withGreen:198 withBlue:0]];
        
        aWinningRateProgressLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_LIGHT withSize:27.0f];
        aWinningRateProgressLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [self.contentView addSubview:aWinningRateProgressLabel];
        
        // naWinningRateProgressLabel
        naWinningRateProgressLabel = [[KAProgressLabel alloc] initWithFrame:CGRectMake(195, 44, 90, 90)];
        [naWinningRateProgressLabel setBackgroundColor:[UIColor clearColor]];
        [naWinningRateProgressLabel setFillColor:[UIColor clearColor]];
        
        [naWinningRateProgressLabel setTrackColor:[[UIManager sharedUIManager] appColor:COLOR_WD]];
        [naWinningRateProgressLabel setProgressColor:[[UIManager sharedUIManager] colorWithRed:255 withGreen:198 withBlue:0]];
        
        naWinningRateProgressLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_LIGHT withSize:27.0f];
        naWinningRateProgressLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        [self.contentView addSubview:naWinningRateProgressLabel];
    }
    return self;
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
