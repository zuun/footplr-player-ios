//
//  DateHelper.h
//  Footplr
//
//  Created by EclipseKim on 2014. 8. 13..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject

+ (DateHelper*)sharedDateHelper;
- (NSString *) getWeekDay3CodeString:(NSDate*) date;
- (NSString *) getHumanizedDateStringForDate:(NSDate*)date withDay:(BOOL) withDay;
- (NSInteger) getWeekDay:(NSDate*) date;
- (NSString*) getWeekDayName:(NSInteger)weekday;
- (NSString*) getMonthName:(int)month;
- (NSUInteger) getMonth:(NSDate*) date;
- (NSUInteger) getDay:(NSDate *) date;
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
@end
