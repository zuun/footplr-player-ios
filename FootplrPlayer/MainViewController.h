//
//  MainViewController.h
//  FootplrPlayer
//
//  Created by EclipseKim on 2015. 5. 5..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"
#import "RatingDashboardView.h"
#import "RateViewController.h"
enum menuTypes
{
    MENU_SCHEDULE = 10,
    MENU_STATS
};

enum mainVCViewTags
{
    MENU_BUTTON_UNDERLINE = 101
};

@interface MainViewController : FootplrViewController <UITableViewDataSource, UITableViewDelegate, RateViewControllerDelegate>
{
    int page;
    int perPage;
    
    // schedules : array of (array of monthly schedules) X(reverted)
    NSMutableArray *schedules;
    
    
    NSString *prevMonthKey;
    
    UIView *tableHeader;
    RatingDashboardView *ratingDashboard;
    
    UIView *tableHeader2;
    RatingDashboardView *ratingDashboard2;
    
    NSArray *annualStats;
    NSUInteger currentMenu;
    NSDictionary *userStats;
    
    UIScrollView *titleView;
    
    BOOL isLoading;
    BOOL isDone;
    
}
@property int teamId;
@property (weak, nonatomic) IBOutlet UITableView *scheduleTableView;
@property (weak, nonatomic) IBOutlet UITableView *statsTableView;
@end
