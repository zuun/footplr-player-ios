//
//  AnnualStatsCell.h
//  footplr
//
//  Created by EclipseKim on 2015. 5. 28..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnualStatsCell : UITableViewCell

@property (strong, nonatomic) UILabel *yearLabel;
@property (strong, nonatomic) UILabel *attendedLabel;
@property (strong, nonatomic) UILabel *goalLabel;
@property (strong, nonatomic) UILabel *assistLabel;
@property (strong, nonatomic) UILabel *momLabel;
@property (strong, nonatomic) UILabel *ratingsLabel;
@end
