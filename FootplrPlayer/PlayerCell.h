//
//  PlayerCell.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 13..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerCell : UITableViewCell

@property (nonatomic, retain) UILabel *backNumberLabel;
@property (nonatomic, retain) UILabel *playerNameLabel;
@property (nonatomic, retain) UILabel *positionLabel;
@end
