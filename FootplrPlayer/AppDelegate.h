//
//  AppDelegate.h
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 2. 23..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

