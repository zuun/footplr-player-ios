//
//  RatingResultView.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 22..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "RatingResultView.h"

@implementation RatingResultView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _init];
    }
    return self;
}

-(void) _init
{
    CGFloat width = CGRectGetWidth(self.frame);
    
    UIView *wrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 30)];
    
    ratingLabel = [UILabel new];
    ratingLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_LIGHT withSize:18.0f];
    ratingLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
    [ratingLabel setFrame:CGRectMake(0, 0, width, 19)];
    [wrapperView addSubview:ratingLabel];
    
    starFillView = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetHeight(ratingLabel.frame) + 4, 0, 7)];
    [starFillView setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:255 withGreen:198 withBlue:0]];
    [wrapperView addSubview:starFillView];
    
    ratingImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"schedule_list_star"]];
    [ratingImageView setFrame:CGRectMake(10, CGRectGetHeight(ratingLabel.frame) + 4, 40, 7)];
    [wrapperView addSubview:ratingImageView];
    
//    wrapperView.center = [self convertPoint:self.center fromCoordinateSpace:self.superview];
    [self addSubview:wrapperView];
    
    rating = 0.0f;
    [self update];
}

-(void) setRating:(CGFloat)_rating
{
    rating = _rating;

    CGRect starFillViewFrame = starFillView.frame;
    starFillViewFrame.size.width = 40 * _rating / 10;
    starFillView.frame = starFillViewFrame;
    
    [self update];
}

-(void) update
{
    ratingLabel.text = [NSString stringWithFormat:@"%.2f" ,rating];
    [ratingLabel sizeToFit];
    CGPoint ratingLabelCenter = ratingLabel.center;
    ratingLabelCenter.x = self.center.x;
    ratingLabel.center = ratingLabelCenter;
    
    // image
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
