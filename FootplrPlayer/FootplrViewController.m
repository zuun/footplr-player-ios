//
//  FootplrViewController.m
//  Footplr
//
//  Created by EclipseKim on 2014. 7. 13..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "FootplrViewController.h"
#import "EntryViewController.h"


@interface FootplrViewController ()

@end

@implementation FootplrViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // log class name
    NSLog(NSStringFromClass([self class]));
//    NSLog(NSStringFromCGRect(self.view.frame));
    
    [Flurry logPageView];
    [[ApiHelper sharedApiHelper] setDelegate:self];
    [[SessionManager sharedSessionManager] setDelegate:self];
    
    self.navigationItem.backBarButtonItem = nil;
    
    // background color
    [self.view setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    
    // hide uinavigation bar bottom line
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.clipsToBounds = YES;
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;

    
    self.navigationController.navigationBar.barTintColor = [[UIManager sharedUIManager] appColor:COLOR_BG];
    self.navigationController.navigationBar.translucent = NO;
    
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void) back
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) setNavigationBackButton
{
    int vcCount = (int)[self.navigationController.viewControllers count];
    if(vcCount == 1) return;
    
    UIBarButtonItem* backButton = [UIBarButtonItem barItemWithImageName:@"nav_back_n.png" target:self action:@selector(back)];
    [self setLeftBarButtons:@[backButton]];
}

-(void) setNavigationTitle:(NSString*)title
{
    UILabel* titleLabel = [[UILabel alloc] init];
//    titleLabel.textColor = [UIColor colorWithRed:56/255.0f green:59/255.0f blue:67/255.0f alpha:1.0];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = title;
    titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
}

-(void) showFootplrTitleView
{
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_logo.png"]];
}


-(void) showLoginViewController
{
    EntryViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"EntryStoryboard"];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:evc];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = navigationController;
}



-(void) setLeftBarButtons:(NSArray *)array
{
    NSMutableArray *leftButtons = [[NSMutableArray alloc] init];
    for(UIBarButtonItem *button in array) {
        if(button.tag == BAR_BUTTON_ICON_IMAGE_TAG) { // if image button, remove space
            UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                               initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                               target:nil action:nil];
            negativeSpacer.width = -10;// it was -6 in iOS 6

            [leftButtons addObject:negativeSpacer];
            [leftButtons addObject:button];
        } else {
            [leftButtons addObject:button];
        }
    }
    [self.navigationItem setLeftBarButtonItems:leftButtons animated:YES];
}
-(void) setRightBarButtons:(NSArray *)array
{
    NSMutableArray *rightButtons = [[NSMutableArray alloc] init];
    for(UIBarButtonItem *button in array) {
        if(button.tag == BAR_BUTTON_ICON_IMAGE_TAG) { // if image button, remove space
            UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                               initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                               target:nil action:nil];
            negativeSpacer.width = -10;// it was -6 in iOS 6
            
            [rightButtons addObject:negativeSpacer];
            [rightButtons addObject:button];
        } else {
            [rightButtons addObject:button];
        }
    }
    [self.navigationItem setRightBarButtonItems:rightButtons animated:YES];
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
