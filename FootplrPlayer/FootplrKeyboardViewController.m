//
//  FootplrKeyboardViewController.m
//  Footplr
//
//  Created by JunhyuckKim on 14. 7. 16..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "FootplrKeyboardViewController.h"
#import "EntryViewController.h"

@interface FootplrKeyboardViewController ()

@end

@implementation FootplrKeyboardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // KEYBOARD CALLBACK;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView*) getFirstResponderInView:(UIView*) v
{
    for (UIView *view in [v subviews]) {
        if ([view isFirstResponder]) {
            return view;
        } else {
            if([self getFirstResponderInView:view] == nil) continue;
            else return [self getFirstResponderInView:view];
        }
    }
    return nil;
}

-(UIView*) getLastSuperView:(UIView*) v
{
    if(v.superview && v.superview.frame.size.height != self.view.frame.size.height) { //if super view is not self.view
        return [self getLastSuperView:v.superview];
    } else { //
        return v;
    }
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSLog(@"keyboardWillShow");
    [self showKeyboard];
}

-(void) showKeyboard
{
    // textField
    UIView* firstResponderView = [self getFirstResponderInView:self.view];
    
    UIView* lastSuperView = [self getLastSuperView:firstResponderView];
    
    [[FootplrHelper sharedFootplrHelper] logFrame:lastSuperView.frame];
    
    
    // get inputBottomY
    CGFloat inputBottomY = 0;
    for(UIView* v = firstResponderView ; v != lastSuperView ; v = v.superview) {
        inputBottomY += v.frame.origin.y;
    }
    
    static NSInteger margin = 10;
    inputBottomY += lastSuperView.frame.origin.y + firstResponderView.frame.size.height + margin;
    CGFloat inputOriginY = inputBottomY - firstResponderView.frame.size.height - margin;
    float navigationHeight = self.navigationController.navigationBarHidden ? 0 : 44;
    static CGFloat keyboardHeight = 216.0f;
    CGFloat keyboardOriginY = self.view.frame.size.height + navigationHeight - keyboardHeight;
    
    NSLog(@"inputOriginY %f", inputOriginY);
    NSLog(@"inputBottomY %f", inputBottomY);
    NSLog(@"keyboardOriginY %f", keyboardOriginY);
    
    NSLog(@"view height %f", self.view.frame.size.height);
    CGFloat contentOffsetY = 0;
    if([self.view isKindOfClass:[UIScrollView class]]) {
        contentOffsetY = ((UIScrollView*)self.view).contentOffset.y;
    } else if(inputWrapperScrollView) {
        contentOffsetY = inputWrapperScrollView.contentOffset.y;
    }
    NSLog(@"contentOffSetY : %f", contentOffsetY);
    
    
    if(inputBottomY - contentOffsetY >= keyboardOriginY) { // 216 :keyboard size // if(view's bottom is lower than keyboard top
        static NSInteger scrollMargin = 40;
        CGFloat y = -(inputBottomY + 20 + scrollMargin - keyboardOriginY) + contentOffsetY;
        [self.view scrollToY:y];

        //        [self.view scrollToY:-(inputBottomY - firstResponderView.frame.size.height-10)];
        
        //        float distance = inputBottomY - ( self.view.frame.size.height - 216 ) + 44;
        ////        self.view.center = CGPointMake(self.view.center.x, self.view.frame.size.height/2 - distance); // 10 for margin
        //        CGRect f = self.view.frame;
        //        f.origin.y = -1 * distance;  //set the -35.0f to your required value
        //        self.view.frame = f;
    }
    if(inputWrapperScrollView) {
        inputWrapperScrollView.frame = CGRectMake(inputWrapperScrollViewFrame.origin.x, inputWrapperScrollViewFrame.origin.y, inputWrapperScrollViewFrame.size.width, inputWrapperScrollViewFrame.size.height - 216 - CGRectGetMinY(self.view.frame));
    }
}

-(void) scrollToCurrentTextField
{
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [self.view  scrollToY:0];
    inputWrapperScrollView.frame = CGRectMake(inputWrapperScrollView.frame.origin.x, inputWrapperScrollView.frame.origin.y, inputWrapperScrollView.frame.size.width, CGRectGetMaxY(self.view.frame) - CGRectGetMinY(inputWrapperScrollView.frame) - CGRectGetMinY(self.view.frame));
//    if(_inputWrapperScrollView) {
//        _inputWrapperScrollView.frame = _inputWrapperScrollViewFrame;
//    }
//    CGRect f = self.view.frame;
//    if(!self.navigationController.navigationBarHidden)
//        f.origin.y = 64.0f;
//    else
//        f.origin.y = 20.0f;
//    self.view.frame = f;
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.phase==UITouchPhaseBegan){
        [self hideKeyboard];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void) hideKeyboard
{
    NSLog(@"hide");
    [[[FootplrHelper sharedFootplrHelper] getFirstResponderInView:self.view] resignFirstResponder];
}

-(void) showLoginViewController
{
    EntryViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"EntryStoryboard"];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:evc];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = navigationController;
}

-(void) setInputWrapperScrollView:(UIScrollView *) scrollView
{
    inputWrapperScrollView = scrollView;
    inputWrapperScrollViewFrame = scrollView.frame;
}
@end
