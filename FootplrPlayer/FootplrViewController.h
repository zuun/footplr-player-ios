//
//  FootplrViewController.h
//  Footplr
//
//  Created by EclipseKim on 2014. 7. 13..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBarButtonItem+PlainText.h"
#import "UIBarButtonItem+IconImage.h"

enum alertViewTags {
    ALERT_NAV_POP_ON_CONFIRM = 3001
};

static NSString * const RES_URL = @"http://footplr.com";

@interface FootplrViewController : UIViewController <SessionDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>

-(void) back;
-(void) setLeftBarButtons:(NSArray *)array;
-(void) setRightBarButtons:(NSArray *)array;
- (UIViewController *)backViewController;
-(void) setNavigationTitle:(NSString*)title;
-(void) showFootplrTitleView;
-(void) setNavigationBackButton;
-(void) showErrorAlertView;
-(void) showLoginViewController;
-(void) logout;
@end
