//
//  UIView+FormScroll.h
//  Footplr
//
//  Created by JunhyuckKim on 14. 9. 23..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FormScroll)

-(void)scrollToY:(float)y;
-(void)scrollToView:(UIView *)view;
-(void)scrollElement:(UIView *)view toPoint:(float)y;
@end
