//
//  ScheduleCell.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 20..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "ScheduleCell.h"

@implementation ScheduleCell
-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
    
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        CGFloat cellWidth = window.frame.size.width;
        
        // dateLabel
        _dateLabel = [UILabel new];
        [_dateLabel setFrame:CGRectMake(0, 0, 40, 18)];
        _dateLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_SYSTEM withSize:18.f];
        _dateLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        _dateLabel.textAlignment = NSTextAlignmentCenter;
        
        // dayLabel
        _dayLabel = [UILabel new];
        [_dayLabel setFrame:CGRectMake(0, 22, 40, 10)];
        _dayLabel.font = [UIFont systemFontOfSize:9.f];
        _dayLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        _dayLabel.textAlignment = NSTextAlignmentCenter;
        
        UIView *dateView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 40, CGRectGetMaxY(_dayLabel.frame))];
        [dateView addSubview:_dateLabel];
        [dateView addSubview:_dayLabel];
        [dateView setCenter:CGPointMake(30, 36)];
        [self addSubview:dateView];
        
        // titleLabel
        _titleLabel = [UILabel new];
        [_titleLabel setFrame:CGRectMake(60, 13, cellWidth - 120, 15)];
        _titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
        _titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        [self addSubview:_titleLabel];
        
        // descriptionLabel
        _descriptionLabel = [UILabel new];
        [_descriptionLabel setFrame:CGRectMake(60, 32, cellWidth - 120, 27)];
        _descriptionLabel.font = [UIFont systemFontOfSize:11.0f];
        _descriptionLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BC];
        _descriptionLabel.numberOfLines = 2;
        _descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:_descriptionLabel];
        
        // scoreResultLabel
        _scoreResultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 13, 0, 15)];
        _scoreResultLabel.font = [UIFont systemFontOfSize:14.0f];
        _scoreResultLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        [_scoreResultLabel setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_LIST_SEPERATOR]];
        _scoreResultLabel.textAlignment = NSTextAlignmentCenter;
        
        _scoreResultLabel.layer.cornerRadius = 2;
        _scoreResultLabel.layer.masksToBounds = YES;
        [self addSubview:_scoreResultLabel];
        
        // divisionLineImageView
        _divisionLineImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"schedule_list_div_line"]];
        [_divisionLineImageView setFrame:CGRectMake(cellWidth - 61, 16, 1, 40)];
        [self addSubview:_divisionLineImageView];
        
        
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(cellWidth-60, 0, 60, 72)];
        [self addSubview:rightView];
        
        // rateButton
        _rateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rateButton setBackgroundImage:[[UIImage imageNamed:@"btn_red_n"] stretchableImageWithLeftCapWidth:4 topCapHeight:4] forState:UIControlStateNormal];
        [_rateButton setTitle:NSLocalizedString(@"s64", nil) forState:UIControlStateNormal];
        _rateButton.titleLabel.font = [UIFont systemFontOfSize:11.0f];
        [_rateButton setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_WA] forState:UIControlStateNormal];
        [_rateButton setFrame:CGRectMake(0, 5, 50, 22)];
        _rateButton.center = [rightView convertPoint:rightView.center fromView:rightView.superview];
        [rightView addSubview:_rateButton];
        
         // scheduleTypeImageView
        _scheduleTypeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        _scheduleTypeImageView.center = [rightView convertPoint:rightView.center fromView:rightView.superview];
        [rightView addSubview:_scheduleTypeImageView];
        
        // ratingView
        _ratingView = [[RatingResultView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
        _ratingView.center = [rightView convertPoint:rightView.center fromView:rightView.superview];
        
        [rightView addSubview:_ratingView];
        
        [super layoutSubviews];
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
