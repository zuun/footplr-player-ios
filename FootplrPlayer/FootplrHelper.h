//
//  FootplrHelper.h
//  Footplr
//
//  Created by EclipseKim on 2014. 2. 9..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FootplrHelper : NSObject

+ (FootplrHelper*)sharedFootplrHelper;

/* UI */
-(UIView*) getFirstResponderInView:(UIView*) v;
-(void) logFrame:(CGRect) frame;
/* UI END */

-(void) showFootplrAlertView:(id)delegate withMessage:(NSString*)message;
-(void) showError;
-(NSString *) getAlertViewTitle;
-(NSString *) getDeviceLocale;
-(NSString *) getDeviceTypeForLog;
@end
