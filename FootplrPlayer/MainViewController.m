//
//  MainViewController.m
//  FootplrPlayer
//
//  Created by EclipseKim on 2015. 5. 5..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "MainViewController.h"
#import "ScheduleCell.h"
#import "AnnualStatsCell.h"
#import "AttendWinningRateCell.h"
#import "AttendStatsCell.h"
#import "RatingStatsCell.h"
#import "MatchDetailViewController.h"
#import "SettingsViewController.h"
#import "WebViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"asdf");
    [self _init];
}

-(void) viewDidAppear:(BOOL)animated
{
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated
{
    [titleView removeFromSuperview];
}

-(void) _init
{
    UIBarButtonItem *showTeamPageBtn = [UIBarButtonItem barItemWithImageName:@"nav_teamweb_n" target:self action:@selector(showTeamPage)];
    [self setLeftBarButtons:@[showTeamPageBtn]];
    
    UIBarButtonItem* settingsBtn = [UIBarButtonItem barItemWithImageName:@"nav_setting_n" target:self action:@selector(settings)];
    [self setRightBarButtons:@[settingsBtn]];
    
    
    titleView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 44.0)];
    [titleView setContentSize:CGSizeMake(0.0, 88.0)];
    titleView.scrollEnabled = NO;
    
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    NSDictionary *userInfo = [userData objectForKey:USER_INFO];
    NSString *userName = @"";
    if(userInfo) {
        userName = [userInfo objectForKey:@"member_nm"];
    }
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 60, CGRectGetWidth(titleView.frame), 22)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[[UIManager sharedUIManager] fontWithWeight:FONT_MEDIUM withSize:18.0f]];
    titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    titleLabel.text = userName;
    [titleView addSubview:titleLabel];

    
    self.navigationItem.titleView = titleView;
    
    schedules = [NSMutableArray new];
    page = 1;
    perPage = 10;
    currentMenu = MENU_SCHEDULE;
    
    _scheduleTableView.tag = MENU_SCHEDULE;
    [_scheduleTableView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    _statsTableView.tag = MENU_STATS;
    [_statsTableView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    _statsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _statsTableView.hidden = YES;
    
    ratingDashboard = [[RatingDashboardView alloc] initWithFrame:CGRectMake(0, 0, 320, 276)];
    _scheduleTableView.tableHeaderView = ratingDashboard;

    ratingDashboard2 = [[RatingDashboardView alloc] initWithFrame:CGRectMake(0, 0, 320, 276)];
    _statsTableView.tableHeaderView = ratingDashboard2;

    // API
    isLoading = NO;
    isDone = NO;
    [self getSchedules];
    [self getUserStats];
}

-(void) getSchedules
{
    if(isLoading || isDone) return ;
    isLoading = YES;
    [[ApiHelper sharedApiHelper]
     postRequestWithUrl:API_URL
     withPath:[NSString stringWithFormat:@"/api/team/schedule/%d/%d/%d/2", _teamId, page, perPage]
     withParameters:nil
     withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
//         NSLog(@"get schedules %@", responseObject);
         if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
             NSArray *_schedules = [responseObject objectForKey:@"result"];
             
             
             if([_schedules count] == 0) { // all data loaded
                 isDone = YES;
                 isLoading = NO;
                 return ;
             }
             for(NSDictionary *schedule in _schedules) {
                 Schedule *s = [[Schedule alloc] initWithScheduleInfo:schedule];
                 if(!prevMonthKey || [prevMonthKey isEqualToString:@""] || [prevMonthKey isEqualToString:s.monthKey]) { // first time, prevkey not set yet or month changed
                     prevMonthKey = s.monthKey;
                     
//                     [schedules addObject:@""];
                     
                     // new array
//                     NSMutableArray *monthlySchedule = [NSMutableArray new];
//                     [monthlySchedule addObject:s];
                     
//                     [schedules addObject:monthlySchedule];
                 } else if([prevMonthKey isEqualToString:s.monthKey]) {
//                     NSMutableArray *monthlySchedule = [[schedules lastObject] mutableCopy];
//                     [monthlySchedule addObject:s];
//                     
//                     [schedules replaceObjectAtIndex:[schedules count]-1 withObject:monthlySchedule];
                 }
                 [schedules addObject:s];
             }
             
             
             page++;
             isLoading = NO;
             [_scheduleTableView reloadData];
         } else {
             isLoading = NO;
         }
    } withErrorCallback:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[FootplrHelper sharedFootplrHelper] showError];
        isLoading = NO;
    }];
}

-(void) getUserStats
{
    int memberId = [[[[NSUserDefaults standardUserDefaults] objectForKey:USER_INFO] objectForKey:@"member_id"] intValue];
    [[ApiHelper sharedApiHelper]
     postRequestWithUrl:API_URL
     withPath:[NSString stringWithFormat:@"/api/member/record/%d/2", memberId]
     withParameters:nil
     withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"user rating : %@", responseObject);
         if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
             NSDictionary *ratingInfo = [responseObject objectForKey:@"result"];
             
             // for dashboard
             CGFloat rating = [[ratingInfo objectForKey:@"rating"] floatValue];
             NSArray *recentRatings = [[(NSArray *)[ratingInfo objectForKey:@"recent_rating"] reverseObjectEnumerator] allObjects];
             [ratingDashboard setRating:rating withRecentRatings:recentRatings];
             [ratingDashboard2 setRating:rating withRecentRatings:recentRatings];
             
             // annualStats
             annualStats = [ratingInfo objectForKey:@"stat"];
             userStats = ratingInfo;
             
             [_statsTableView reloadData];
         }
     }];
}

#pragma mark - UITableViewDataSource

//-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return (tableView.tag == MENU_SCHEDULE) ? [schedules count] : 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == MENU_SCHEDULE) {
        return [schedules count];
    } else {
        return 8 + [annualStats count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == MENU_SCHEDULE) { // schedule
        ScheduleCell *cell = [[ScheduleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ScheduleCell"];
//        NSArray *monthlySchedule = [schedules objectAtIndex:indexPath.section];
//        Schedule *schedule = [monthlySchedule objectAtIndex:indexPath.row];
        Schedule *schedule = [schedules objectAtIndex:indexPath.row];
        
        cell.dayLabel.text = [[DateHelper sharedDateHelper] getWeekDayName:[[DateHelper sharedDateHelper] getWeekDay:schedule.matchTime]];
        cell.dateLabel.text = [NSString stringWithFormat:@"%lu", [[DateHelper sharedDateHelper] getDay:schedule.matchTime]];
        cell.titleLabel.text = schedule.title;
        
        cell.descriptionLabel.text = [NSString stringWithFormat:@"%@\n%@", schedule.matchTimeString, schedule.location];
        
        if(schedule.isMatchDone) { // match done
            cell.scoreResultLabel.text = [NSString stringWithFormat:@"%d:%d", schedule.gf, schedule.ga];
            [cell.scoreResultLabel sizeToFit];
            
            [cell.titleLabel sizeToFit];
            
            CGFloat margin = 5;
            CGRect scoreResultLabelFrame = cell.scoreResultLabel.frame;
            scoreResultLabelFrame.origin.x = CGRectGetMaxX(cell.titleLabel.frame) + margin;
            scoreResultLabelFrame.size.width += 10;
            cell.scoreResultLabel.frame = scoreResultLabelFrame;
            
            if(CGRectGetMaxX(cell.scoreResultLabel.frame) + margin > CGRectGetMinX(cell.divisionLineImageView.frame)) { // if scoreLabel is wider than area
                NSLog(@"%d row", indexPath.row);
                CGRect scoreResultLabelFrame = cell.scoreResultLabel.frame;
                scoreResultLabelFrame.origin.x = CGRectGetMinX(cell.divisionLineImageView.frame) - margin - scoreResultLabelFrame.size.width;
                cell.scoreResultLabel.frame = scoreResultLabelFrame;
                
                CGRect titleLabelFrame = cell.titleLabel.frame;
                titleLabelFrame.size.width = CGRectGetMinX(scoreResultLabelFrame) - margin - CGRectGetMinX(titleLabelFrame);
                cell.titleLabel.frame = titleLabelFrame;
            }
            
            if(schedule.attended) { //attended
                if(schedule.rated) { // already rated
                    cell.rateButton.hidden = YES;
                    cell.ratingView.hidden = NO;
                    [cell.ratingView setRating:schedule.rating];
//                    cell.scheduleTypeImageView.image = [UIImage imageNamed:@"schedule_ic_fultime"];
                } else { // attended, but not rated yet
                    cell.rateButton.hidden = NO;
                    cell.ratingView.hidden = YES;
//                    cell.rateButton.tag = indexPath.section * 1000 + indexPath.row;
                    cell.rateButton.tag = indexPath.row;
                    [cell.rateButton addTarget:self action:@selector(rateMatch:) forControlEvents:UIControlEventTouchUpInside];
                    if(schedule.ratingExpired) {
                        cell.rateButton.hidden = YES;
                        cell.scheduleTypeImageView.image = [UIImage imageNamed:@"schedule_ic_fultime"];
                    } else {
                        cell.scheduleTypeImageView.image = nil;
                    }
                }
            } else { // not attended
                cell.rateButton.hidden =  YES;
                cell.ratingView.hidden = YES;
                cell.scheduleTypeImageView.image = [UIImage imageNamed:@"schedule_ic_fultime"];
            }
        } else {
            cell.rateButton.hidden = YES;
            cell.ratingView.hidden = YES;
            if(schedule.type == MATCH) {
                cell.scheduleTypeImageView.image = [UIImage imageNamed:@"schedule_ic_match"];
            } else {
                cell.scheduleTypeImageView.image = [UIImage imageNamed:@"schedule_ic_event"];
            }
        }
        return cell;
    } else { // stats
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        [cell.contentView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
        cell.textLabel.font = [UIFont systemFontOfSize:12.0f];
        cell.textLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
//        if(indexPath.section == 0) { // annual stats
            if(indexPath.row == 0) {
                cell.textLabel.text = NSLocalizedString(@"s66", nil);
            } else if(indexPath.row == 1) {
                AnnualStatsCell *_cell = [tableView dequeueReusableCellWithIdentifier:@"AnnualStatsCell"];
                if(!_cell) {
                    _cell = [[AnnualStatsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AnnualStatsCell"];
                }
                _cell.yearLabel.text = NSLocalizedString(@"s67", nil);
                _cell.attendedLabel.text = NSLocalizedString(@"s68", nil);
                _cell.goalLabel.text = NSLocalizedString(@"s69", nil);
                _cell.assistLabel.text = NSLocalizedString(@"s70", nil);
                _cell.momLabel.text = NSLocalizedString(@"s71", nil);
                _cell.ratingsLabel.text = NSLocalizedString(@"s72", nil);
                cell = _cell;
            } else if( indexPath.row == 2 + [annualStats count]) { // total
                AnnualStatsCell *_cell = [tableView dequeueReusableCellWithIdentifier:@"AnnualStatsCell2"];
                if(!_cell) {
                    _cell = [[AnnualStatsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AnnualStatsCell2"];
                }
            

                int attendedSum = 0;
                int goalSum = 0;
                int assistSum = 0;
                int momSum = 0;
                CGFloat ratingsAvg = 0;


                NSArray *assistStats = [userStats objectForKey:@"assist_cnt"];
                for(NSDictionary *assistStat in assistStats) {
                    assistSum += [[assistStat objectForKey:@"cnt"] intValue];
                }

                NSArray *goalStats = [userStats objectForKey:@"goal_cnt"];
                for(NSDictionary *goalStat in goalStats) {
                    goalSum += [[goalStat objectForKey:@"cnt"] intValue];
                }

                for(NSDictionary *annualStat in annualStats) {
                    int attendCount = [[annualStat objectForKey:@"attend_cnt"] intValue];

                    attendedSum += attendCount;
                    momSum += [[annualStat objectForKey:@"mom"] intValue];
                    ratingsAvg += [[annualStat objectForKey:@"rate"] floatValue] * attendCount;
                }
                if(attendedSum != 0) {
                    ratingsAvg /= attendedSum;
                }

                _cell.yearLabel.text = NSLocalizedString(@"s73", nil);
                _cell.attendedLabel.text = [NSString stringWithFormat:@"%d", attendedSum];
                _cell.goalLabel.text = [NSString stringWithFormat:@"%d", goalSum];
                _cell.assistLabel.text = [NSString stringWithFormat:@"%d", assistSum];
                _cell.momLabel.text = [NSString stringWithFormat:@"%d", momSum];
                _cell.ratingsLabel.text = [NSString stringWithFormat:@"%.2f", ratingsAvg];
                
                UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 29.5, 320, 0.5)];
                [line setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WD]];
                [_cell.contentView addSubview:line];
                cell = _cell;
            } else if(indexPath.row > 1 && indexPath.row <= 1 + [annualStats count]){
                AnnualStatsCell *_cell = [tableView dequeueReusableCellWithIdentifier:@"AnnualStatsCell"];
                if(!_cell) {
                    _cell = [[AnnualStatsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AnnualStatsCell"];
                }
                NSDictionary *annualStat = [annualStats objectAtIndex:indexPath.row - 2];
                
                NSString *season = [annualStat objectForKey:@"season"];
                
                int assist = 0;
                NSArray *assistStats = [userStats objectForKey:@"assist_cnt"];
                for(NSDictionary *assistStat in assistStats) {
                    if([[assistStat objectForKey:@"season"] isEqualToString:season]) {
                        assist = [[assistStat objectForKey:@"cnt"] intValue];
                        break;
                    }
                }
//                if([assistStats count] > indexPath.row - 2) assist = [[[assistStats objectAtIndex:indexPath.row - 2] objectForKey:@"cnt"] intValue];

                int goal = 0;
                NSArray *goalStats = [userStats objectForKey:@"goal_cnt"];
                for(NSDictionary *goalStat in goalStats) {
                    if([[goalStat objectForKey:@"season"] isEqualToString:season]) {
                        goal = [[goalStat objectForKey:@"cnt"] intValue];
                        break;
                    }
                }
//                if([goalStats count] > indexPath.row - 2) goal = [[[goalStats objectAtIndex:indexPath.row - 2] objectForKey:@"cnt"] intValue];

                _cell.yearLabel.text = season;
                _cell.attendedLabel.text = [annualStat objectForKey:@"attend_cnt"];
                _cell.goalLabel.text = [NSString stringWithFormat:@"%d", goal];
                _cell.assistLabel.text = [NSString stringWithFormat:@"%d", assist];
                _cell.momLabel.text = [annualStat objectForKey:@"mom"];
                _cell.ratingsLabel.text = [NSString stringWithFormat:@"%.2f", [[annualStat objectForKey:@"rate"] floatValue]];
                cell = _cell;
            }
//        } else if(indexPath.section == 1) { // attend stats
            else if(indexPath.row == 3 + [annualStats count]) {
                AttendWinningRateCell *_cell = [tableView dequeueReusableCellWithIdentifier:@"AttendWinningRateCell"];
                if(!_cell) {
                    _cell = [[AttendWinningRateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AttendWinningRateCell"];
                }
                NSDictionary *attendingStats = [userStats objectForKey:@"attending"];
                CGFloat aWinningRate = 0, naWinningRate = 0;
                NSLog(@"attendingStats : %@", attendingStats);
                
                if(attendingStats) {
                    int aCount = [[attendingStats objectForKey:@"attend_cnt"] intValue];
                    int aWinCount = [[attendingStats objectForKey:@"attend_win_cnt"] intValue];
                    int naCount = [[attendingStats objectForKey:@"not_attend_cnt"] intValue];
                    int naWinCount = [[attendingStats objectForKey:@"not_attend_win_cnt"] intValue];
                    aWinningRate = (aCount == 0) ? 0 : (CGFloat)aWinCount / aCount;
                    naWinningRate = (naCount == 0) ? 0 : (CGFloat)naWinCount / naCount;
                }
                
                [_cell.aWinningRateProgressLabel setProgress:aWinningRate];
                _cell.aWinningRateProgressLabel.text = [NSString stringWithFormat:@"%d%%", (int)(aWinningRate * 100)];
                [_cell.naWinningRateProgressLabel setProgress:naWinningRate];
                _cell.naWinningRateProgressLabel.text = [NSString stringWithFormat:@"%d%%", (int)(naWinningRate * 100)];
                cell = _cell;
            } else if(indexPath.row == 4 + [annualStats count] || indexPath.row == 5 + [annualStats count]){
                AttendStatsCell *_cell = [tableView dequeueReusableCellWithIdentifier:@"AttendStatsCell"];
                if(!_cell) {
                    _cell = [[AttendStatsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AttendStatsCell"];
                }
                NSDictionary *attendingStats = [userStats objectForKey:@"attending"];
                if(indexPath.row == 4 + [annualStats count]) { // GF
                    _cell.titleLabel.text = NSLocalizedString(@"s77", nil);
                    _cell.attendedValueLabel.text = [NSString stringWithFormat:@"%.1f", [[attendingStats objectForKey:@"attend_gf"] floatValue]];
                    _cell.notAttendedValueLabel.text = [NSString stringWithFormat:@"%.1f", [[attendingStats objectForKey:@"not_attend_gf"] floatValue]];
                } else { // GA
                    _cell.titleLabel.text = NSLocalizedString(@"s78", nil);
                    _cell.attendedValueLabel.text = [NSString stringWithFormat:@"%.1f", [[attendingStats objectForKey:@"attend_ga"] floatValue]];
                    _cell.notAttendedValueLabel.text = [NSString stringWithFormat:@"%.1f", [[attendingStats objectForKey:@"not_attend_ga"] floatValue]];
                    
                    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 49.5, 320, 0.5)];
                    [line setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WD]];
                    [_cell.contentView addSubview:line];
                }
                cell = _cell;
            } else if(indexPath.row >= 6 + [annualStats count]) {
                RatingStatsCell *_cell = [tableView dequeueReusableCellWithIdentifier:@"RatingStatsCell"];
                if(!_cell) {
                    _cell = [[RatingStatsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RatingStatsCell"];
                }
                
                NSDictionary *ratingInfo;
                if(indexPath.row == 6 + [annualStats count] ) { // best
                    _cell.titleLabel.text = NSLocalizedString(@"s80", nil);
                    ratingInfo = [userStats objectForKey:@"max_rating"];
                } else { // worst
                    _cell.titleLabel.text = NSLocalizedString(@"s81", nil);
                    ratingInfo = [userStats objectForKey:@"min_rating"];
                }

                if(ratingInfo && [ratingInfo count] > 0) { // if rating info exist
                    _cell.scheduleTitleLabel.text = [ratingInfo objectForKey:@"title"];
                    
                    NSString *matchPlace = [ratingInfo objectForKey:@"location"];
                    NSString *matchTime = [ratingInfo objectForKey:@"schedule"];
                    if(!matchPlace) matchPlace = @"";
                    if(matchTime) {
                        NSDateFormatter *df = [[NSDateFormatter alloc] init];
                        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSDate *_matchTime = [df dateFromString:matchTime];
                        matchTime = [[DateHelper sharedDateHelper] getHumanizedDateStringForDate:_matchTime withDay:YES];
                    } else {
                        matchTime = @"";
                    }
                    
                    _cell.scheduleDescriptionLabel.text = [NSString stringWithFormat:@"%@\n%@", matchTime, matchPlace];
                    
                    //ratingLabel
                    CGFloat rating = [[ratingInfo objectForKey:@"rate"] floatValue];
                    _cell.ratingLabel.text = [NSString stringWithFormat:@"%.1f", rating];
                    
                    // ratingView
                    _cell.ratingView.hidden = NO;
                    
                    // starFillView
                    [_cell.starFillView setFrame:CGRectMake(0, 0, 44 * rating / 10, 8)];
                } else {
                    // scheduleTitleLabel
                    _cell.scheduleTitleLabel.text = NSLocalizedString(@"s63", nil);
                    
                    // ratingLabel
                    _cell.ratingLabel.text = @"";
                    
                    // ratingView
                    _cell.ratingView.hidden = YES;
                }
                cell = _cell;
            }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(tableView.tag == MENU_SCHEDULE) {
//        NSArray *monthlySchedule = [schedules objectAtIndex:indexPath.section];
//        Schedule *schedule = [monthlySchedule objectAtIndex:indexPath.row];
        Schedule *schedule = [schedules objectAtIndex:indexPath.row];
        
        MatchDetailViewController *mdvc = [self.storyboard instantiateViewControllerWithIdentifier:@"MatchDetailStoryboard"];
        mdvc.schedule = schedule;
        [self.navigationController pushViewController:mdvc animated:YES];
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == MENU_SCHEDULE) return 72.0f;
    else {
            if(indexPath.row == 0) {
                return 44.0f;
            } else if(indexPath.row >= 1 && indexPath.row <= 2 + [annualStats count]){
                return 30.0f;
            }
            else if(indexPath.row == 3 + [annualStats count]) {
                return 134.0f;
            } else if(indexPath.row == 4 + [annualStats count] || indexPath.row == 5 + [annualStats count]) {
                return 50.0f;
            }else {
                return 100.0f;
            }
        }
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == MENU_SCHEDULE) {
//        NSArray *monthlySchedules = [schedules lastObject];
//        if(indexPath.row >= [monthlySchedules count]-3) {
        if(indexPath.row >= [schedules count] - 3) {
            [self getSchedules];
        }
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 44)];
    [header setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BA]];
    
    // schedule button
    UIButton *scheduleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [scheduleButton setFrame:CGRectMake(0, 0, 160, 44)];
    [scheduleButton setTitle:NSLocalizedString(@"s61", nil) forState:UIControlStateNormal];
    scheduleButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [scheduleButton setTitleColor:[[UIManager sharedUIManager] appColor:(currentMenu == MENU_SCHEDULE) ? COLOR_WA : COLOR_WC] forState:UIControlStateNormal];
    scheduleButton.tag = MENU_SCHEDULE;
    [scheduleButton addTarget:self action:@selector(toggleMenu:) forControlEvents:UIControlEventTouchUpInside];
    [scheduleButton setBackgroundColor:[UIColor clearColor]];
    
    // set underline
    UIView *underlineView = [[UIView alloc] initWithFrame:CGRectMake(0, 42, 160, 2)];
    [underlineView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WA]];
    underlineView.tag = MENU_BUTTON_UNDERLINE;
    underlineView.hidden = (currentMenu == MENU_STATS);
    [scheduleButton addSubview:underlineView];
    
    // stats button
    UIButton *statsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [statsButton setFrame:CGRectMake(160, 0, 160, 44)];
    [statsButton setTitle:NSLocalizedString(@"s62", nil) forState:UIControlStateNormal];
    statsButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [statsButton setTitleColor:[[UIManager sharedUIManager] appColor:(currentMenu == MENU_STATS) ? COLOR_WA : COLOR_WC] forState:UIControlStateNormal];
    statsButton.tag = MENU_STATS;
    [statsButton addTarget:self action:@selector(toggleMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    // set underline
    UIView *underlineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 42, 160, 2)];
    [underlineView2 setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_WA]];
    underlineView2.tag = MENU_BUTTON_UNDERLINE;
    underlineView2.hidden = (currentMenu == MENU_SCHEDULE);
    [statsButton addSubview:underlineView2];
    
    [header addSubview:scheduleButton];
    [header addSubview:statsButton];
    return header;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 0 ? 44.0f : 0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat contentOffsetY = scrollView.contentOffset.y;

    // titleView
    CGPoint contentOffset = CGPointMake(0.0, MIN(scrollView.contentOffset.y, 44.0));
    [titleView setContentOffset:contentOffset];

    // table header
    if(contentOffsetY <= 320) {
        if(scrollView.tag == MENU_SCHEDULE) {
            _statsTableView.contentOffset = _scheduleTableView.contentOffset;
        } else {
            _scheduleTableView.contentOffset = _statsTableView.contentOffset;
        }
    } else {
        if(scrollView.tag == MENU_SCHEDULE) {
            if(_statsTableView.contentOffset.y < 320) {
                _statsTableView.contentOffset = _scheduleTableView.contentOffset;
            }
        } else {
            if(_scheduleTableView.contentOffset.y < 320) {
                _scheduleTableView.contentOffset = _statsTableView.contentOffset;
            }
        }
    }
}

#pragma mark - Actions
-(void) rateMatch:(UIButton *) button
{
//    NSInteger section = button.tag / 1000;
//    NSInteger row = button.tag % 1000;
//    
//    NSArray *monthlySchedule = [schedules objectAtIndex:section];
//    Schedule *schedule = [monthlySchedule objectAtIndex:row];
    Schedule *schedule = [schedules objectAtIndex:button.tag];
    RateViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"RateStoryboard"];
    rvc.schedule = schedule;
    rvc.delegate = self;
    [self.navigationController pushViewController:rvc animated:YES];
}

#pragma mark - RateViewControllerDelegate
-(void) rated
{
    [_scheduleTableView reloadData];
}

-(void) toggleMenu:(UIButton *) button
{
    currentMenu = button.tag;

    if(button.tag == MENU_SCHEDULE) {
        _statsTableView.hidden = YES;
        _scheduleTableView.hidden = NO;
    } else {
        _statsTableView.hidden = NO;
        _scheduleTableView.hidden = YES;
    }
    [_scheduleTableView reloadData];
    [_statsTableView reloadData];
}

-(void) showTeamPage
{
//    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
//    int teamId = [[[userData objectForKey:USER_INFO] objectForKey:@"team_id"] intValue];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://footplr.com/team/overview/%d", teamId]]];
    
    
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    int teamId = [[[userData objectForKey:USER_INFO] objectForKey:@"team_id"] intValue];
    WebViewController *wvc = [[WebViewController alloc] init];
    wvc.urlString = [NSString stringWithFormat:@"http://footplr.com/team/overview/%d", teamId];
    [self.navigationController pushViewController:wvc animated:YES];
}

-(void) settings
{
    SettingsViewController *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsStoryboard"];
    [self.navigationController pushViewController:svc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
