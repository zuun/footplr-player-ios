//
//  PlayerRateCell.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 15..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "PlayerRateCell.h"

@implementation PlayerRateCell
-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {		// Initialization
        //avatarImageView
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 12, 60, 60)];
        _avatarImageView.image = [UIImage imageNamed:@"default_profile_photo"];
        _avatarImageView.layer.cornerRadius = 30;
        _avatarImageView.layer.masksToBounds = YES;
        [self addSubview:_avatarImageView];
        
        // playerNameLabel
        _playerNameLabel = [UILabel new];
        [_playerNameLabel setFrame:CGRectMake(80, 26, 230, 18)];
        _playerNameLabel.font = [[UIManager sharedUIManager] fontWithWeight:FONT_BOLD withSize:14.0f];
        _playerNameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        [self addSubview:_playerNameLabel];
        
        // positionLabel
        _positionLabel = [UILabel new];
        [_positionLabel setFrame:CGRectMake(80, 44, 230, 16)];
        _positionLabel.font = [UIFont systemFontOfSize:12.0f];
        _positionLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BC];
        [self addSubview:_positionLabel];
        
        _ratingView = [[RatingView alloc] initWithFrame:CGRectMake(10, 82, 300, 30)];
        [self addSubview:_ratingView];
    }
    
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
