//
//  MatchDetailViewController.m
//  FootplrPlayer
//
//  Created by EclipseKim on 2015. 5. 5..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "MatchDetailViewController.h"
#import "MomCell.h"
#import "AttendedPlayerCell.h"
#import "RatingChartCell.h"
#import "ScoreCell.h"
#import "MatchHeaderCell.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "MainViewController.h"

@interface MatchDetailViewController ()

@end

@implementation MatchDetailViewController

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) _init
{
    customNavBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [customNavBar setBackgroundColor:[UIColor clearColor]];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"nav_back_n"] forState:UIControlStateNormal];
    [backButton setFrame:CGRectMake(11, 11, 22, 22)];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [customNavBar addSubview:backButton];
    [self.view addSubview:customNavBar];
    
//    [self setNavigationBackButton];
    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    
    showGoals = NO;
    
    momInfo = [NSDictionary new];
    
    [self loadScheduleDetail];
    if(_schedule.attended && (_schedule.rated || _schedule.ratingExpired)) {
        [self loadMyRatings];
    }
}

-(void) loadScheduleDetail
{
    NSString *localeCode = [[FootplrHelper sharedFootplrHelper] getDeviceLocale];
    [[ApiHelper sharedApiHelper] getRequestWithUrl:MANAGER_API_URL withPath:[NSString stringWithFormat:@"/api/match/detail/%d/%@/2", _schedule.matchId, localeCode] withParameters:nil withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"sdsd : %@", responseObject);
        if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
            NSDictionary *result = [responseObject objectForKey:@"result"];
            
            NSArray *goals = [result objectForKey:@"goal"];
            _schedule.scores = [goals mutableCopy];
            
            [_schedule setAttendedPlayers:[result objectForKey:@"attend"]];
            
            NSDictionary *matchInfo = [result objectForKey:@"match"];
            _schedule.matchInfo = matchInfo;
            _schedule.memo = [matchInfo objectForKey:@"memo"];
            // mom
            if([matchInfo objectForKey:@"mom"] && ![[matchInfo objectForKey:@"mom"] isEqualToString:@""]) {
                momInfo = @{
                            @"mom" : [matchInfo objectForKey:@"mom"],
                            @"mom_back_no" : [matchInfo objectForKey:@"mom_back_no"],
                            @"mom_member_nm" : [matchInfo objectForKey:@"mom_member_nm"],
                            @"mom_position_nm" : [matchInfo objectForKey:@"mom_position_nm"],
                            };
            }
            
            
            [_tableView reloadData];
        }
    }];
}

-(void) loadMyRatings
{
    [[ApiHelper sharedApiHelper]
     getRequestWithUrl:API_URL
     withPath:[NSString stringWithFormat:@"/api/match/my_rate/%d/2", _schedule.matchId]
     withParameters:nil withSuccessCallback:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"%@", responseObject);
         if([responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] intValue] == API_SUCCESS) {
             myRatings = [[responseObject objectForKey:@"result"] objectForKey:@"my_rating"];
             [_tableView reloadData];
         }
     }];
}

#pragma mark - UITableViewDataSource

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_schedule.type == MATCH) {
        return _schedule.isMatchDone ? 5 : 1;
    } else {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_schedule.type == MATCH) {
        if(!_schedule.isMatchDone) {
            return 1;
        } else { // match done
            if(section == 0) {
                if(showGoals) {
                    return 1 + [_schedule.scores count];
                } else {
                    return 1;
                }
            } else if(section == 1) {
                if(_schedule.attended) {
                    if((_schedule.rated || _schedule.ratingExpired)) {
                        return 2;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            } else if(section == 2) { // attended players
                return [_schedule.attendedPlayers count];
            } else if(section == 3) {
                if(![_schedule.memo isEqualToString:@""]) { // memo exist
                    return 2;
                } else { // no memo
                    return 0;
                }
            } else {
                if([momInfo count] > 0) { // mom exist
                    return 1;
                } else { // no mom
                    return 0;
                }
            }
        }
    } else {
        if(section == 0) {
            return 1;
        } else { // attended players
            return [_schedule.attendedPlayers count];
        }
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        if(indexPath.row == 0) return 320; // header
        else return 44; // goals
    } else if(_schedule.type == MATCH && indexPath.section == 1) {
        if(_schedule.attended) {
            if((_schedule.rated || _schedule.ratingExpired)) {
                if(indexPath.row == 1) return 106.0f; // rate chart
                else return 44.0f;
            } else {
                if(indexPath.row == 0) return 44.0f;
                else return 48.0f; // rate button
            }
        } else {
            return 0;
        }
    } else if((_schedule.type == TEAM_SCHEDULE && indexPath.section == 1) ||  (_schedule.type == MATCH && indexPath.section == 2)) { // attended players
        return 44.0f;
    } else if(indexPath.section == 3) {
        if(_schedule.memo && ![_schedule.memo isEqualToString:@""]) { // memo exist
            if(indexPath.row == 0) return 44.0f;
            else {
                return [StringHelper heightForLabelWithFont:[UIFont systemFontOfSize:14.0f] withLabelWidth:300 withContent:_schedule.memo] + 20;
            }
        } else { // no memo
            return 0;
        }
    } else {
        if([momInfo count] > 0) { // mom exist
            return 200.0f;
        } else { // no mom
            return 0;
        }
    }
    return 0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    if(indexPath.section == 0) {
        if(indexPath.row == 0) { // header
            static NSString *matchHeaderCellReuseID = @"MatchHeaderCell";
            MatchHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:matchHeaderCellReuseID];
            if(!cell) cell = [[MatchHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:matchHeaderCellReuseID];
            
            cell.titleLabel.text = [NSString stringWithFormat:@"%@ / %@", _schedule.matchTimeString, _schedule.location];

            if(_schedule.type == MATCH) {
                cell.homeTeamNameLabel.text = [_schedule.matchInfo objectForKey:@"team_nm"];
                cell.awayTeamNameLabel.text = _schedule.title;
                
                
                if(!_schedule.isMatchDone) { // match not done yet
                    cell.homeScoreLabel.text = @"-";
                    cell.awayScoreLabel.text = @"-";
                    cell.matchStatusLabel.text = @"VS";
                    
                    // goalToggleButton
                    cell.goalToggleButton.hidden = YES;
                } else { // match done
                    [cell.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_URL, _schedule.coverImageUrl]]];
                    cell.homeScoreLabel.text = [NSString stringWithFormat:@"%d", _schedule.gf];
                    cell.awayScoreLabel.text = [NSString stringWithFormat:@"%d", _schedule.ga];
                    cell.matchStatusLabel.text = @"FT";
                    
                    // goalToggleButton
                    NSString *buttonTitle = showGoals ? NSLocalizedString(@"s_29", nil) : NSLocalizedString(@"s_28", nil);
                    [cell.goalToggleButton setTitle:buttonTitle forState:UIControlStateNormal];
                    [cell.goalToggleButton addTarget:self action:@selector(toggleGoalArea) forControlEvents:UIControlEventTouchUpInside];
                    
                    if([_schedule.scores count] == 0) { // no goals at all
                        cell.goalToggleButton.hidden = YES;
                    } else {
                        cell.goalToggleButton.hidden = NO;
                    }
                }
            } else {
                [cell.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_URL, _schedule.coverImageUrl]]];
                cell.subTitleLabel.text = _schedule.title;
                cell.matchStatusLabel.hidden = YES;
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else { //
            static NSString *scoreCellReuseID = @"ScoreCell";
            ScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:scoreCellReuseID];
            if(!cell) cell = [[ScoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:scoreCellReuseID];
            
            const static int GF = 1;
            
            NSDictionary *scoreInfo = [_schedule.scores objectAtIndex:indexPath.row-1];
            int goalType = [[scoreInfo objectForKey:@"type"] intValue];
            if(goalType == GF) {
                NSString *scoredPlayerName = [scoreInfo objectForKey:@"goal_member_nm"];
                NSString *assistedPlayerName = [scoreInfo objectForKey:@"as_member_nm"];
                NSString *scoredInfoString = scoredPlayerName;
                if(![assistedPlayerName isEqualToString:@""]) {
                    scoredInfoString = [NSString stringWithFormat:@"%@\n%@", scoredPlayerName, assistedPlayerName];
                }
                
                NSMutableAttributedString *homeScore = [[NSMutableAttributedString alloc] initWithString:scoredInfoString];
                [homeScore addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:NSMakeRange(0, [scoredPlayerName length])];
                [homeScore addAttribute:NSForegroundColorAttributeName value:[[UIManager sharedUIManager] appColor:COLOR_WA] range:NSMakeRange(0, [scoredPlayerName length])];
                [homeScore addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:11.0f] range:NSMakeRange([scoredPlayerName length], [homeScore length] - [scoredPlayerName length])];
                [homeScore addAttribute:NSForegroundColorAttributeName value:[[UIManager sharedUIManager] appColor:COLOR_WC] range:NSMakeRange([scoredPlayerName length], [homeScore length] - [scoredPlayerName length])];
                
                
                cell.homeScoreLabel.attributedText = homeScore;
                cell.awayScoreLabel.text = @"";
            } else {
                cell.homeScoreLabel.text = @"";
                cell.awayScoreLabel.text = @"GOAL";
            }
            cell.timeLabel.text = [scoreInfo objectForKey:@"goal_time"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    } else if(_schedule.type == MATCH && indexPath.section == 1) {
        if((_schedule.rated || _schedule.ratingExpired)) { // rated
            if(indexPath.row == 0) { // rate title
                cell.textLabel.text = NSLocalizedString(@"s_30", nil);
                cell.textLabel.font = [UIFont boldSystemFontOfSize:12.0f];
                cell.textLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
                [cell setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:163 withGreen:190 withBlue:199]];
            } else { // rate chart
                static NSString *ratingChartCellReuseID = @"RatingChartCell";
                RatingChartCell *cell = [tableView dequeueReusableCellWithIdentifier:ratingChartCellReuseID];
                if(!cell) cell = [[RatingChartCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ratingChartCellReuseID];
                
                int maxCount = 0;
                for(NSDictionary *rating in myRatings) {
                    int count = [[rating objectForKey:@"cnt"] intValue];
                    maxCount = maxCount > count ? maxCount : count;
                }
                
                for(NSDictionary *rating in myRatings) {
                    CGFloat ratingValue = [[rating objectForKey:@"rating"] floatValue];
                    int ratingIndex = ratingValue - 1;
                    int count = [[rating objectForKey:@"cnt"] intValue];
                    RatingChartBar *bar = (RatingChartBar *)[cell.ratingBars objectAtIndex:ratingIndex];
                    [bar setCount:count withMaximumCount:maxCount];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        } else { // not rated
            if(indexPath.row == 0) {
                cell.textLabel.text = NSLocalizedString(@"s_30", nil);
                cell.textLabel.font = [UIFont boldSystemFontOfSize:12.0f];
                cell.textLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
            } else {
                UIButton *rateButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [rateButton setFrame:CGRectMake(16, 0, 288, 48)];
                [rateButton setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:133 withGreen:170 withBlue:183]];
                [rateButton addTarget:self action:@selector(rate) forControlEvents:UIControlEventTouchUpInside];
                [rateButton setTitle:NSLocalizedString(@"s_31", nil) forState:UIControlStateNormal];
                [cell.contentView addSubview:rateButton];
            }
        }
    } else if((_schedule.type == TEAM_SCHEDULE && indexPath.section == 1) ||  (_schedule.type == MATCH && indexPath.section == 2)) { // attended players
        static NSString *attendedPlayerCellReuseID = @"AttendedPlayerCell";
        AttendedPlayerCell *cell = [tableView dequeueReusableCellWithIdentifier:attendedPlayerCellReuseID];
        if(!cell) cell = [[AttendedPlayerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:attendedPlayerCellReuseID];
        
        Player *p = [_schedule.attendedPlayers objectAtIndex:indexPath.row];
        cell.playerNameLabel.text = p.name;
        cell.backNumberLabel.text = [NSString stringWithFormat:@"%d", p.backNumber];
        if(_schedule.type == MATCH && (!_schedule.attended || (_schedule.attended && (_schedule.rated || _schedule.ratingExpired)))) { // only when match : not attended or attended&rated then show ratings
            cell.ratingLabel.text = [NSString stringWithFormat:@"%.2f", p.ratings];
        } else {
            cell.ratingLabel.text = @"";
        }
        if(_schedule.attended && (_schedule.rated || _schedule.ratingExpired) && indexPath.row == 0) { // attend & rated then highlight me
            [cell setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:133 withGreen:170 withBlue:183]];
            cell.playerNameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
            cell.backNumberLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
        } else {
            [cell setBackgroundColor:[UIColor whiteColor]];
            cell.playerNameLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
            cell.backNumberLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    } else if(indexPath.section == 3) { // memo
        if(indexPath.row == 0) {
            cell.textLabel.text = NSLocalizedString(@"s_32", nil);
            cell.textLabel.font = [UIFont boldSystemFontOfSize:12.0f];
            cell.textLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
        } else {
            cell.textLabel.numberOfLines = 0;
            cell.textLabel.text = _schedule.memo;
            cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
            cell.textLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_BA];
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            [cell.textLabel sizeToFit];
        }
    } else { // mom
        static NSString *momCellReuseID = @"MomCell";
        MomCell *cell = [tableView dequeueReusableCellWithIdentifier:momCellReuseID];
        if(!cell) {
            cell = [[MomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:momCellReuseID];
        }
        cell.momLabel.text = [NSString stringWithFormat:@"%@ %@", [momInfo objectForKey:@"mom_back_no"], [momInfo objectForKey:@"mom_member_nm"]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger numberOfRows = [self tableView:_tableView numberOfRowsInSection:0];
    CGFloat headerHeight = 0;
    for(int i = 0 ; i < numberOfRows ; i++) {
        headerHeight += [self tableView:_tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    if(scrollView.contentOffset.y > headerHeight-44) {
        [customNavBar setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
//        self.navigationController.navigationBarHidden = NO;
    } else {
        [customNavBar setBackgroundColor:[UIColor clearColor]];
//        self.navigationController.navigationBarHidden = YES;
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section >= 3) { // memo, mom
        UIView *header = [UIView new];
        [header setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
        return header;
    }
    return nil;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(_schedule.memo && ![_schedule.memo isEqualToString:@""] && section == 3) {
        return 20;
    } else if([momInfo count] > 0 && section == 4){
        return 20;
    }
    return 0;
}


#pragma mark - Actions
-(void) rate
{
    RateViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"RateStoryboard"];
    rvc.schedule = _schedule;
    rvc.delegate = self;
    [self.navigationController pushViewController:rvc animated:YES];
}

-(void) rated
{
    UIViewController *backVC = [self backViewController];
    if([backVC isKindOfClass:[MainViewController class]]) {
        [((MainViewController *) backVC).scheduleTableView reloadData];
    }
    
    [self loadScheduleDetail];
    [self loadMyRatings];
    [_tableView reloadData];
}

-(void) toggleGoalArea
{
    showGoals = !showGoals;
    [_tableView reloadData];
}
@end
