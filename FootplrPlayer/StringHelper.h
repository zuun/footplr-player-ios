//
//  StringHelper.h
//  Footplr
//
//  Created by EclipseKim on 2015. 1. 5..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringHelper : NSObject


+(CGFloat) heightForLabelWithFont:(UIFont *)font withLabelWidth:(CGFloat)labelWidth withContent:(NSString*) content;
+(BOOL) isEmptyString:(NSString *) s;
@end
