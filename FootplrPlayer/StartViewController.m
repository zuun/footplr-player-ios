//
//  StartViewController.m
//  footplr
//
//  Created by EclipseKim on 2015. 5. 7..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "StartViewController.h"
#import "JoinViewController.h"
#import "LoginViewController.h"
#import "TeamVerificationViewController.h"
#import "UserSelectionViewController.h"
#import "MainViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) _init
{
    [self setNavigationTitle:NSLocalizedString(@"s8", nil)];
    [self setNavigationBackButton];
    
    [self.view setBackgroundColor:[[UIManager sharedUIManager] appColor:COLOR_BG]];
    
    // fbJoinBtn
    [_fbJoinBtn setBackgroundImage:[[UIImage imageNamed:@"btn_fb_n"] stretchableImageWithLeftCapWidth:4 topCapHeight:4] forState:UIControlStateNormal];
    [_fbJoinBtn setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_WA] forState:UIControlStateNormal];
    [[_fbJoinBtn titleLabel] setFont:[UIFont systemFontOfSize:14.0f]];
    [_fbJoinBtn setTitle:NSLocalizedString(@"s9", nil) forState:UIControlStateNormal];
    [_fbJoinBtn addTarget:self action:@selector(joinWithFb) forControlEvents:UIControlEventTouchUpInside];
    
    // emailJoinBtn
    [_emailJoinBtn setBackgroundImage:[[UIImage imageNamed:@"btn_white_n"] stretchableImageWithLeftCapWidth:4 topCapHeight:4] forState:UIControlStateNormal];
    [_emailJoinBtn setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_BA] forState:UIControlStateNormal];
    [[_emailJoinBtn titleLabel] setFont:[UIFont systemFontOfSize:14.0f]];
    [_emailJoinBtn setTitle:NSLocalizedString(@"s10", nil) forState:UIControlStateNormal];
    [_emailJoinBtn addTarget:self action:@selector(joinWithEmail) forControlEvents:UIControlEventTouchUpInside];
    
    // loginBtn
    [_loginBtn setBackgroundImage:[[UIImage imageNamed:@"btn_white_n"] stretchableImageWithLeftCapWidth:4 topCapHeight:4] forState:UIControlStateNormal];
    [_loginBtn setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_BA] forState:UIControlStateNormal];
    [[_loginBtn titleLabel] setFont:[UIFont systemFontOfSize:14.0f]];
    [_loginBtn setTitle:NSLocalizedString(@"s12", nil) forState:UIControlStateNormal];
    [_loginBtn addTarget:self action:@selector(showLoginViewController) forControlEvents:UIControlEventTouchUpInside];
}


-(void) joinWithFb
{
    if ([FBSDKAccessToken currentAccessToken]) {
        
        NSString *accessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
        [self joinWithFbAccessToken:accessToken];
        NSLog(@"token from session : %@", accessToken);
    } else {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                // Process error
            } else if (result.isCancelled) {
                // Handle cancellations
            } else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if ([result.grantedPermissions containsObject:@"email"]) {
                    // Do work
                    NSString *accessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
                    NSLog(@"token from login : %@", accessToken);
                    [self joinWithFbAccessToken:accessToken];
                }
            }
        }];
    }
}

-(void) joinWithFbAccessToken:(NSString *)accessToken
{
    [[SessionManager sharedSessionManager] loginWithFb:accessToken];
    [[SessionManager sharedSessionManager] setDelegate:self];
}


-(void) joinWithEmail
{
    JoinViewController *jvc = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinStoryboard"];
    [self.navigationController pushViewController:jvc animated:YES];
}

-(void) showLoginViewController
{
    LoginViewController *lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginStoryboard"];
    [self.navigationController pushViewController:lvc animated:YES];
}

#pragma mark - SessionDelegate
-(void)loginComplete:(NSDictionary *)userInfo
{
    NSLog(@"loginComplete");
    NSString *teamId = [userInfo objectForKey:@"team_id"];
    NSString *userId = [userInfo objectForKey:@"user_id"];
    UIViewController *rootVC;
    if(teamId && [teamId isEqualToString:@""]) { // team not verified
        TeamVerificationViewController *tvvc = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamVerificationStoryboard"];
        rootVC = tvvc;
    } else if(userId && [userId isEqualToString:@""]) { // member not selected
        UserSelectionViewController *usvc =  [self.storyboard instantiateViewControllerWithIdentifier:@"UserSelectionStoryboard"];
        usvc.teamId = [teamId intValue];
        rootVC = usvc;
    } else {
        MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainStoryboard"];
        mainVC.teamId = [teamId intValue];
        rootVC = mainVC;
    }
    
    [self.navigationController pushViewController:rootVC animated:YES];
}

-(void) needJoin:(NSString *)accessToken
{
    JoinViewController *jvc = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinStoryboard"];
    jvc.accessToken = accessToken;
    [self.navigationController pushViewController:jvc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
