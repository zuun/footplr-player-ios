//
//  RatingView.m
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 19..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "RatingView.h"

@implementation RatingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _init];
    }
    return self;
}

- (void)refreshStars {
    int starIndex = roundf(_rating);
    [_delegate ratingChanged:_rating forTag:self.tag];
    for(int i = 1; i <= starViews.count; ++i) {
        UIImageView *imageView = [starViews objectAtIndex:i-1];
        if (i < starIndex) {
//            imageView.image = smallFullStarImage;
            imageView.image = highlightedDot;
        } else if (i > starIndex) {
//            imageView.image = smallEmptyStarImage;
            imageView.image = unhighlightedDot;
        } else {
            imageView.image = currentRatingDot;
            if(!currentRatingLabel) {
                currentRatingLabel = [UILabel new];
                currentRatingLabel.font = [UIFont systemFontOfSize:18.0f];
                currentRatingLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
                currentRatingLabel.textAlignment = NSTextAlignmentCenter;
                [self addSubview:currentRatingLabel];
            }
            
            currentRatingLabel.text = [NSString stringWithFormat:@"%d", i];
            [currentRatingLabel sizeToFit];
            currentRatingLabel.center = imageView.center;
            
//            if(isTouching) {
//                if(_rating == i) {
//                    imageView.image = bigFullStarImage;
//                } else {
//                    imageView.image = bigHalfStarImage;
//                }
//            } else {
//                if(_rating == i) {
//                    imageView.image = smallFullStarImage;
//                } else {
//                    imageView.image = smallHalfStarImage;
//                }
//            }
        }
    }
}

-(void) _init
{
    starViews = [NSMutableArray new];
    
//    isTouching = NO;
    
    _maxRating = 10;
    _rating = INITIAL_RATING;
    
    highlightedDot = [UIImage imageNamed:@"rate_dot_s"];
    unhighlightedDot = [UIImage imageNamed:@"rate_dot_n"];
    currentRatingDot = [UIImage imageNamed:@"rate_dot_f"];
    
//    smallEmptyStarImage = [UIImage imageNamed:@"rate_star_empty_n"];
//    bigEmptyStarImage = [UIImage imageNamed:@"rate_star_empty_p"];
//    smallHalfStarImage = [UIImage imageNamed:@"rate_star_half_n"];
//    bigHalfStarImage = [UIImage imageNamed:@"rate_star_half_p"];
//    smallFullStarImage = [UIImage imageNamed:@"rate_star_fill_n"];
//    bigFullStarImage = [UIImage imageNamed:@"rate_star_fill_p"];
    
    [self setupView];
}

- (void)setupView {
    for(int i = 0; i < _maxRating; ++i) {
        CGFloat starPadding = 0;
        CGFloat starWidth  = 30;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((starWidth+starPadding)*i, 0, starWidth, starWidth)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.image = unhighlightedDot;
//        imageView.image = smallEmptyStarImage;
        [starViews addObject:imageView];
        //            [starBtn setTag:RATE_BUTTON_TAG * (i+1)];
//        [[FootplrHelper sharedFootplrHelper] logFrame:imageView.frame];
        [self addSubview:imageView];
    }
    [self refreshStars];
}

- (void)setRating:(int)rating {
    _rating = rating;
    [self refreshStars];
}


#pragma mark - Touch Detection

- (void)handleTouchAtLocation:(CGPoint)touchLocation {
    _rating = 0;
    for(int i = (int)starViews.count - 1; i >= 0; i--) {
        UIImageView *imageView = [starViews objectAtIndex:i];
        if(touchLocation.x > imageView.frame.origin.x) {
            _rating = i + 1;
            break;
        }
    }
    [self refreshStars];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    isTouching = YES;
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
//    isTouching = NO;
    [self refreshStars];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    isTouching = NO;
    [self refreshStars];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
