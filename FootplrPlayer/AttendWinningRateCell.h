//
//  AttendWinningRateCell.h
//  footplr
//
//  Created by EclipseKim on 2015. 5. 30..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KAProgressLabel/KAProgressLabel.h>

@interface AttendWinningRateCell : UITableViewCell
{
    UILabel *attendedTextLabel;
    UILabel *notAttendedTextLabel;
    UILabel *ratingTextLabel;
}
@property (strong, nonatomic) KAProgressLabel *aWinningRateProgressLabel;
@property (strong, nonatomic) KAProgressLabel *naWinningRateProgressLabel;
@end
