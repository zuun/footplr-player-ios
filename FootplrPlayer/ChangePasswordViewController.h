//
//  ChangePasswordViewController.h
//  footplr
//
//  Created by EclipseKim on 2015. 6. 14..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "FootplrViewController.h"

@interface ChangePasswordViewController : FootplrViewController 

@property (strong, nonatomic) IBOutlet UITextField *curPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *pwTextField;
@property (strong, nonatomic) IBOutlet UITextField *pwTextField2;

@end
