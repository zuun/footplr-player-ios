//
//  ApiHelper.m
//  Footplr
//
//  Created by EclipseKim on 2014. 8. 17..
//  Copyright (c) 2014년 footplr. All rights reserved.
//

#import "ApiHelper.h"

@implementation ApiHelper
@synthesize delegate;
static ApiHelper *sharedApiHelper;
+ (ApiHelper*)sharedApiHelper {
    if (sharedApiHelper == nil) {
        sharedApiHelper = [[ApiHelper alloc] init];
    }
    return sharedApiHelper;
}

+(id)alloc
{
    @synchronized([ApiHelper class])
    {
        NSAssert(sharedApiHelper == nil, @"Attempted to allocate a second instance of a singleton.");
        sharedApiHelper = [super alloc];
        
        return sharedApiHelper;
    }
    
    return nil;
}

-(id)init
{
    self = [super init];
    [self initActivityView];
    return self;
}

#pragma mark - ActivityView
-(void) initActivityView
{
    window = [[[UIApplication sharedApplication] delegate] window];
    
    //        layer = [[UIView alloc] initWithFrame:window.frame];
    layer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    layer.center = window.center;
    [layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2f]];
    layer.layer.cornerRadius = 5.0f;
    layer.layer.masksToBounds = YES;
    layer.tag = API_LOADING_VIEW_TAG;
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = CGRectMake(20, 20, 40.0, 40.0);
    activityIndicator.hidden = NO;
    [layer addSubview:activityIndicator];
}


-(void) showActivityView
{
    [window addSubview:layer];
    [activityIndicator startAnimating];
}

-(void) hideActivityView
{
    if([layer superview] != nil)
        [layer removeFromSuperview];
}


#pragma mark - GET

-(void) getRequestWithUrl:(const NSString*)url withPath:(NSString*)path withParameters:(NSDictionary*) params withSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback
{
    if([path isKindOfClass:[NSNull class]] || [path isEqualToString:@""]) return;
    [self showActivityView];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager GET:[NSString stringWithFormat:@"%@%@", url, path] parameters:params success:[self setSuccessCallback:successCallback] failure:[self setErrorCallback:nil]];
}

-(void) getRequestWithUrl:(const NSString*)url withPath:(NSString*)path withParameters:(NSDictionary*) params withSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback withErrorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback
{
    if([path isKindOfClass:[NSNull class]] || [path isEqualToString:@""]) return;
    [self showActivityView];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager GET:[NSString stringWithFormat:@"%@%@", url, path] parameters:params success:[self setSuccessCallback:successCallback] failure:[self setErrorCallback:errorCallback]];
}

#pragma mark - POST

// success callback only
-(void) postRequestWithUrl:(const NSString*)url withPath:(NSString*)path withParameters:(NSDictionary*) params withSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback
{
    if([path isKindOfClass:[NSNull class]] || [path isEqualToString:@""]) return;
    [self showActivityView];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager POST:[NSString stringWithFormat:@"%@%@", url, path] parameters:params success:[self setSuccessCallback:successCallback] failure:[self setErrorCallback:nil]];
}

// success callback, error callback
-(void) postRequestWithUrl:(const NSString*)url withPath:(NSString*)path withParameters:(NSDictionary*) params withSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback withErrorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback
{
    if([path isKindOfClass:[NSNull class]] || [path isEqualToString:@""]) return;
    [self showActivityView];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager POST:[NSString stringWithFormat:@"%@%@", url, path] parameters:params success:[self setSuccessCallback:successCallback] failure:[self setErrorCallback:errorCallback]];
}

#pragma mark - CommonCallbacks

-(void (^)(AFHTTPRequestOperation *operation, id responseObject)) setSuccessCallback:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successCallback
{
    return ^void(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideActivityView];
        successCallback(operation, [self trimResponseObject:responseObject]);
        
        // 공통적으로 처리해야할것
        if([[responseObject objectForKey:@"code"] intValue] == 500 || [[responseObject objectForKey:@"message"] isEqualToString:@"Need login"]) {
            [Flurry logEvent:@"SESSION FAILED - NEEW LOGIN"];
            [delegate showLoginViewController];
            return ;
        }
    };
}

-(void (^)(AFHTTPRequestOperation *operation, NSError *error)) setErrorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback
{
    return ^void(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideActivityView];
        
        if(errorCallback) {
            errorCallback(operation, error);
        }
        
        // 공통적으로 처리해야할것
        NSLog(@"[Footplr]API Error : %@", error);
        NSLog(@"%@", operation.responseString);
        [[FootplrHelper sharedFootplrHelper] showError];
    };
}

-(id) trimResponseObject:(id) responseObject
{
    if([responseObject isKindOfClass:[NSDictionary class]]) {
        responseObject = [self trimDictionary:responseObject];
    } else if([responseObject isKindOfClass:[NSArray class]]) {
        responseObject = [self trimArray:responseObject];
    }
    return responseObject;
}

-(NSDictionary *) trimDictionary:(NSMutableDictionary *)_dictionary
{
    NSMutableDictionary *dictionary = [_dictionary mutableCopy];
    for(id key in [dictionary allKeys]) {
        id obj =[dictionary objectForKey:key];
        if([obj isKindOfClass:[NSArray class]]) {
            [dictionary setObject:[self trimArray:obj] forKey:key];
        } else if([obj isKindOfClass:[NSDictionary class]]) {
            [dictionary setObject:[self trimDictionary:obj] forKey:key];
        } else if([obj isKindOfClass:[NSNull class]]) {
            [dictionary setObject:@"" forKey:key];
        }
    }
    return dictionary;
}

-(NSArray *) trimArray:(NSMutableArray *)_array
{
    NSMutableArray *array = [_array mutableCopy];
    for(int i = 0 ; i < [array count] ; i++) {
        id obj = [array objectAtIndex:i];
        if([obj isKindOfClass:[NSArray class]]) {
            [array replaceObjectAtIndex:i withObject:[self trimArray:obj]];
        } else if([obj isKindOfClass:[NSDictionary class]]) {
            [array replaceObjectAtIndex:i withObject:[self trimDictionary:obj]];
        } else if([obj isKindOfClass:[NSNull class]]) {
            [array replaceObjectAtIndex:i withObject:@""];
        }
    }
    return array;
}

//
//-(NSDictionary*) requestUrlWithUrlPath:(NSString*) path withParameters:(NSDictionary*) params
//{
//    NSDictionary* headers = @{@"accept": @"application/json"};
//    
//    UNIHTTPJsonResponse *response = [[UNIRest post:^(UNISimpleRequest *request) {
//        [request setUrl:[NSString stringWithFormat:@"%@%@", API_URL, path]];
//        [request setHeaders:headers];
//        [request setParameters:params];
//    }] asJson];
//    
//    NSDictionary* result = [[response body] JSONObject];
//    if([[result objectForKey:@""] isEqualToString:@"NEED LOGIN"]) {
//        [delegate showLoginViewController];
//        return nil;
//    }
//    return result;
//}

@end
