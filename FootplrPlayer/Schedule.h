//
//  Schedule.h
//  footplr
//
//  Created by JunhyuckKim on 2015. 5. 21..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DateHelper.h"

enum scheduleTypes {
    MATCH = 10,
    TEAM_SCHEDULE
};
@interface Schedule : NSObject
{
    NSDictionary *schedule;
    int rateCount;
    CGFloat ratingSum;
}
-(id) initWithScheduleInfo:(NSDictionary *) _schedule;
@property NSString *title;
@property int type;
@property int teamId;
@property int stadiumId;
@property NSDate *matchTime;
@property NSString *matchTimeString;
@property int matchId;
@property NSString *location;
@property int ga;
@property int gf;
@property NSString *coverImageUrl;
@property BOOL isMatchDone;
@property BOOL rated;
@property BOOL attended;
@property CGFloat rating;
@property NSString *memo;
@property NSMutableArray *attendedPlayers;
@property NSMutableArray *scores;
@property NSDictionary *matchInfo;
@property BOOL ratingExpired;

@property NSString *monthKey;

-(void) setAttendedPlayers:(NSMutableArray *)attendedPlayers;
-(void) addRating:(CGFloat)__rating;
@end
