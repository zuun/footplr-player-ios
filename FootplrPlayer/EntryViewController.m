//
//  EntryViewController.m
//  FootplrPlayer
//
//  Created by JunhyuckKim on 2015. 2. 23..
//  Copyright (c) 2015년 footplr. All rights reserved.
//

#import "EntryViewController.h"
#import "StartViewController.h"
#import "WebViewController.h"

@interface EntryViewController ()

@end

@implementation EntryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[FootplrHelper sharedFootplrHelper] logFrame:self.view.frame];
    
    [self _init];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void) _init
{
    [self.view setBackgroundColor:[[UIManager sharedUIManager] colorWithRed:26 withGreen:28 withBlue:29]];
    
    //titleLabel
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 44, 280, 60)];
    titleLabel.text = NSLocalizedString(@"s3", nil);
    titleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WA];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:24.0f];
    titleLabel.numberOfLines = 2;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.view addSubview:titleLabel];
    
    // subtitleLabel
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 120, 280, 40)];
    subTitleLabel.text = NSLocalizedString(@"s4", nil);
    subTitleLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_WC];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    subTitleLabel.font = [UIFont systemFontOfSize:14.0f];
    subTitleLabel.numberOfLines = 0;
    subTitleLabel.lineBreakMode = NSLineBreakByWordWrapping ;
    [self.view addSubview:subTitleLabel];
    
    
    // UIImageView
    imageIndex = 0;
    images = @[
               [UIImage imageNamed:@"start_hero_01"],
               [UIImage imageNamed:@"start_hero_02"],
               [UIImage imageNamed:@"start_hero_03"]
               ];
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(64, 208, 192, 210)];
//    [imageView setBackgroundColor:[UIColor clearColor]];
    imageView.image = [images objectAtIndex:imageIndex];
    NSTimer *t = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(updateImage) userInfo:nil repeats:YES];
    
    [self.view addSubview:imageView];
    
    // startButton
    UIButton *startbutton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [startbutton setFrame:CGRectMake(20, CGRectGetMaxY(self.view.frame) - 114, 280, 48)];
    [startbutton setBackgroundImage:[[UIImage imageNamed:@"btn_black_n"] stretchableImageWithLeftCapWidth:4 topCapHeight:4] forState:UIControlStateNormal];
    [startbutton setTitle:NSLocalizedString(@"s5", nil) forState:UIControlStateNormal];
    [startbutton setTitleColor:[[UIManager sharedUIManager] appColor:COLOR_WA] forState:UIControlStateNormal];
    startbutton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [startbutton addTarget:self action:@selector(start) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startbutton];
    
    // moreLabel
    UILabel *moreLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.view.frame) - 41, 280, 18)];
	moreLabel.text = NSLocalizedString(@"s6", nil);
    moreLabel.textColor = [[UIManager sharedUIManager] appColor:COLOR_RED_];
    moreLabel.textAlignment = NSTextAlignmentCenter;
    moreLabel.userInteractionEnabled = YES;
    moreLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.view addSubview:moreLabel];
    
    UITapGestureRecognizer *moreTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDetail)];
    [moreLabel addGestureRecognizer:moreTap];
}


#pragma mark - Actions

-(void) showDetail
{
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://footplr.com"]];
    WebViewController *wvc = [[WebViewController alloc] init];
    wvc.urlString = @"http://footplr.com";
    [self.navigationController pushViewController:wvc animated:YES];
}

-(void) start
{
    StartViewController *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"StartStoryboard"];
    [self.navigationController pushViewController:svc animated:YES];
}

-(void) updateImage
{
    imageIndex ++;
    if(imageIndex > 2) imageIndex = 0;
    
    [UIView transitionWithView:imageView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        imageView.image = [images objectAtIndex:imageIndex];
                    } completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
